# Reinforcement Learning PPO using OpenAI Gym

This project reimplemented the baseline for the PPO (Proximal Policy Optimisation)
algorithm using OpenAI Gym environment in Tensorflow 2.
Some multiagent environment have been created and one with a simple world generator,
 an implementation of the A* pathfinding algorithm and an integration of box2d lite 2d physics engine.

A simple render using moderngl has also been added.

## Installation

It is recommended to use a virtual environment. This project is written for Python 3

To install using pip:

`pip install -r requirements.txt`

Then to compile Cython files:

`python3 setup.py build_ext --inplace`

## Usage

For the training backend you need to run :

`python3 rollout_worker.py --port {port} --address {address} `

Where port and url are what the Tornado server will use to listen to incoming connection (default are 8080 and localhost).

To launch the training :

`python3 run.py --network {network} --env {PursuingEnv,GoalEnv,TurretEnv} --conf {Conf} --num_envs {Num_envs} --num_envs_remote {Num_env_remotes} --num_iter {num_iter} --train`

