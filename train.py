from typing import Union, Callable, Dict, Any, List, cast

from models import Model_PPO, get_network
from policies import SimplePolicy, HistoryPolicy
import time
import os
from tornado.websocket import websocket_connect, httpclient, httputil, WebSocketClientConnection, IOLoop, _default_max_message_size
from wait_iterator_modifiable import WaitIteratorModifiable
import zlib
import json
import base64
import pickle
import cloudpickle
import tornado
import numpy as np
import logging

logger = logging.getLogger("ppo")
logger.setLevel(logging.DEBUG)


def websocket_connect_train(
    url: Union[str, httpclient.HTTPRequest],
    callback: Callable[["Future[WebSocketClientConnection]"], None] = None,
    connect_timeout: float = None,
    on_message_callback: Callable[[Union[None, str, bytes]], None] = None,
    compression_options: Dict[str, Any] = None,
    ping_interval: float = None,
    ping_timeout: float = None,
    max_message_size: int = _default_max_message_size,
    subprotocols: List[str] = None,
    max_buffer_size=1024*1024*20,
) -> "Awaitable[WebSocketClientConnection]":

    if isinstance(url, httpclient.HTTPRequest):
        assert connect_timeout is None
        request = url
        # Copy and convert the headers dict/object (see comments in
        # AsyncHTTPClient.fetch)
        request.headers = httputil.HTTPHeaders(request.headers)
    else:
        request = httpclient.HTTPRequest(url, connect_timeout=connect_timeout)
    request = cast(
        httpclient.HTTPRequest,
        httpclient._RequestProxy(request, httpclient.HTTPRequest._DEFAULTS),
    )
    conn = WebSocketClientConnection(
        request,
        on_message_callback=on_message_callback,
        compression_options=compression_options,
        ping_interval=ping_interval,
        ping_timeout=ping_timeout,
        max_message_size=max_message_size,
        subprotocols=subprotocols,
    )
    conn.max_buffer_size = max_buffer_size
    if callback is not None:
        IOLoop.current().add_future(conn.connect_future, callback)
    return conn.connect_future

def constfn(val):
    def f(_):
        return val

    return f


def explained_variance(ypred, y):
    """
    Computes fraction of variance that ypred explains about y.
    Returns 1 - Var[y-ypred] / Var[y]

    interpretation:
        ev=0  =>  might as well have predicted zero
        ev=1  =>  perfect prediction
        ev<0  =>  worse than just predicting zero

    """
    assert y.ndim == 1 and ypred.ndim == 1
    vary = np.var(y)
    return np.nan if vary == 0 else 1 - np.var(y - ypred) / vary


def print_layers(m):
    if hasattr(m, "layers"):
        for l in m.layers:
            print(l.name)
            print_layers(l)


def prepare_model_streaming(model):
    model.save_weights("temp.hdf5")

    with open("temp.hdf5", "rb") as f:
        model_data = base64.b64encode(f.read()).decode("ascii")

    return model_data


def get_size_buffer(buffer):
    len_buffer = 0
    for info, data in buffer:
        len_buffer += len(data[0][0])

    return len_buffer


def wait_for_msg_on(con, waitIterator):
    if waitIterator.done():
        waitIterator = WaitIteratorModifiable(con.read_message())
    else:
        waitIterator.add_future(con.read_message(), waitIterator.current_index)

    return waitIterator


def send_models_to_worker(con, policies, frac):
    models_train = [prepare_model_streaming(p.model) for p in policies]

    message_resp = {"type": "model", "models_train": models_train, "frac": frac}

    con.write_message(json.dumps(message_resp))


def extract_data_buffer(buffer):
    data_buffer = {}

    num_agents = len(buffer[0][-1][0])

    info_temp = {"ep_len": [], "rewards": [], "avg_reward": [], "num_ep_done": []}
    data_temp = [[[] for _ in range(num_agents)] for _ in range(len(buffer[0][-1]))]

    num_buffer = len(buffer)

    for info, data in buffer:

        info_temp["ep_len"].append(info["ep_len"])
        info_temp["rewards"].append(info["rewards"])
        info_temp["avg_reward"].append(info["avg_reward"])
        info_temp["num_ep_done"].append(info["num_ep_done"])

        for idx, d in enumerate(data):
            for idx_a in range(num_agents):
                data_temp[idx][idx_a].append(d[idx_a])

    data_buffer["data"] = []
    for data in data_temp:
        data_buffer["data"].append([0] * num_agents)
        for idx_a in range(num_agents):
            if all(d is None for d in data[idx_a]):
                data_buffer["data"][-1][idx_a] = None
            else:
                data_buffer["data"][-1][idx_a] = np.concatenate(data[idx_a], axis=0)

    data_buffer["info"] = {"ep_len": np.mean(info_temp["ep_len"]),
                           "rewards": np.concatenate(info_temp["rewards"], axis=-1),
                           "avg_reward": np.mean(info_temp["avg_reward"], axis=0),
                           "num_ep_done": np.sum(info_temp["num_ep_done"])}

    return data_buffer


METRICS_BUFFER = None
EPISODE_DONE = 0

def init_metrics_buffer(num_agents):
    global METRICS_BUFFER, EPISODE_DONE

    METRICS_BUFFER = []
    for i in range(num_agents):
        METRICS_BUFFER.append(
            {"values": [], "returns": [], "rewards": [], "lossvals": [],
             "avg_reward": [], "ep_len": [], "fps": [], "tot_ep_done": EPISODE_DONE})


def update_metrics(idx, values, returns, epinfo, lossvals, fps):
    METRICS_BUFFER[idx]["values"].append(values)
    METRICS_BUFFER[idx]["returns"].append(returns)
    # METRICS_BUFFER[idx]["rewards"].append(epinfo["rewards"][idx])
    METRICS_BUFFER[idx]["avg_reward"].append(epinfo["avg_reward"][idx])
    METRICS_BUFFER[idx]["ep_len"].append(epinfo["ep_len"])
    METRICS_BUFFER[idx]["fps"].append(fps)
    METRICS_BUFFER[idx]["lossvals"].append(lossvals)
    METRICS_BUFFER[idx]["tot_ep_done"] += epinfo["num_ep_done"]


def log_metrics_batch(num_agents, iteration, steps_per_iter, elapsed_time):
    global EPISODE_DONE
    print('Done.')
    print("Iteration {} :".format(iteration))
    for idx in range(num_agents):

        values = np.concatenate(METRICS_BUFFER[idx]["values"], axis=0)
        returns = np.concatenate(METRICS_BUFFER[idx]["returns"], axis=0)
        # rewards = np.concatenate(METRICS_BUFFER[idx]["rewards"], axis=0)
        avg_reward = np.mean(np.stack(METRICS_BUFFER[idx]["avg_reward"], axis=0), axis=0)
        ep_len = np.mean(np.stack(METRICS_BUFFER[idx]["ep_len"], axis=0))

        ev = np.mean(np.stack([explained_variance(v.reshape((-1, 1))[:, 0], r.reshape((-1, 1))[:, 0]) for v, r in
                               zip(METRICS_BUFFER[idx]["values"], METRICS_BUFFER[idx]["returns"])], axis=0), axis=0)

        lossvals = [[] for _ in METRICS_BUFFER[idx]["lossvals"][0]]
        lossname = [n for l, n in METRICS_BUFFER[idx]["lossvals"][0]]

        for lv in METRICS_BUFFER[idx]["lossvals"]:
            for i, val_name in enumerate(lv):
                lossvals[i].append(val_name[0])

        lossvals = [(np.mean(np.stack(lossvals[i], axis=0)), name) for i, name in enumerate(lossname)]

        # print("agent {} avg reward : {:.5g}".format(idx, np.mean(rewards)))
        print("agent {} avg reward corrected : {:.5g}".format(idx, avg_reward))
        print("agent {} avg episode len : {:.6g}".format(idx, ep_len))
        print("agent {} explained variance : {:.6g}".format(idx, float(ev)))
        print("agent {} returns mean : {:.6g} returns std : {:.6g}".format(idx,
                                                                           np.mean(returns),
                                                                           np.std(returns)))
        print("agent {} values mean : {:.6g} values std : {:.6g}".format(idx,
                                                                         np.mean(values), np.std(values)))
        for loss, name in lossvals:
            print("agent {} loss/{} : {:.5g}".format(idx, name, loss))

    fps = np.mean(np.stack(METRICS_BUFFER[0]["fps"], axis=0))
    print("End of iteration {}".format(iteration))
    print("fps : {:.4g}".format(fps))
    print("time elapsed : {:.5g}".format(elapsed_time))
    print("steps done : {:.5g}".format(iteration * steps_per_iter))
    print("episodes done : {:.5g}".format(METRICS_BUFFER[0]["tot_ep_done"]))
    EPISODE_DONE += METRICS_BUFFER[0]["tot_ep_done"]

    init_metrics_buffer(num_agents)


def train_model_on_trajectory(policies, data_buffer, do_log, steps_per_iter,
                              tfirststart, tstart, i, noptepochs, batch_sizes,
                              lrnow, cliprangenow):
    obs, returns, actions, values, neglogp, advs, mask, states, states_val = data_buffer["data"]

    epinfo = data_buffer["info"]

    lossvals = []

    if METRICS_BUFFER is None:
        init_metrics_buffer(len(policies))

    for idx, p in enumerate(policies):
        # opponents_p = policies_opponent[idx]
        advs_norm = (advs[idx] - advs[idx].mean()) / (max(advs[idx].std(), 1e-4))
        states_arg = (states[idx], states_val[idx])

        losses = p.train_on(lrnow, cliprangenow, obs[idx], actions[idx], advs_norm,
                            returns[idx], values[idx], neglogp[idx], noptepochs,
                            batch_sizes[idx], mask=mask, states=states_arg)

        lossvals.append(losses)

        p.save_iter(i)

    # End timer
    tnow = time.perf_counter()
    # Calculate the fps (frame per second)
    fps = int(steps_per_iter / (tnow - tstart))
    elapsed_time = tnow - tfirststart

    for idx in range(len(policies)):
        update_metrics(idx, values[idx], returns[idx], epinfo, lossvals[idx], fps)

    if do_log:
        log_metrics_batch(len(policies), i, steps_per_iter, elapsed_time)


async def learn(network, agents, runners, timestep_size, rollout_size_per_worker=500,
                total_iter=1000, start_iter=None, lr=5e-4,
                gamma=0.999, lam=0.95, batch_size=10000,
                log_interval=10, noptepochs=6, cliprange=0.2, save_path="models", **net_kwargs):
    # assert isinstance(env, AsyncVectorEnv), "error env should be an AsyncVectorEnv instance"

    assert isinstance(timestep_size, int)

    max_size_buffer = np.sum([r["nenvs"] for r in runners]) * rollout_size_per_worker

    print("Starting training with %d iter and %d envs with %d timesteps" % (
        total_iter, max_size_buffer // rollout_size_per_worker, rollout_size_per_worker))

    if isinstance(lr, float):
        lr_float = lr
        lr = constfn(lr)
    else:
        assert callable(lr)
        lr_float = lr(1)
    if isinstance(cliprange, float):
        cliprange = constfn(cliprange)
    else:
        assert callable(cliprange)
    total_iter = int(total_iter)
    noptepochs = int(noptepochs)

    network_tuple = get_network(network)

    if timestep_size is None:
        raise ValueError("timestep_size should be provided")

    policies = []
    policies_history = []

    models_args = []
    models_kwargs = []
    batch_sizes = []

    nenvs = np.sum([r["env_fn"]().nenvs for r in runners])
    print("Creating models")
    for i, a in enumerate(agents):
        input_shape = a.observation_space.shape

        model_args = (input_shape, network_tuple, a.action_space, timestep_size)
        model_kwargs = dict(prefix="agent_{}_".format(i), lr=lr_float, **net_kwargs)
        print(f"model args: {model_args} model kwargs: {model_kwargs}")
        model = Model_PPO(*model_args, **model_kwargs)

        batch_size_env = batch_size // timestep_size if model.recurrent else batch_size
        batch_sizes.append(batch_size_env)

        print("warming up")
        model.warm_up(batch_size_env)
        policies.append(SimplePolicy(model, save_path, "agent_%d_policy" % i))

        models_args.append(model_args)
        models_kwargs.append(model_kwargs)

    if start_iter is None:
        start_iter = 1
    else:
        for p in policies:
            p.load_iter(start_iter)

    nenvs = 0

    websocket_conn = []

    print("launch ppo with {} runners".format(len(runners)))
    for r in runners:
        nenvs += r["nenvs"]
        con = await websocket_connect_train(r["url"], max_message_size=1024 * 1024 * 512,
                                            max_buffer_size=1024 * 1024 * 512)
        message = {"type": "initialization",
                   "parameters": {"gamma": gamma, "lambda": lam, "timestep_size": timestep_size,
                                  "rollout_size": rollout_size_per_worker},
                   "env_fn": base64.b64encode(cloudpickle.dumps(r["env_fn"])).decode("ascii"),
                   "models_args": base64.b64encode(cloudpickle.dumps(models_args)).decode("ascii"),
                   "models_kwargs": base64.b64encode(cloudpickle.dumps(models_kwargs)).decode("ascii"),
                   "batch_sizes": base64.b64encode(cloudpickle.dumps(batch_sizes)).decode("ascii")}
        await con.write_message(json.dumps(message))
        websocket_conn.append(con)

    trajectory_data_buffer = []  # info, data_trajectory per worker roll out

    # Start total timer
    waitIterator = WaitIteratorModifiable(*[con.read_message() for con in websocket_conn])

    tfirststart = time.perf_counter()

    i = start_iter
    # Start timer
    tstart = time.perf_counter()
    frac = (i - 1.0) / total_iter

    # Calculate the learning rate
    lrnow = lr(1 - frac)
    # Calculate the cliprange
    cliprangenow = cliprange(frac)

    traj_data = None
    while i <= total_iter:
        while not waitIterator.done():
            try:
                raw_msg = await waitIterator.next()
            except Exception as e:
                print("Error {} from {}".format(e, waitIterator.current_future))
            else:

                try:
                    msg = json.loads(raw_msg)
                except Exception as e:
                    print("Error message received {} is not JSON string, ending training".format(raw_msg))
                    tornado.ioloop.IOLoop.current().add_callback(lambda: tornado.ioloop.IOLoop.current().stop())
                    return
                if msg["type"] == "trajectory_data":
                    compressed_data = base64.b64decode(msg["data"].encode("ascii"))
                    traj_data = pickle.loads(zlib.decompress(compressed_data))

                    # trajectory_data_buffer.append((traj_data[-1], list(traj_data[:-1])))

        # len_buffer = get_size_buffer(trajectory_data_buffer)
        if traj_data:  # len_buffer >= max_size_buffer:

            if i % log_interval == 0: print('Stepping environment...')

            data_buffer = {"data": traj_data[0], "info": traj_data[1]}
            train_model_on_trajectory(policies, data_buffer, i % log_interval == 0 or i == start_iter,
                                      max_size_buffer, tfirststart, tstart, i, noptepochs, batch_sizes,
                                      lrnow, cliprangenow)

            i += 1
            # Start timer
            tstart = time.perf_counter()
            frac = (i - 1.0) / total_iter

            # Calculate the learning rate
            lrnow = lr(1 - frac)
            # Calculate the cliprange
            cliprangenow = cliprange(frac)

            trajectory_data_buffer = []

        for con in websocket_conn:
            send_models_to_worker(con, policies, frac)
        waitIterator = WaitIteratorModifiable(*[con.read_message() for con in websocket_conn])
        traj_data = None

    print("training complete")
    tornado.ioloop.IOLoop.current().add_callback(lambda: tornado.ioloop.IOLoop.current().stop())
