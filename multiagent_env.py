import math
import time

import gym
from gym import spaces, logger
from gym.utils import seeding
from objects import Agent, GameGrid
import numpy as np
from copy import deepcopy
from physics_engine.loose_grid import Grid
from physics_engine.PyWorld import PyWorld

try:
    import rendering

    g_rendering_enabled = True
except:
    g_rendering_enabled = False
    print("Cannot import render")


class MultiAgent2DEnv(gym.Env):

    @staticmethod
    def get_default_agents_objects(height, width):
        raise NotImplementedError

    def __init__(self, agents, width, height, objects=None, seed=None, timesteps=1000,
                 start_decrease_alpha=0.1, len_decrease_alpha=0.1, rendering=False):

        assert all(isinstance(agent, Agent) for agent in agents), "Error agent argument must subclass Agent"
        self.agents = agents
        self.objects = objects
        self.num_agents = len(agents)

        self.start_decrease_alpha = start_decrease_alpha
        self.len_decrease_alpha = len_decrease_alpha

        self.all_entities = []
        self.rendering = rendering
        if not self.agents is None:
            self.all_entities += self.agents
        if not self.objects is None:
            self.all_entities += self.objects

        self.num_entities = len(self.all_entities)

        self.max_timesteps = timesteps
        self.cur_step = 1

        self.physics_world = PyWorld(0, 0, 50, 5, 5, width, height)
        self.tlast_frame = None

        self.width = width
        self.height = height

        self.frac = 0

        self.cur_step = 0

        self.alpha = 1.0
        self.seed_state = seed
        self.seed(seed=seed)

        self._viewer = None

        self.observation_space = spaces.Tuple([agent.observation_space for agent in agents])
        self.action_space = spaces.Tuple([agent.action_space for agent in agents])

    def _step(self, actions):
        assert len(actions) == len(self.agents), "Wrong action set !"
        self.alpha = 1 - min(0.5, max(0, self.frac - self.start_decrease_alpha) / self.len_decrease_alpha)

        self.physics_world.step(self.tau)

        for i, agent in enumerate(self.agents):
            agent.step(actions[i])

        if not self.objects is None:
            for o in self.objects:
                o.step()

    def _compute_tau(self):
        if self.tlast_frame is None:
            self.tau = 1 / 60
            self.tlast_frame = time.perf_counter()
        else:
            tcur_frame = time.perf_counter()
            self.tau = min(tcur_frame - self.tlast_frame, 1 / 30)
            self.tlast_frame = tcur_frame

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        for a in self.agents:
            a.np_random = self.np_random
        if not self.objects is None:
            for o in self.objects:
                o.np_random = self.np_random
        return [seed]

    def reset(self):
        self.cur_step = 0

        self.seed_state = self.seed_state + 1 if isinstance(self.seed_state, int) else None
        self.seed(seed=self.seed_state)

        obs = []

        for agent in self.agents:
            obs.append(agent.reset(self.width, self.height, self.all_entities))

        if not self.objects is None:
            for o in self.objects:
                o.reset(self.width, self.height)

        self.physics_world.clear()
        for ent in self.all_entities:
            self.physics_world.add_proxy(ent.physics_proxy)

        self.tlast_frame = None

        return tuple(obs)

    def set_frac(self, frac):
        self.frac = frac

    @property
    def viewer(self):
        if not g_rendering_enabled or not self.rendering:
            return None

        if self._viewer is None:

            self._viewer = rendering.Viewer(self.width, self.height)

            for i in range(self.num_agents):
                if isinstance(self.agents[i].geom, list):
                    for g in self.agents[i].geom:
                        self._viewer.add_geom(g)
                else:
                    self._viewer.add_geom(self.agents[i].geom)

            if self.objects:
                for i in range(len(self.objects)):
                    if isinstance(self.objects[i].geom, list):
                        for g in self.objects[i].geom:
                            self._viewer.add_geom(g)
                    else:
                        self._viewer.add_geom(self.objects[i].geom)

        return self._viewer

    def render(self, mode='human'):

        if not g_rendering_enabled or not self.rendering:
            return

        self.viewer.render()

    def close(self):
        pass


class RewardSumWrapper(gym.Wrapper):

    def reset(self, **kwargs):
        self.rew_sum = 0.0
        return self.env.reset(**kwargs)

    def step(self, action):
        observation, reward, done, info = self.env.step(action)

        # reward = [max(0, r) for r in reward]
        if isinstance(self.rew_sum, float):
            size_rew = 1 if isinstance(reward, float) else len(reward)
            self.rew_sum = np.zeros((size_rew,))
        self.rew_sum += np.array(reward)

        info["rew_sum"] = self.rew_sum
        return observation, reward, done, info
