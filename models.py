import numpy as np

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.layers import *
from tensorflow.keras.models import Model
from tensorflow.keras import optimizers
from tensorflow.keras.initializers import *
from tensorflow.python.keras.losses import kullback_leibler_divergence
from gym import spaces
from tensorflow.python.keras.utils import layer_utils

mapping = {}
CUSTOM_LAYERS = {}


def register(name, is_recurrent):
    def _thunk(func):
        mapping[name] = (func, is_recurrent)
        return func

    return _thunk


def get_network(name):
    """
    If you want to register your own network outside models.py, you just need:

    Usage Example:
    -------------
    from baselines.common.models import register
    @register("your_network_name")
    def your_network_define(**net_kwargs):
        ...
        return network_fn

    """
    if callable(name):
        return name
    elif name in mapping:
        return mapping[name]
    else:
        raise ValueError('Unknown network type: {}'.format(name))


def register_layer(name):
    def _thunk(cls):
        CUSTOM_LAYERS[name] = cls
        return cls

    return _thunk


@register("mlp_network", False)
class MLPLayer(Layer):

    def __init__(self, hiddens=None, activation="relu", init_k='glorot_uniform', prefix="agent_", **kwargs):
        super(MLPLayer, self).__init__(name=prefix + "mlp_layer", **kwargs)

        self.activation = activation
        if hiddens is None:
            hiddens = [32, 32]

        self.h_lstm = hiddens[0]
        self.denses = [Dense(int(h), kernel_initializer=init_k, name=prefix + "dense_{}".format(idx)) for idx, h in
                       enumerate(hiddens)]

    def call(self, x):

        for d in self.denses:
            x = d(x)
            x = Activation(self.activation)(x)

        return x

@register("chasing_network", True)
class ChasingLayer(Layer):

    def __init__(self, init_k='glorot_uniform', h_embedding=64, h_dense=64,
                 num_attention_heads=2, size_attention_head=32, len_lidar=30, num_objects=16,
                 h_lstm=64, activation="relu", prefix="agent_", **kwargs):
        super(ChasingLayer, self).__init__(name=prefix + "lstm_layer", **kwargs)

        self.activation = activation
        self.prefix = prefix
        self.dense = Dense(int(h_dense), name=prefix + "dense", activation=activation)
        self.dense_bn = BatchNormalization(name=self.prefix + f"dense_bn")

        self.self_embedding = Dense(int(h_embedding), name=prefix + "self_embedding")
        self.self_emb_bn = BatchNormalization(name=self.prefix + f"self_bn_emb")
        self.agent_embedding = Dense(int(h_embedding), name=prefix + "agent_embedding")
        self.agent_emb_bn = BatchNormalization(name=self.prefix + f"agent_bn_emb")
        self.object_embedding = Dense(int(h_embedding), name=prefix + "object_embedding")
        self.obj_emb_bn = BatchNormalization(name=self.prefix + f"obj_bn_emb")
        self.h_embedding = int(h_embedding)

        self.h_lstm = int(h_lstm)
        self.num_objects = num_objects
        self.len_lidar = len_lidar
        self.lstm = LSTM(int(h_lstm), return_state=True, return_sequences=True, kernel_initializer=init_k,
                         name=prefix + "lstm")
        self.lstm_bn = BatchNormalization(name=self.prefix + f"lstm_bn")

        self.circular_conv = Conv1D(1, 3, padding='same', name=prefix + "circular_1d_conv",
                                    input_shape=(self.len_lidar + 2, 1))

        self.num_attention_heads = int(num_attention_heads)
        self.size_attention_head = int(size_attention_head)
        self.d_attention = self.num_attention_heads * self.size_attention_head
        assert self.d_attention == self.h_embedding

        self.multi_attention = MultiHeadAttention(self.num_attention_heads, self.size_attention_head, name=prefix + f"multi_attention_head")
        self.dense_attention = Dense(self.d_attention, name=prefix+ f"dense_attention", activation=activation)

        self.bn_dense_attention = BatchNormalization(name=self.prefix + "bn_dense_attention")
        self.bn_attention = BatchNormalization(name=self.prefix + "bn_attention")

        self.average_pooling = GlobalAveragePooling1D(name=prefix + "average_pooling")

    def split_heads(self, x, batch_size):
        x = tf.reshape(x, (batch_size, -1, self.num_attention_heads, self.size_attention_heads))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, x, mask_inp, states):
        batch_size = tf.shape(x)[0]
        timestep_size = tf.shape(x)[1]

        #extract objects from observation
        x_self, x_lidar, agent, objects = tf.split(x, [8, self.len_lidar, 6, self.num_objects * 4], axis=-1)
        objects = tf.split(objects, self.num_objects, axis=-1)

        #Create mask for self attention
        mask_batch = batch_size * timestep_size
        len_seq = 2 + self.num_objects
        attention_mask = [tf.ones((mask_batch, 1)),
                tf.expand_dims(tf.reduce_sum(tf.abs(tf.reshape(agent, [mask_batch, -1])), axis=-1), -1)]
        for obj in objects:
            attention_mask.append(tf.expand_dims(tf.reduce_sum(tf.abs(tf.reshape(obj, [mask_batch, -1])), axis=-1), -1))

        attention_mask_vec = tf.cast(tf.clip_by_value(tf.concat(attention_mask, -1), clip_value_min=0,
                                     clip_value_max=1.), dtype=tf.uint8)
        attention_mask_row = tf.reshape(tf.expand_dims(tf.tile(attention_mask_vec, [1, len_seq]), -1), [-1, len_seq, len_seq])
        attention_mask_col = tf.linalg.matrix_transpose(attention_mask_row)
        attention_mask = tf.cast(attention_mask_row * attention_mask_col, dtype=tf.bool)

        #Perform circular 1D convolution on lidar data
        x_lidar = tf.expand_dims(tf.concat([x_lidar[:, :, :1], x_lidar,
                            x_lidar[:, :, -1:]], axis=-1), -1)

        x_lidar = tf.reshape(self.circular_conv(x_lidar), [-1, timestep_size, 1, self.len_lidar + 2])

        x_self = tf.concat([tf.expand_dims(x_self, axis=2), x_lidar], axis=-1)

        #Perform embeddings
        self_embed = self.self_embedding(x_self)
        self_embed = self.self_emb_bn(self_embed)
        agent_embed = self.agent_embedding(tf.concat([x_self, tf.expand_dims(agent, axis=2)], axis=-1))
        agent_embed = self.agent_emb_bn(agent_embed)
        objects = [self.object_embedding(tf.concat([x_self, tf.expand_dims(o, axis=2)], axis=-1)) for o in objects]
        objects = [self.obj_emb_bn(o) for o in objects]

        concat_embed = tf.reshape(tf.concat([self_embed, agent_embed] + objects, axis=2), [mask_batch, -1, self.h_embedding])

        # Self attention module
        out_attention = self.multi_attention(concat_embed, concat_embed, attention_mask=attention_mask) + concat_embed
        out_attention = self.bn_attention(out_attention)

        out_attention = self.dense_attention(out_attention) + out_attention
        out_attention = self.bn_dense_attention(out_attention)

        average_embedding = self.average_pooling(out_attention, mask=attention_mask_vec)
        average_embedding = tf.reshape(average_embedding, [batch_size, timestep_size, -1])

        average = self.dense(average_embedding)
        average = self.dense_bn(average)

        states_splitted = tf.split(states, 2, axis=-1)

        out_lstm, h, c = self.lstm(average, initial_state=states_splitted, mask=mask_inp)
        out_lstm = self.lstm_bn(out_lstm)

        new_states = tf.concat([h, c], axis=-1)

        return out_lstm, new_states

@register("lstm_network", True)
class LSTMLayer(Layer):

    def __init__(self, init_k='glorot_uniform', h_dense=32,
                 h_lstm=32, activation="relu", prefix="agent_", **kwargs):
        super(LSTMLayer, self).__init__(name=prefix + "lstm_layer", **kwargs)

        self.activation = activation
        self.dense = Dense(int(h_dense), name=prefix + "dense")
        self.h_lstm = int(h_lstm)
        self.lstm = LSTM(int(h_lstm), return_state=True, return_sequences=True, kernel_initializer=init_k,
                         name=prefix + "lstm")

    def call(self, x, mask_inp, states):
        x = self.dense(x)
        x = Activation(self.activation)(x)

        states_splitted = tf.split(states, 2, axis=-1)

        x, h, c = self.lstm(x, initial_state=states_splitted, mask=mask_inp)

        new_states = tf.concat([h, c], axis=-1)

        return x, new_states


@register_layer("LambdaNeglopLayer")
class LambdaNeglogpLayer(Layer):

    def build(self, input_shape):
        self.inp_shape = input_shape
        super(LambdaNeglogpLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, y):
        pi, x = y
        x = tf.cast(x, tf.int32)
        # one-hot encoding
        x = tf.one_hot(x, pi.get_shape().as_list()[-1])
        return tf.expand_dims(tf.nn.softmax_cross_entropy_with_logits(
            logits=pi,
            labels=x, axis=-1), axis=-1)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask

    def compute_output_shape(self, inp):
        return (*inp[0][:-1], 1)


@register_layer("LambdaSampleLayer")
class LambdaSampleLayer(Layer):

    def build(self, input_shape):
        super(LambdaSampleLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        u = tf.random.uniform(tf.shape(x), dtype=x.dtype) * 0.3
        return tf.argmax(x - tf.math.log(-tf.math.log(u)), axis=-1)

    def compute_output_shape(self, inp):
        return (*inp[:-1], 1)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask


@register_layer("LambdaMultiNeglopLayer")
class LambdaMultiNeglogpLayer(Layer):

    def build(self, input_shape):
        super(LambdaMultiNeglogpLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        return tf.add_n(x)

    def compute_output_shape(self, inp):
        return (*inp[0][:-1], 1)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask


@register_layer("LambdaMultiSampleLayer")
class LambdaMultiSampleLayer(Layer):

    def __init__(self, ncat, **kwargs):
        self.ncat = ncat
        super(LambdaMultiSampleLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(LambdaMultiSampleLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        return tf.cast(tf.stack(x, axis=-1), tf.int32)

    def compute_output_shape(self, inp):
        return (*inp[0][:-1], self.ncat)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask

    def get_config(self):
        config = {
            'ncat': self.ncat
        }
        base_config = super(LambdaMultiSampleLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


def categorical_sample(pi, prefix, name=1):
    sample = LambdaSampleLayer(name=prefix + "cat_sample_" + str(name))(pi)
    return sample


def categorical_neglogp(pr_inp, inp_action, prefix, name=1):
    neglop = LambdaNeglogpLayer(name=prefix + "cat_neglop_" + str(name))([pr_inp, inp_action])
    return neglop


def multicategorical_sample(pi, action_space, prefix, name=1):
    pi_list = tf.split(pi, np.array(action_space.nvec, dtype=np.int32), axis=-1)
    list_sample = [categorical_sample(p, prefix, name + i) for i, p in enumerate(pi_list)]

    return LambdaMultiSampleLayer(len(action_space.nvec),
                                  name=prefix + "multi_cat_sample_" + str(name))(list_sample)


def multicategorical_neglogp(pi, inp_action, action_space, output_name, prefix, name=1):
    pi_list = tf.split(pi, np.array(action_space.nvec, dtype=np.int32), axis=-1)
    inp_action_list = tf.split(inp_action, action_space.shape[0], axis=-1)

    neglogp_list = [categorical_neglogp(p, a, prefix, name + i) for i, (p, a) in
                    enumerate(zip(pi_list, inp_action_list))]

    return LambdaMultiNeglogpLayer(name=output_name)(neglogp_list)


def multicategorical_top(inp_action, action_space, dense_pi, net_top, prefix, infer=False):
    pi = dense_pi(net_top)
    # tf.print("pi shape : ", pi.shape)
    sample = multicategorical_sample(pi, action_space, prefix)
    # tf.print("sample :", sample)
    neglogp = multicategorical_neglogp(pi, sample,
                                       action_space, prefix + "multi_cat_neglop_sample", prefix, name=1)

    # tf.print("inp action :", sample)
    if not infer:
        neglogp_train = multicategorical_neglogp(pi, inp_action,
                                                 action_space, prefix + "multi_cat_neglop", prefix, name=5)
    else:
        neglogp_train = None

    return sample, neglogp, neglogp_train


def reduce_mask(value, mask):
    return tf.reduce_sum(value) / tf.reduce_sum(mask)


class Model_PPO(Model):

    def __init__(self, input_shape, network_tuple, action_space, nsteps,
                 prefix="agent_", lr=5e-4, **net_args):

        super(Model_PPO, self).__init__()
        NetworkLayer, recurrent = network_tuple

        init_k = 'glorot_uniform'
        self.gradient_clipping = 5.0

        self.recurrent = recurrent
        self.action_space = action_space
        self.prefix = prefix
        self.nsteps = nsteps

        init_k = Orthogonal(gain=0.1)
        self.network = NetworkLayer(init_k=init_k, prefix=prefix, **net_args)
        self.network_val = NetworkLayer(init_k=init_k, prefix=prefix + "val_", **net_args)

        if self.recurrent:
            self.initial_state = lambda nenv: np.zeros((nenv, self.network.h_lstm * 2), dtype=np.float32)
            self.input_shape_init = (nsteps, *input_shape)
        else:
            self.initial_state = lambda nenv: 0
            self.input_shape_init = input_shape

        if isinstance(action_space, spaces.Box):
            raise NotImplementedError
            assert len(action_space.shape) == 1
            self.dense_pi = Dense(action_space.shape[0], name=prefix + "logits",
                       kernel_initializer=keras.initializers.Orthogonal(gain=np.sqrt(2)))(net_top_infer)
            raise NotImplementedError
        elif isinstance(action_space, spaces.Discrete):
            raise NotImplementedError
            self.preds_dim = action_space.n
            self.dense_pi = Dense(action_space.n, name=prefix + "logits",
                             kernel_initializer=keras.initializers.Orthogonal(gain=np.sqrt(2)),
                             activation="softmax")

        elif isinstance(self.action_space, spaces.MultiDiscrete):
            self.preds_dim = action_space.nvec.astype('int32').sum()
            self.action_dim = len(action_space.nvec)

            self.dense_pi = Dense(self.preds_dim, name=prefix + "logits",
                                  kernel_initializer=init_k)

        self.dense_vpred = Dense(1, name=prefix + "vpred", kernel_initializer=Orthogonal(gain=1))

        # Cliprange
        self.cliprange = 0.2
        self.cliprange_tensor = tf.constant([0.2])

        self.optimizer = optimizers.Adam(lr=lr, global_clipnorm=self.gradient_clipping)
        self.optimizer_v = optimizers.Adam(lr=1e-3, global_clipnorm=self.gradient_clipping)
        self.loss_names = [prefix + "multi_cat_neglogp_loss", prefix + "multi_cat_neglogp_ratio",
                           prefix + "multi_cat_neglogp_approxkl", prefix + "multi_cat_neglogp_clipfrac",
                           prefix + 'vpred_loss']

    def warm_up(self, batch_size):
        """
        This function is used to initialize the model and its weights it should be called after init
        """
        inp = tf.zeros((batch_size, *self.input_shape_init))
        mask = tf.ones((batch_size, 1)) if not self.recurrent else tf.ones((batch_size, self.nsteps, 1))
        inp_action = tf.zeros((batch_size, self.action_dim)) if not self.recurrent else tf.ones(
            (batch_size, self.nsteps,
             self.action_dim))
        states = tf.zeros((batch_size,)) if not self.recurrent else self.initial_state(batch_size)
        states_val = tf.zeros((batch_size,)) if not self.recurrent else self.initial_state(batch_size)
        self.__call__(inp, mask, inp_action, states, states_val)
        self.summary()

    def call(self, x, mask, inp_action, inp_states, inp_states_val, training=True):
        if not self.recurrent:
            net_top = self.network(x)
            net_top_val = self.network_val(x)

            states = tf.constant(0)
            states_val = tf.constant(0)
        else:
            squeezed_mask = tf.cast(tf.squeeze(mask, [-1]), dtype=tf.bool)

            net_top, states = self.network(x, squeezed_mask, inp_states)
            net_top_val, states_val = self.network_val(x, squeezed_mask, inp_states_val)

        if isinstance(self.action_space, spaces.MultiDiscrete):

            sample, neglogp, neglogp_train = multicategorical_top(inp_action,
                                                                  self.action_space, self.dense_pi, net_top,
                                                                  self.prefix)
        else:
            raise NotImplementedError

        vpred = self.dense_vpred(net_top_val)

        return neglogp, neglogp_train, sample, vpred, states, states_val

    def call_train_pi(self, x, mask, inp_action, inp_states):
        if not self.recurrent:
            net_top = self.network(x)
        else:
            squeezed_mask = tf.cast(tf.squeeze(mask, [-1]), dtype=tf.bool)
            net_top, _ = self.network(x, squeezed_mask, inp_states)

        if isinstance(self.action_space, spaces.MultiDiscrete):

            sample, neglogp, neglogp_train = multicategorical_top(inp_action,
                                                                  self.action_space, self.dense_pi, net_top,
                                                                  self.prefix)
        else:
            raise NotImplementedError

        return neglogp_train

    def call_train_value(self, x, mask, inp_states_val):
        if not self.recurrent:
            net_top_val = self.network_val(x)
        else:
            squeezed_mask = tf.cast(tf.squeeze(mask, [-1]), dtype=tf.bool)
            net_top_val, _ = self.network_val(x, squeezed_mask, inp_states_val)

        vpred = self.dense_vpred(net_top_val)

        return vpred

    def get_input_signatures(self):
        mask_shape = (None, 1) if not self.recurrent else (None, self.nsteps, 1)
        imp_action_shape = (None, self.action_dim) if not self.recurrent else (None, self.nsteps, self.action_dim)
        state_shape = None if not self.recurrent else (None, self.initial_state(1).shape[-1])

        return (
            tf.TensorSpec((None, *self.input_shape_init), tf.float32), tf.TensorSpec(mask_shape, tf.float32),
            tf.TensorSpec(imp_action_shape, tf.float32),
            tf.TensorSpec(mask_shape, tf.float32), tf.TensorSpec(mask_shape, tf.float32),
            tf.TensorSpec(mask_shape, tf.float32),
            tf.TensorSpec(mask_shape, tf.float32), tf.TensorSpec(state_shape, tf.float32),
            tf.TensorSpec(state_shape, tf.float32))

    def get_train_function(self):
        inp_signatures = self.get_input_signatures()

        @tf.function(input_signature=inp_signatures)
        def train_fun(x, mask, inp_action, old_pred, old_vpred, adv, returns, inp_states, inp_states_val):
            return self._train_step(x, mask, inp_action, old_pred, old_vpred, adv, returns, inp_states, inp_states_val)

        return train_fun

    def _train_step(self, x, mask, inp_action, old_pred, old_vpred, adv, returns, inp_states, inp_states_val):

        with tf.GradientTape() as tape:
            neglogp_train = self.call_train_pi(x, mask, inp_action, inp_states)
            loss_neglogp = self.loss_fn(neglogp_train, old_pred, adv, mask)

        gradients = tape.gradient(loss_neglogp, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        with tf.GradientTape() as tape:
            vpred = self.call_train_value(x, mask, inp_states_val)
            loss_vf = self.vf_loss_fn(returns, vpred, old_vpred, mask)

        gradients_v = tape.gradient(loss_vf, self.trainable_variables)
        self.optimizer_v.apply_gradients(zip(gradients_v, self.trainable_variables))

        ratio = self.ratio(neglogp_train, old_pred, mask)
        approxkl = self.approxkl(neglogp_train, old_pred, mask)
        clipfrac = self.clipfrac(neglogp_train, old_pred, mask)

        return [loss_neglogp, ratio, approxkl, clipfrac, loss_vf]

    def vf_loss_fn(self, y_true, y_pred, old_vpred, mask):

        # vpredclipped = old_vpred + tf.clip_by_value(y_pred - old_vpred, - self.cliprange, self.cliprange)
        # Unclipped value
        vf_losses1 = tf.square(y_pred - y_true)
        # Clipped value
        # vf_losses2 = tf.square(vpredclipped - y_true)

        vf_loss = vf_losses1  # tf.maximum(vf_losses1, vf_losses2)

        mask = tf.cast(mask, tf.float32)
        vf_loss = vf_loss * mask

        return reduce_mask(vf_loss, mask)

    def ratio(self, neglog_pred, neglog_old_pred, mask):

        # Calculate ratio (pi current policy / pi old policy)
        r = tf.exp(neglog_old_pred - neglog_pred)

        mask = tf.cast(mask, tf.float32)
        r = r * mask

        return reduce_mask(r, mask)

    def approxkl(self, neglog_pred, neglog_old_pred, mask):

        appkl = kullback_leibler_divergence(neglog_old_pred, neglog_pred)

        mask = tf.cast(mask, tf.float32)
        appkl = appkl * tf.squeeze(mask, [-1])

        return reduce_mask(appkl, mask)

    def approxent(self, neglog_pred, mask):

        mask = tf.cast(mask, tf.float32)
        ent = -neglog_pred * tf.squeeze(mask, [-1])

        return reduce_mask(ent, mask)

    def clipfrac(self, neglog_pred, neglog_old_pred, mask):
        r = self.ratio(neglog_pred, neglog_old_pred, mask)
        clfrac = tf.cast(tf.greater(tf.abs(r - 1.0), self.cliprange_tensor), tf.float32)

        mask = tf.cast(mask, tf.float32)
        clfrac = clfrac * mask

        return reduce_mask(clfrac, mask)

    # Creating the loss function
    def loss_fn(self, neglog_pred, neglog_old_pred, adv, mask):
        # CALCULATE THE LOSS

        # Calculate ratio (pi current policy / pi old policy)
        ratio = tf.exp(neglog_old_pred - neglog_pred)

        # min_adv = tf.where(adv > 0, (1 + self.cliprange) * adv, (1 - self.cliprange) * adv)
        clip_ratio = tf.clip_by_value(ratio, clip_value_min=1 - self.cliprange, clip_value_max=1 + self.cliprange)

        # Final PG loss
        pg_loss = tf.minimum(ratio * adv, clip_ratio * adv)
        mask = tf.cast(mask, tf.float32)
        pg_loss = pg_loss * mask

        return -reduce_mask(pg_loss, mask)
