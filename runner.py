import time

import numpy as np
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def sf01(arr):
    """
    swap and then flatten axes 0 and 1
    """
    if isinstance(arr, list):
        return [sf01(a) for a in arr]
    else:
        s = arr.shape
        return arr.swapaxes(0, 1).reshape(s[0] * s[1], *s[2:])


class Episode(object):
    obs = []
    rewards = []
    states = []
    states_val = []
    terminal = []
    actions = []
    neglogp = []
    values = []

    def __init__(self, num_agent, obs_init):
        assert isinstance(num_agent, int) and num_agent >= 1

        self.num_agent = num_agent
        self.reset(obs_init)

    def reset(self, obs_init):
        self.obs = [[obs] for obs in obs_init]
        self.rewards = [[] for _ in range(self.num_agent)]
        self.states = [[] for _ in range(self.num_agent)]
        self.states_val = [[] for _ in range(self.num_agent)]
        self.terminal = []
        self.actions = [[] for _ in range(self.num_agent)]
        self.neglogp = [[] for _ in range(self.num_agent)]
        self.values = [[] for _ in range(self.num_agent)]

    def len(self):
        if len(self.rewards) == 0:
            return 0

        return len(self.rewards[0])

    def __len__(self):
        return self.len()


class Runner(object):

    def __init__(self, env, gamma, lam, rollout_size, num_agent, timestep_size):

        self.obs = env.reset()

        self.mb_names = ["mb_obs", "mb_rewards", "mb_actions", "mb_values",
                         "mb_neglogp"]
        self.states = None
        self.states_val = None
        self.states_init = False
        self.gamma = gamma
        self.lam = lam
        self.nenv = env.num_envs if hasattr(env, 'num_envs') else 1
        self.nsteps = rollout_size
        self.timestep_size = timestep_size
        self.env = env
        self.dones = np.array([False for _ in range(self.nenv)], dtype=np.bool)
        self.episodes = [[] for _ in range(self.nenv)]
        self.num_agent = num_agent
        self.cur_episode = [Episode(num_agent, [self.obs[j][i, :] for j in range(num_agent)]) for i in range(self.nenv)]
        self.obs_dim = [obs.shape[-1] for obs in self.obs]
        self.episodes_added = 0

    def finish_episode(self, idx_env, step):
        if step == self.env.max_timesteps and len(self.cur_episode[idx_env]) > 0:
            self.cur_episode[idx_env].terminal[-1] = False

        self.episodes[idx_env].append(self.cur_episode[idx_env])
        self.episodes_added += 1
        for idx_a in range(self.num_agent):
            if isinstance(self.states[idx_a], np.ndarray) and len(self.states[idx_a].shape) > 1:
                self.states[idx_a][idx_env, :] = 0
                self.states_val[idx_a][idx_env, :] = 0

        self.cur_episode[idx_env].reset([self.obs[idx_a][idx_env] for idx_a in range(self.num_agent)])

        for idx_a in range(self.num_agent):
            if isinstance(self.states[idx_a], np.ndarray) and len(self.states[idx_a].shape) > 1:
                self.cur_episode[idx_env].states[idx_a].append(self.states[idx_a][idx_env])
                self.cur_episode[idx_env].states_val[idx_a].append(self.states_val[idx_a][idx_env])
            else:
                self.cur_episode[idx_env].states[idx_a].append(self.states[idx_a])
                self.cur_episode[idx_env].states_val[idx_a].append(self.states_val[idx_a])

    def split_episode(self, obs, advs, returns, values, neglogp, actions, states, states_val):

        len_ep = len(obs)
        batch_elements = []

        for i in range((len_ep - 1) // self.timestep_size + 1):
            start = i * self.timestep_size
            end = min((i + 1) * self.timestep_size, len_ep)
            l = end - start

            b_obs = np.zeros((self.timestep_size, obs.shape[1]))
            b_obs[:l] = obs[start:end]

            b_states = states[start]
            b_states_val = states_val[start]

            b_advs = np.zeros((self.timestep_size, 1))
            b_advs[:l, 0] = advs[start:end]

            b_returns = np.zeros((self.timestep_size, 1))
            b_returns[:l, 0] = returns[start:end]

            b_actions = np.zeros((self.timestep_size, actions.shape[-1]))
            b_actions[:l] = actions[start:end]

            b_neglogp = np.zeros((self.timestep_size, 1))
            b_neglogp[:l] = neglogp[start:end]

            b_values = np.zeros((self.timestep_size, 1))
            b_values[:l, 0] = values[start:end]

            batch_elements.append((b_obs, b_advs, b_returns, b_values, b_neglogp, b_actions, b_states, b_states_val))

        return batch_elements

    def process_episodes(self, policies):
        print("episodes added {}".format(self.episodes_added))
        mb_states = [[] for _ in range(self.num_agent)]
        mb_states_val = [[] for _ in range(self.num_agent)]
        mb_returns = [[] for _ in range(self.num_agent)]
        mb_advs = [[] for _ in range(self.num_agent)]
        mb_actions = [[] for _ in range(self.num_agent)]
        mb_neglogp = [[] for _ in range(self.num_agent)]
        mb_values = [[] for _ in range(self.num_agent)]
        mb_obs = [[] for _ in range(self.num_agent)]
        mb_mask = []

        num_episode = 0
        episodes_len = 0
        for idx_env in range(self.nenv):
            for episode in self.episodes[idx_env]:
                episode_len = len(episode)
                if episode_len == 0:
                    continue

                num_episode += 1
                episodes_len += episode_len
                for i in range((episode_len - 1) // self.timestep_size + 1):
                    start = i * self.timestep_size
                    end = min((i + 1) * self.timestep_size, episode_len)
                    l = end - start
                    mask = np.zeros((self.timestep_size, 1))
                    mask[:l] = 1

                    mb_mask.append(mask)

                for idx_a in range(self.num_agent):
                    ep_states = np.asarray(episode.states[idx_a])[:-1]
                    ep_states_val = np.asarray(episode.states_val[idx_a])[:-1]

                    ep_values = np.zeros(episode_len + 1)
                    ep_values[:-1] = episode.values[idx_a]

                    ep_advs = np.zeros((episode_len,))
                    ep_returns = np.zeros((episode_len + 1,))

                    if episode.terminal[-1]:
                        ep_values[-1] = 0
                    else:
                        _, v, _ = self.sample_unique_policies(policies[idx_a], episode.obs[idx_a][-1][np.newaxis, :],
                                                              episode.states[idx_a][-1][np.newaxis, :],
                                                              episode.states_val[idx_a][-1][np.newaxis, :])
                        ep_values[-1] = v

                    ep_returns[-1] = ep_values[-1]
                    for i in reversed(range(episode_len)):
                        r = episode.rewards[idx_a][i]
                        next_v = ep_values[i + 1]
                        cur_v = ep_values[i]
                        diff = r + self.gamma * next_v - cur_v
                        if i == episode_len - 1:
                            ep_advs[i] = diff
                        else:
                            ep_advs[i] = diff + self.gamma * self.lam * ep_advs[i + 1]
                        ep_returns[i] = r + self.gamma * ep_returns[i + 1]

                    ep_returns = ep_returns[:-1]
                    ep_obs = np.asarray(episode.obs[idx_a][:-1])
                    ep_values = ep_values[:-1]

                    splitted = self.split_episode(ep_obs, ep_advs, ep_returns, ep_values,
                                                  np.asarray(episode.neglogp[idx_a]),
                                                  np.asarray(episode.actions[idx_a]), ep_states,
                                                  ep_states_val)

                    for b_obs, b_advs, b_returns, b_values, b_neglogp, b_actions, b_states, b_states_val in splitted:
                        mb_obs[idx_a].append(b_obs)
                        mb_advs[idx_a].append(b_advs)
                        mb_returns[idx_a].append(b_returns)
                        mb_values[idx_a].append(b_values)
                        mb_neglogp[idx_a].append(b_neglogp)
                        mb_actions[idx_a].append(b_actions)
                        mb_states[idx_a].append(b_states)
                        mb_states_val[idx_a].append(b_states_val)

        mb_obs = [np.asarray(obs, dtype=self.obs[0].dtype) for obs in mb_obs]
        mb_returns = [np.asarray(rew, dtype=np.float32) for rew in mb_returns]
        mb_advs = [np.asarray(a, dtype=np.float32) for a in mb_advs]
        mb_actions = [np.asarray(actions) for actions in mb_actions]
        mb_neglogp = [np.asarray(neglogp, dtype=np.float32) for neglogp in mb_neglogp]
        mb_values = [np.asarray(values, dtype=np.float32) for values in mb_values]
        mb_states = [np.asarray(states, dtype=np.float32) for states in mb_states]
        mb_states_val = [np.asarray(states, dtype=np.float32) for states in mb_states_val]
        mb_mask = np.stack(mb_mask, axis=0)

        print("num episode done {} - mean len {}".format(num_episode, episodes_len / num_episode))
        return mb_obs, mb_returns, mb_advs, mb_actions, mb_neglogp, mb_values, mb_states, mb_states_val, mb_mask

    def sample_unique_policies(self, policy, obs, states, states_val):

        obs_filled = np.ones((self.nenv, 1, obs.shape[-1]), dtype=np.float32) * -1
        if policy.model.recurrent:

            obs_filled[:, 0, :] = obs

            actions, values, neglogp, _, _ = policy.step(obs_filled,
                                                         np.repeat(states, self.nenv, axis=0),
                                                         np.repeat(states_val, self.nenv, axis=0),
                                                         mask=np.ones((self.nenv, 1, 1),
                                                                      dtype=np.bool))

            actions = actions[:, 0, :]
            values = values[:, 0, :]
            neglogp = neglogp[:, 0, :]
        else:
            actions, values, neglogp, _, _ = policy.step(obs,
                                                         states, states_val,
                                                         mask=np.ones((self.nenv, 1)))

        return actions[0], values[0], neglogp[0]

    def sample_policies(self, policies, save_new_state=True):

        actions = []
        values = []
        neglogp = []
        states = [[] for _ in range(self.num_agent)]
        states_val = [[] for _ in range(self.num_agent)]

        obs_filled = [np.ones((self.nenv, 1, obs.shape[-1]), dtype=np.float32) * -1 for obs in self.obs]
        for idx, p in enumerate(policies):
            if p.model.recurrent:

                obs_filled[idx][:, 0, :] = self.obs[idx]

                a, v, n, states[idx], states_val[idx] = p.step(obs_filled[idx],
                                                               self.states[idx], self.states_val[idx],
                                                               mask=np.ones((self.nenv, 1, 1),
                                                                            dtype=np.bool))

                a = a[:, 0, :]
                v = v[:, 0, :]
                n = n[:, 0, :]
            else:
                a, v, n, states[idx], states_val[idx] = p.step(self.obs[idx],
                                                               self.states[idx], self.states_val[idx],
                                                               mask=np.ones((self.nenv, 1)))
            actions.append(a)
            values.append(v)
            neglogp.append(n)

        if save_new_state:
            self.states = states
            self.states_val = states_val

        return actions, values, neglogp

    def compute_steps(self, policies, idx_avoid=None):

        if not self.states_init:
            self.states = [p.model.initial_state(self.nenv) for p in policies]
            self.states_val = [p.model.initial_state(self.nenv) for p in policies]

            for idx_env in range(self.nenv):
                for idx_a in range(self.num_agent):
                    self.cur_episode[idx_env].states[idx_a].append(self.states[idx_a][idx_env])
                    self.cur_episode[idx_env].states_val[idx_a].append(self.states_val[idx_a][idx_env])

        self.episodes = [[] for _ in range(self.nenv)]
        self.episodes_added = 0
        # For n in range number of steps
        num_ep_done = 0
        rewards_sums = []
        epinfos = {}

        for idx_step in range(self.nsteps):

            terminal_idx = np.nonzero(self.dones == 1)[0]
            # print(idx_step, terminal_idx, self.nenv)
            for idx in terminal_idx:
                self.finish_episode(idx, idx_step)

            # Given observations, get action value and neglogpacs
            # print("sampling policies")
            actions, values, neglogp = self.sample_policies(policies, save_new_state=True)

            # # Take actions in env and look at the results
            # print("stepping env")
            # tstart = time.perf_counter()
            self.obs, rewards, self.dones, infos = self.env.step(np.asarray(actions).swapaxes(0, 1))
            # tend = time.perf_counter()
            # print(f"stepping took {tend - tstart} seconds")
            num_ep_done += np.sum(self.dones)
            for idx, d in enumerate(self.dones):
                if d:
                    rewards_sums.append(infos[idx]["rew_sum"])

            rewards = np.asarray(rewards).swapaxes(0, 1)

            for idx_env in range(self.nenv):
                self.cur_episode[idx_env].terminal.append(self.dones[idx_env])
                for idx_a in range(self.num_agent):
                    self.cur_episode[idx_env].obs[idx_a].append(self.obs[idx_a][idx_env])
                    self.cur_episode[idx_env].actions[idx_a].append(actions[idx_a][idx_env])
                    self.cur_episode[idx_env].neglogp[idx_a].append(neglogp[idx_a][idx_env])
                    self.cur_episode[idx_env].values[idx_a].append(values[idx_a][idx_env])
                    self.cur_episode[idx_env].rewards[idx_a].append(rewards[idx_a][idx_env])
                    self.cur_episode[idx_env].states[idx_a].append(self.states[idx_a][idx_env])
                    self.cur_episode[idx_env].states_val[idx_a].append(self.states_val[idx_a][idx_env])

        avg_ep_len = self.nsteps * self.nenv / num_ep_done
        epinfos["ep_len"] = avg_ep_len
        epinfos["num_ep_done"] = num_ep_done
        epinfos["avg_reward"] = np.mean(np.array(rewards_sums), axis=0)

        return epinfos

    def roll_out_all_agent(self, policies):

        assert len(policies) == self.num_agent, "len policies is differnet that num_agent"

        epinfos = self.compute_steps(policies)

        mb_obs, mb_returns, mb_advs, mb_actions, mb_neglogp, mb_values, mb_states, mb_states_val, mb_mask = self.process_episodes(
            policies)

        logger.info(epinfos["avg_reward"])
        logger.info("Num episodes done : {}".format(epinfos["num_ep_done"]))
        logger.info("Num env : {}".format(mb_mask.shape[0]))

        return (
            (mb_obs, mb_returns, mb_actions, mb_values, mb_neglogp, mb_advs, mb_mask, mb_states, mb_states_val),
            epinfos)
