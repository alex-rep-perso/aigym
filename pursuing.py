from multiagent_env import MultiAgent2DEnv
from objects import *
from register_env import register


@register("PursuingEnv")
class PursuingEnv(MultiAgent2DEnv):

    @staticmethod
    def get_default_agents_objects(height, width):
        base_speed = 80
        defender = RectangleRunner([base_speed * 2, base_speed * 3], color=(0, 0, 1, 1), defend=True, range_w=[8, 15],
                                   range_h=[8, 15],
                                   range_pos=np.array([-width / 2, -height / 2, width / 2, height / 2]))
        attacker = RectangleRunner([base_speed * 1.2, base_speed * 1.85], color=(1, 0, 0, 1), defend=False,
                                   range_w=[25, 55], range_h=[25, 55],
                                   range_pos=np.array([-width / 2, -height / 2, width / 2, height / 2]))

        offset = 30
        pos_borders = [[-width / 2 - offset, 0, -width / 2 - offset, 0],
                       [0, height / 2 + offset, 0, height / 2 + offset],
                       [width / 2 + offset, 0, width / 2 + offset, 0],
                       [0, -height / 2 - offset, 0, -height / 2 - offset]
                       ]
        borders = [
            RectangleObject(range_w=np.array([80, 80]) if i % 2 == 0 else np.array([width, width]),
                            range_h=np.array([80, 80]) if i % 2 == 1 else np.array([height, height]),
                            range_pos=pos_borders[i], mass=0) for i in range(4)]

        agents_pursuing = [defender, attacker]

        return agents_pursuing, None

    def __init__(self, agents, width, height, objects=None, seed=None, timesteps=1000,
                 start_decrease_alpha=0.1, len_decrease_alpha=0.1):

        super(PursuingEnv, self).__init__(agents, width, height, objects=objects, seed=seed, timesteps=timesteps,
                                          start_decrease_alpha=start_decrease_alpha,
                                          len_decrease_alpha=len_decrease_alpha)

        assert len(agents) == 2, "Error env accept only two agents"
        assert all(isinstance(agent, RectangleRunner) for agent in agents), "Agent must be of type SphereRunner"

        assert agents[0].defend and not agents[1].defend, "Two asymmetric defender role must be set"

        self.agents[0].marker = "defender"
        self.agents[0].target = "attacker"

        self.agents[1].marker = "attacker"
        self.agents[1].target = "defender"

        self.end_goal_reward = 2000

    def reset(self):
        self.cur_step = 0

        self.seed_state = self.seed_state + 1 if isinstance(self.seed_state, int) else None
        self.seed(seed=self.seed_state)

        obs = []

        t_remain = np.array([self.max_timesteps - self.cur_step])
        for agent in self.agents:
            obs.append(np.concatenate([agent.reset(self.width, self.heightself.all_entities), t_remain]))

        if not self.objects is None:
            for o in self.objects:
                o.reset(self.width, self.height)

        self.physics_world.clear()
        for ent in self.all_entities:
            self.physics_world.add_proxy(ent.physics_proxy)

        self.tlast_frame = None

        return tuple(obs)

    def step(self, actions):
        self.tau = 1 / 60
        self._step(actions)

        obs = []
        done = False
        reward = []  # Defender, attacker

        t_remain = np.array([self.max_timesteps - self.cur_step])

        for agent in self.agents:
            rew_agent, done_agent, ob = agent.get_reward(self.agents, self.tau)
            obs.append(np.concatenate([ob, t_remain]))
            r = self.alpha * rew_agent if agent.marker == "defender" else rew_agent * max(0.3, self.alpha)
            reward.append(r)
            done = done or done_agent

            if done_agent:
                coef = 1 if agent.marker == "attacker" else -1
                reward[-1] += 0.5 * (self.max_timesteps - self.cur_step) * (1 - self.alpha) * coef

        self.cur_step += 1

        if self.cur_step == self.max_timesteps and not done:
            done = True
            for i, a in enumerate(self.agents):
                if a.defend:
                    reward[i] += self.end_goal_reward * (1 - self.alpha)
                else:
                    reward[i] -= self.end_goal_reward * (1 - self.alpha)
        elif done:
            for i, a in enumerate(self.agents):
                if a.defend:
                    reward[i] -= self.end_goal_reward * (1 - self.alpha)
                else:
                    reward[i] += self.end_goal_reward * (1 - self.alpha)

        return obs, reward, done, {}
