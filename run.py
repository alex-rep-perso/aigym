from train import learn
from async_vector_env_multi import AsyncVectorEnv
from pursuing import PursuingEnv
from chasing_env import ChasingEnv
from multiagent_env import RewardSumWrapper
from goal_env import GoalEnv, TurretEnv
from models import get_network, Model_PPO
from policies import SimplePolicy
import sys
import numpy as np
import tornado
import register_env
import os

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    import tensorflow as tf

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    import keras
    from keras.models import model_from_json, Model, load_model


def arg_parser():
    """
    Create an empty argparse.ArgumentParser.
    """
    import argparse
    return argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)


def common_arg_parser():
    """
    Create an argparse.ArgumentParser for run_mujoco.py.
    """
    parser = arg_parser()
    parser.add_argument('--save_path', help='Path to save trained model to', default="models", type=str)
    parser.add_argument('--network', help='Path to save trained model to', default="mlp_network", type=str)
    parser.add_argument('--env', help='Env to use', choices=list(register_env.envs.keys()), required=True, type=str)
    parser.add_argument('--conf', help='Conf file pointing to worker url', default="worker.conf", type=str)
    parser.add_argument('--num_envs_remote', help='Number of parallel thread running an env', default=1, type=int)
    parser.add_argument('--seed', help='Seed', default=None, type=int)
    parser.add_argument('--start_decrease_alpha', help='percentage in the training when to start decrease alpha'
                        , required=True, type=float)
    parser.add_argument('--len_decrease_alpha', help='percentage in the training during which alpah decreases',
                        required=True, type=float)
    parser.add_argument('--frac', help='Frac use for play', default=0, type=float)
    parser.add_argument('--num_iter', help='Number of iteration for training', default=500, type=int)
    parser.add_argument('--start_iter', help='Iteration to continue training', default=None, type=int)
    parser.add_argument('--train', help='training mode', action="store_true", default=False)
    parser.add_argument('--play', default=False, action='store_true')
    return parser


def parse_unknown_args(args):
    """
    Parse arguments not consumed by arg parser into a dictionary
    """
    retval = {}
    preceded_by_key = False
    for arg in args:
        if arg.startswith('--'):
            if '=' in arg:
                key = arg.split('=')[0][2:]
                value = arg.split('=')[1]
                retval[key] = value
            else:
                key = arg[2:]
                preceded_by_key = True
        elif preceded_by_key:
            retval[key] = arg
            preceded_by_key = False

    return retval


def parse_cmdline_kwargs(args):
    '''
    convert a list of '='-spaced command-line arguments to a dictionary, evaluating python objects when possible
    '''

    def parse(v):

        assert isinstance(v, str)
        try:
            return eval(v)
        except (NameError, SyntaxError):
            return v

    return {k: parse(v) for k, v in parse_unknown_args(args).items()}


def checkAuthorizedValue(conf, head_val_list, authorizedValue):
    for head, val in head_val_list:

        if not head in authorizedValue:
            print("%s is not a valid parameter" % (head), file=sys.stderr)
            sys.exit(1)
        if "done" in authorizedValue[head] and authorizedValue[head]["done"]:
            print("%s have already been defined" % (head), file=sys.stderr)
            sys.exit(1)

        conf[head] = authorizedValue[head]["type"](val)
        authorizedValue[head]["done"] = True

    for key, val in authorizedValue.items():
        if not "default" in val and (not "done" in val or not val["done"]):
            print("Error: %s must be defined" % (key), file=sys.stderr)
            sys.exit(1)
        elif (not "done" in val or not val["done"]):
            conf[key] = val["default"]


def checkConfFile(conf, authorizedValue):
    with open(conf, 'r') as confRd:

        head_val_list = []
        for line in confRd:
            line = line.strip()
            if len(line) < 2 or ((line[0] == '[' and line[-1] == ']') or line[0] == ';'):
                continue
            try:
                head, val = line.split("=")
                head_val_list.append((head.strip(), val.strip()))
            except:
                print("config file has an incorrect syntax", file=sys.stderr)
                sys.exit(1)

    confDict = dict()
    checkAuthorizedValue(confDict, head_val_list, authorizedValue)
    return confDict


def play(env_fn, network, save_path, start_iter, frac, **net_args):
    env = env_fn()
    env.unwrapped.rendering = True
    env.unwrapped.start_decrease_alpha = 0
    env.unwrapped.end_decrease_alpha = 1
    env.set_frac(frac)

    network_tuple = get_network(network)

    remove_keywords = ["nminibatches", "noptepochs", "lr", "rollout_size_per_worker", "batch_size"]

    for key in remove_keywords:
        if key in net_args:
            del net_args[key]

    policies = []
    for i, a in enumerate(env.agents):
        input_shape = a.observation_space.shape
        model = Model_PPO(input_shape, network_tuple, a.action_space, 1,
                          prefix="agent_{}_".format(i), **net_args)
        model.warm_up(1)
        policies.append(SimplePolicy(model, save_path, "agent_%d_policy" % i))
        print(start_iter)
        if start_iter is None:
            policies[-1].load_most_recent()
        elif start_iter > 0:
            policies[-1].load_iter(start_iter)

    obs = env.reset()

    states = [p.model.initial_state(1) for p in policies]
    states_val = [p.model.initial_state(1) for p in policies]

    all_rewards = []
    max_episode = 16
    cur_episode = 1
    rewards_sums = []
    obs_filled = np.zeros((1, 1, obs[i].shape[-1]))
    while cur_episode < max_episode:
        actions = []

        for i, p in enumerate(policies):
            if p.model.recurrent:

                obs_filled[0, 0, :] = np.array([obs[i]])
                a, _, _, states[i], states_val[i] = p.step(obs_filled, states[i],
                                                           states_val[i], mask=np.ones((1, 1, 1), dtype=np.bool))
                a = a[:, 0, :]
            else:
                a, _, _, states[i], states_val[i] = p.step(np.array([obs[i]]), states[i], states_val[i])
            # a = np.ones((1, 2), dtype=np.int32)
            actions.append(a[0])

        obs, rewards, done, info = env.step(actions)
        env.render()

        all_rewards.append(np.array(rewards))

        # if all(r != 0 for r in rewards):
        # print("rewards : %f - %f" % (rewards[0], rewards[1]), end='\r')
        print("rewards : {:f}".format(rewards[0]), end='\r')
        if done:
            # print(np.array(all_rewards).reshape((len(all_rewards))))
            rewards_sums.append(np.sum(np.array(all_rewards), axis=0))
            print("sum", rewards_sums[-1])
            print("sum info", info["rew_sum"])

            for i in range(len(policies)):
                if policies[i].model.recurrent:
                    states[i] *= 0
                    states_val[i] *= 0

            cur_episode += 1
            all_rewards = []
            obs = env.reset()

    print("avg reward", np.mean(np.array(rewards_sums), axis=0))


import random


def set_global_seeds(i):
    try:
        import MPI
        rank = MPI.COMM_WORLD.Get_rank()
    except ImportError:
        rank = 0

    myseed = i + 1000 * rank if i is not None else None
    try:
        import tensorflow as tf
        tf.random.set_seed(myseed)
    except ImportError:
        pass
    np.random.seed(myseed)
    random.seed(myseed)


def main():
    parser = common_arg_parser()
    args_parsed, unknown_args = parser.parse_known_args()

    authorizedValue = {"urls": {"type": str}, "envs": {"type": str}}
    confDict = checkConfFile(args_parsed.conf, authorizedValue)

    urls = confDict["urls"].split(",")
    envs = [int(e) for e in confDict["envs"].split(",")]

    assert len(urls) == len(envs)

    width = 1280
    height = 720
    kwargs = parse_cmdline_kwargs(unknown_args)

    env_type = register_env.envs[args_parsed.env]



    timestep_size = kwargs["timestep_size"] if "timestep_size" in kwargs else 500
    if "timestep_size" in kwargs:
        del kwargs["timestep_size"]
    print(timestep_size, "timestep_size")

    def prep_env_fn(subrank=0):
        if not args_parsed.seed is None:
            s = args_parsed.seed + subrank * 10000
        else:
            s = None

        def env_fn():
            agents, objects = env_type.get_default_agents_objects(height, width)
            return RewardSumWrapper(env_type(agents, width=width, height=height, objects=objects,
                                             seed=s, start_decrease_alpha=args_parsed.start_decrease_alpha,
                                             len_decrease_alpha=args_parsed.len_decrease_alpha))

        return env_fn

    set_global_seeds(args_parsed.seed)

    # lr_float = kwargs.get("lr", 7e-4)
    # kwargs["lr"] = lambda x: lr_float * (np.exp((x-1) * 2))

    if args_parsed.train:
        num_env = np.sum(envs)
        if num_env % args_parsed.num_envs_remote != 0:
            raise ValueError("error num_envs must be divisible by num_envs_remote !")

        def env_fn_gen(num_env):
            in_series = num_env // args_parsed.num_envs_remote
            return lambda: AsyncVectorEnv([prep_env_fn(i) for i in range(num_env)], in_series=in_series)

        runners = [{"url": url, "nenvs": envs[idx], "env_fn": env_fn_gen(envs[idx])} for idx, url in enumerate(urls)]

        agents, objects = env_type.get_default_agents_objects(height, width)
        tornado.ioloop.IOLoop.current().add_callback(learn, args_parsed.network, agents, runners, timestep_size,
                                                     total_iter=args_parsed.num_iter,
                                                     start_iter=args_parsed.start_iter, save_path=args_parsed.save_path,
                                                     **kwargs)
        tornado.ioloop.IOLoop.current().start()
    if args_parsed.play:
        play(prep_env_fn(), args_parsed.network, args_parsed.save_path,
             args_parsed.start_iter, args_parsed.frac, **kwargs)


if __name__ == '__main__':
    main()
