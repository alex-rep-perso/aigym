envs = {}


def register(name):
    def _thunk(func):
        envs[name] = func
        return func

    return _thunk
