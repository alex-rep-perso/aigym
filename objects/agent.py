import time
import numpy as np
import scipy.ndimage.measurements as scipy_measurements

import pathfinding
from objects.objects import PhysicObject, SphereObject, TurretObject, RectangleObject
from gym import spaces

try:
    import rendering

    g_rendering_enabled = True
except:
    g_rendering_enabled = False
    print("Cannot import render")
import math


def distance(a, b):
    return np.sqrt(np.sum((a - b) ** 2))


class Agent(PhysicObject):
    action_space = spaces.MultiDiscrete([11, 11])  # Grid around agent of 10 * 10 with a no op action
    observation_space = None

    def __init__(self, scalar_speed: float, pos: np.ndarray = np.array([0, 0])):

        if not hasattr(self, "physics_proxy_init") or not self.physics_proxy_init:
            PhysicObject.__init__(self, pos)

        self.scalar_speed = scalar_speed
        self.actions_dict = np.array([[0, 0], [-1, 0], [1, 0], [0, 1], [0, -1]])
        self.last_pos = np.zeros((2,))
        self.last_speed = self.speed
        self._reset()
        self.np_random = None
        self.trans = None
        self._geom = None
        self._marker = None
        self.cur_move_objective = None
        self.move_queue = None

    def _reset(self):

        self.move_queue = []
        self.cur_move_objective = None
        self.dist_reducing = 0

    def reset(self, width, height, other_objects):
        raise NotImplementedError

    def set_move_objective(self):
        if self.move_queue is None or len(self.move_queue) == 0:
            return False

        self.cur_move_objective = self.move_queue.pop()
        self.speed = np.array([0, 0])
        return True

    def colliding_with(self, other_agent):
        raise NotImplementedError

    def _step(self):
        self.check_speed()
        self.last_pos[:] = self.pos
        self.pos = self.physics_proxy.get_pos()
        # self.check_coord_bound()

        self.rotation = 0  # self.physics_proxy.get_rotation()

        self.physics_proxy.set_speed(*self.speed.tolist())
        # self.physics_proxy.set_pos(*self.pos.tolist())

        if self.geom:
            self.geom.set_translation(self.pos)
            self.geom.set_scale(self.scale)
            self.geom.set_rotation(np.array([self.rotation]))

    def step(self, action):
        self.last_speed[:] = self.speed.copy()

        def adjust_offset(action, scale=2):
            if action == 0:
                return 0

            offset = action - 6
            if offset >= 0: offset += 1

            return offset * scale

        if action[0] != 0 or action[1] != 0:
            target = np.array([adjust_offset(action[0]), adjust_offset(action[1])], dtype=np.float32)
            target += self.pos

            self.cur_move_objective = target
            self.speed = np.array([0, 0])

        self._step()

    # Return reward, done, obs
    def get_reward(self, tau, other_agents=None):
        raise NotImplementedError

    def _set_speed(self):
        self.speed = self.cur_move_objective - self.pos
        self.speed = self.speed / np.linalg.norm(self.speed) * self.scalar_speed

    def check_speed(self):
        if self.cur_move_objective is None:
            self.speed = np.array([0, 0])
        elif distance(self.cur_move_objective, self.pos) < 2:
            if len(self.move_queue) > 0:
                self.cur_move_objective = self.move_queue.pop()
                self._set_speed()
            else:
                self.cur_move_objective = None
        elif len(self.physics_proxy.get_colliding_ids()) > 0:
            self._set_speed()
        elif np.all(self.speed == 0):
            self._set_speed()


class SphereAgent(Agent, SphereObject):

    def __init__(self, scalar_speed, radius, pos=np.array([0, 0]), color=(0, 0, 0, 1)):
        Agent.__init__(self, scalar_speed, pos)

        self.radius = radius
        self.color = color

    def reset(self, width, height, other_objects):
        raise NotImplementedError


class RectangleAgent(Agent, RectangleObject):

    def __init__(self, scalar_speed, w, h, pos=np.array([0, 0]),
                 color=(0, 0, 0, 1)):
        Agent.__init__(self, scalar_speed, pos)

        self.w = w
        self.h = h
        self.color = color

    def reset(self, width, height, other_objects):
        raise NotImplementedError


class PathfindingAgent(Agent):

    def __init__(self, scalar_speed: float, pos: np.ndarray = np.array([0, 0]),
                 big_grid_size: int = 5, small_grid_size: int = 5):
        assert big_grid_size % 2 == 1
        assert small_grid_size % 2 == 1
        Agent.__init__(self, scalar_speed, pos)
        self.action_space = spaces.MultiDiscrete([big_grid_size, big_grid_size, small_grid_size, small_grid_size])

        self.big_grid_size: int = big_grid_size
        self.small_grid_size: int = small_grid_size
        self.pathfinder = None
        self.cur_step = 0

    def step(self, action):
        self.last_speed[:] = self.speed.copy()

        action_big_grid = action[:2] - (self.big_grid_size - 1) // 2
        action_small_grid = action[2:] - (self.small_grid_size - 1) // 2

        if self.cur_step == 3 and (any(action_big_grid != 0) or any(action_small_grid != 0)):
            self.cur_step = -1
            cur_col, cur_row = self.pathfinder.to_cell_coord(self.pos[0], self.pos[1])

            big_grid_col = cur_col + self.small_grid_size * action_big_grid[0]
            big_grid_row = cur_row + self.small_grid_size * action_big_grid[1]

            small_grid_col = big_grid_col + action_small_grid[0]
            small_grid_row = big_grid_row + action_small_grid[1]
            # print(cur_col, cur_row)
            # print(big_grid_col, big_grid_row)
            # print(small_grid_col, small_grid_row)
            # tstart = time.perf_counter()
            path = self.pathfinder.get_path_to(cur_col, cur_row, small_grid_col, small_grid_row, force_start=True)
            # tend = time.perf_counter()
            # print(f"pathfinding took {tend - tstart} seconds")
            if path:
                del path[-1]
                move_queue = []
                for cItem in path:
                    x, y = self.pathfinder.to_world_coord(cItem[0], cItem[1])
                    move_queue.append(np.array([x, y]))

                self.move_queue = move_queue
                self.set_move_objective()

        self._step()
        self.cur_step += 1

    def _reset_pos(self, spawn_key: int = pathfinding.SPAWN_DEFENDER):
        self.cur_step = 0
        structure = np.ones((3, 3), dtype=np.int)
        spawn_map = (self.pathfinder.grid_pathfinding == spawn_key).astype(np.uint)
        labels, ncomp = scipy_measurements.label(spawn_map, structure)

        # if ncomp == 0:
        #     self.pos = np.array([0,0])
        #     print("No spawn detected for agent")

        assert ncomp > 0, "No spawn detected for agent"
        comp = self.np_random.randint(1, ncomp + 1)

        points = np.stack(np.where(labels == comp), axis=1)

        point_idx = self.np_random.choice(list(range(0, points.shape[0])))
        point = points[point_idx]

        self.pos = np.array(list(self.pathfinder.to_world_coord(point[1], point[0])))


class PathRectangleAgent(PathfindingAgent, RectangleObject):

    def __init__(self, scalar_speed, w, h, pos=np.array([0, 0]),
                 color=(0, 0, 0, 1), big_grid_size: int = 5, small_grid_size: int = 5,
                 range_w=np.array([10, 40]), range_h=np.array([10, 40])):
        RectangleObject.__init__(self, w=w, h=h, pos=pos, color=color, range_w=range_w, range_h=range_h)
        PathfindingAgent.__init__(self, scalar_speed, pos, big_grid_size=big_grid_size,
                                  small_grid_size=small_grid_size)

    def reset(self, width, height, other_objects):
        self._reset_pos()

        self.w = self.np_random.uniform(low=self.range_w[0], high=self.range_w[1])
        self.h = self.np_random.uniform(low=self.range_h[0], high=self.range_h[1])

        if self.geom:
            self.scale = np.array([self.w, self.h]) * 0.5
            self.geom.set_scale(self.scale)
            self.geom.set_translation(self.pos)

        self.size[:] = [self.w, self.h]
        self.speed = np.zeros((2,))

        self.physics_proxy.reset()
        self.physics_proxy.set_width(self.w, self.h)
        self.physics_proxy.set_pos(*self.pos.tolist())
        self.physics_proxy.set_speed(*self.speed.tolist())


class PathRectangleRunner(PathRectangleAgent):

    def __init__(self, scalar_speed, w=1, h=1, pos=np.array([0, 0]), target=None, defend=False,
                 color=(0, 0, 0, 1), big_grid_size: int = 5, small_grid_size: int = 5,
                 range_w=np.array([10, 40]), range_h=np.array([10, 40]), range_scalar_speed=np.array([5, 10])):
        PathRectangleAgent.__init__(self, scalar_speed, w, h, pos=pos,
                                    color=color, big_grid_size=big_grid_size, small_grid_size=small_grid_size,
                                    range_w=range_w, range_h=range_h)

        self.colliding_target = False
        self._target = target
        self.range_scalar_speed = range_scalar_speed
        self.defend = defend
        high = np.array([np.inf] * (8 + 30 + 6 + 16 * 4))
        low = -high
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        # Observation are pos, speed, size for agent and pos,size for objects
        # first observation is self + timestep + can output action and lidar (36 observation), second is other agent
        # and 16 objects (object and agent not seen are masked away)
        # then we have timestep remaining

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, mark):
        self._target = mark

    def _get_target(self, other_objects):
        target_object = None
        for a in other_objects:
            if self.target == a.marker:
                target_object = a
                break

        if target_object is None:
            raise ValueError("target_object should not be none")

        return target_object

    def reset(self):

        if self.defend:
            self._reset_pos(pathfinding.SPAWN_DEFENDER)
        else:
            self._reset_pos(pathfinding.SPAWN_ATTACKER)

        self.w = self.np_random.uniform(low=self.range_w[0], high=self.range_w[1])
        self.h = self.np_random.uniform(low=self.range_h[0], high=self.range_h[1])

        if self.geom:
            self.scale = np.array([self.w, self.h]) * 0.5
            self.geom.set_scale(self.scale)
            self.geom.set_translation(self.pos)

        self.size[:] = [self.w, self.h]
        self.speed = np.zeros((2,))

        self.colliding_target = False

        self.scalar_speed = self.np_random.uniform(low=self.range_scalar_speed[0],
                                                   high=self.range_scalar_speed[1])

        self.physics_proxy.reset()
        self.physics_proxy.set_width(self.w, self.h)
        self.physics_proxy.set_pos(*self.pos.tolist())
        self.physics_proxy.set_speed(*self.speed.tolist())

        return []

    def get_reward(self, other_agents, tau=0.2):
        target_agent = self._get_target(other_agents)

        self.colliding_target = target_agent.physics_proxy.get_physics_id() in self.physics_proxy.get_colliding_ids()
        done = self.colliding_target

        dist_target = np.sqrt(np.sum((self.pos - target_agent.pos) ** 2))
        last_dist_target = np.sqrt(np.sum((self.last_pos - target_agent.last_pos) ** 2))
        dist_done = np.sqrt(np.sum((self.pos - self.last_pos) ** 2))

        # print("agent %s - dist_target %f - last_dist_target %f - dist_done %f" % (self.marker, dist_target, last_dist_target, dist_done))

        coef = -1 if self.defend else 1
        move_reward = 2 if dist_done > 1 else -2

        toward_target = 0
        if dist_target > last_dist_target + self.scalar_speed * 0.8 * tau:
            toward_target = -5
        elif dist_target < last_dist_target - self.scalar_speed * 0.8 * tau:
            toward_target = 5

        running_reward = coef * toward_target
        if running_reward < 0:
            running_reward *= 1.5

        dense_reward = running_reward

        obs = np.concatenate([self.pos, self.speed, np.array([self.w, self.h]), target_agent.pos,
                              np.array([target_agent.w, target_agent.h, dist_target])])

        return dense_reward, done, obs


class RectangleGoal(RectangleAgent):

    def __init__(self, scalar_speed, w=1, h=1, target=None, pos=np.array([0, 0]), color=np.array([0, 0, 0, 1]),
                 range_speed=np.array([0, 0, 0, 0]), range_w=np.array([10, 40]), range_h=np.array([10, 40]),
                 range_pos=[0, 0, 0, 0]):
        RectangleObject.__init__(self, w=w, h=h, pos=pos, color=color, range_speed=range_speed, range_w=range_w,
                                 range_h=range_h, range_pos=range_pos)
        Agent.__init__(self, scalar_speed, pos=pos)

        self.colliding_target = False
        self._target = target
        high = np.array([np.inf] * 9)
        low = -high
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        # Observation are pos, target_pos, size_w, size_h, target_w, target_h, dist_to_target

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, mark):
        self._target = mark

    def _get_target(self, other_objects):
        target_object = None
        for a in other_objects:
            if self.target == a.marker:
                target_object = a
                break

        if target_object is None:
            raise ValueError("target_object and timestep should not be none")

        return target_object

    def reset(self, width, height, other_objects):
        self._reset()
        target_object = self._get_target(other_objects)

        RectangleObject.reset(self, width, height)

        self.colliding_target = False

        dist_target = np.sqrt(np.sum((self.pos - target_object.pos) ** 2))

        obs = np.concatenate([self.pos, target_object.pos,
                              np.array([self.w, self.h, target_object.w, target_object.h, dist_target])])

        return obs

    def get_reward(self, other_objects, tau=0.2):

        target_object = self._get_target(other_objects)
        self.colliding_target = target_object.physics_proxy.get_physics_id() in self.physics_proxy.get_colliding_ids()
        done = self.colliding_target

        dist_target = np.sqrt(np.sum((self.pos - target_object.pos) ** 2))
        last_dist_target = np.sqrt(np.sum((self.last_pos - target_object.pos) ** 2))
        dist_done = np.sqrt(np.sum((self.pos - self.last_pos) ** 2))

        # print("agent - diff dist %f - dist_target %f - last_dist_target %f - dist_done %f" % (dist_target - last_dist_target, dist_target, last_dist_target, dist_done), end='\r')
        move_reward = 0 if dist_done > 0.2 else -1

        toward_target = 0
        if last_dist_target > dist_target + self.scalar_speed * 0.8 * tau:
            toward_target = 1
        else:
            toward_target = -1.5

        running_reward = toward_target

        dense_reward = running_reward

        obs = np.concatenate([self.pos, target_object.pos,
                              np.array([self.w, self.h, target_object.w, target_object.h, dist_target])])

        return dense_reward, done, obs


class SphereAvoid(SphereAgent):

    def __init__(self, scalar_speed, max_health, target=None, pos=np.array([0, 0]), color=(0, 0, 0, 1), radius=1,
                 range_speed=np.array([0, 0, 0, 0]), range_radius=np.array([10, 40]), range_pos=[0, 0, 0, 0]):
        SphereObject.__init__(self, radius, pos=pos, color=color, range_speed=range_speed, range_radius=range_radius,
                              range_pos=range_pos)

        Agent.__init__(self, scalar_speed, pos)

        self.colliding_target = False
        self._target = target
        self.max_health = max_health
        self.health = max_health
        self.health_step = math.floor(max_health / 35)
        high = np.array([np.inf] * 12)
        low = -high
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        # Observation are pos, target_pos, radius, target_radius,
        #  target_orientation_x, target_orientation_y, target_firing, dist_to_target,
        #  diff_to_target_x, diff_to_target_y

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, mark):
        self._target = mark

    def colliding_with(self, other_agent):

        if isinstance(other_agent, SphereObject):
            dist = np.sqrt(np.sum(np.square(self.pos - other_agent.pos)))
            if dist < self.radius + other_agent.radius:
                if other_agent.marker == self.target:
                    self.colliding_target = True
                return True
            return False
        elif isinstance(other_agent, TurretObject):

            colliding = other_agent.colliding_with(self)
            # print("colliding with turret: {}, {} ".format(colliding, other_agent.marker))
            self.colliding = colliding
            if self.colliding and other_agent.marker == self.target:
                self.colliding_target = True

            return colliding
        else:
            raise NotImplementedError

    def _get_target(self, other_objects):
        target_object = None
        for a in other_objects:
            if self.target == a.marker:
                target_object = a
                break

        if target_object is None:
            raise ValueError("target_object and timestep should not be none")

        return target_object

    def reset(self, width, height, other_objects):
        self._reset()
        target_object = self._get_target(other_objects)

        SphereObject.reset(self, width, height)

        self.colliding_target = False

        self.health = self.max_health

        diff = self.pos - target_object.pos
        dist_target = np.sqrt(np.sum((diff) ** 2))

        obs = np.concatenate([self.pos, target_object.pos,
                              np.array([self.radius, target_object.radius, math.cos(target_object.rotation),
                                        -math.sin(target_object.rotation),
                                        0, dist_target, diff[0], diff[1]])])

        return obs

    def step(self, action):
        Agent.step(self, action)
        self.colliding_target = False
        self.colliding = False

    def get_reward(self, other_objects, alpha=1.0, tau=0.2):

        target_object = self._get_target(other_objects)

        diff = self.pos - target_object.pos
        last_diff = self.last_pos - target_object.last_pos
        dist_target = np.sqrt(np.sum((diff) ** 2))
        last_dist_target = np.sqrt(np.sum((last_diff) ** 2))
        # dist_done = np.sqrt(np.sum((self.pos - self.last_pos)**2))

        # move_reward = 1 if dist_done > 5 else 0

        health_reward = 0
        if self.colliding_target:
            self.health -= self.health_step
            health_reward = -28.6

        target_orientation = target_object.orientation
        last_target_orientation = target_object.last_orientation

        last_angle_diff = np.dot(last_target_orientation, last_diff / last_dist_target)
        angle_diff = np.dot(target_orientation, diff / dist_target)

        angle_reward = 0
        if 100 * abs(angle_diff - last_angle_diff) / 2 > 0.02:
            angle_reward = 1 if angle_diff < last_angle_diff else -2
        dense_reward = health_reward + angle_reward

        done = self.health <= 0

        goal_reward = 0
        if done:
            goal_reward = -500

        reward = alpha * dense_reward + (1 - alpha) * goal_reward

        target_firing = 0 if not target_object.firing else 1
        obs = np.concatenate([self.pos, target_object.pos,
                              np.array([self.radius, target_object.radius,
                                        target_orientation[0], target_orientation[1],
                                        target_firing, dist_target, diff[0], diff[1]])])

        return reward, done, obs


class SphereGoal(SphereAgent):

    def __init__(self, scalar_speed, target=None, pos=np.array([0, 0]), color=np.array([0, 0, 0, 1]), radius=1,
                 range_speed=np.array([0, 0, 0, 0]), range_radius=np.array([10, 40]), range_pos=[0, 0, 0, 0]):
        SphereObject.__init__(self, radius, pos=pos, color=color, range_speed=range_speed, range_radius=range_radius,
                              range_pos=range_pos)
        Agent.__init__(self, scalar_speed, pos=pos)

        self.colliding_target = False
        self._target = target
        high = np.array([np.inf] * 7)
        low = -high
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        # Observation are pos, target_pos, radius, target_radius, dist_to_target

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, mark):
        self._target = mark

    def colliding_with(self, other_agent):

        if isinstance(other_agent, SphereObject):
            dist = np.sqrt(np.sum(np.square(self.pos - other_agent.pos)))
            if dist < self.radius + other_agent.radius:
                if other_agent.marker == self.target:
                    self.colliding_target = True
                    other_agent.colliding_target = True
                return True
            return False
        else:
            raise NotImplementedError

    def _get_target(self, other_objects):
        target_object = None
        for a in other_objects:
            if self.target == a.marker:
                target_object = a
                break

        if target_object is None:
            raise ValueError("target_object and timestep should not be none")

        return target_object

    def reset(self, width, height, other_objects):
        target_object = self._get_target(other_objects)

        SphereObject.reset(self, width, height)

        self.colliding_target = False
        self._reset()

        dist_target = np.sqrt(np.sum((self.pos - target_object.pos) ** 2))

        obs = np.concatenate([self.pos, target_object.pos,
                              np.array([self.radius, target_object.radius, dist_target])])

        return obs

    def get_reward(self, other_objects, tau=0.2):
        done = self.colliding_target

        target_object = self._get_target(other_objects)

        dist_target = np.sqrt(np.sum((self.pos - target_object.pos) ** 2))
        last_dist_target = np.sqrt(np.sum((self.last_pos - target_object.pos) ** 2))
        dist_done = np.sqrt(np.sum((self.pos - self.last_pos) ** 2))

        # print("agent - diff dist %f - dist_target %f - last_dist_target %f - dist_done %f" % (dist_target - last_dist_target, dist_target, last_dist_target, dist_done), end='\r')
        move_reward = 0 if dist_done > 0.2 else -1

        toward_target = 0
        if last_dist_target > dist_target + self.scalar_speed * 0.8 * tau:
            toward_target = 1
        else:
            toward_target = -1.5

        running_reward = toward_target

        dense_reward = running_reward

        obs = np.concatenate([self.pos, target_object.pos,
                              np.array([self.radius, target_object.radius, dist_target])])

        return dense_reward, done, obs


class RectangleRunner(RectangleAgent):

    def __init__(self, range_scalar_speed, target=None, pos=np.array([0, 0]), color=(0, 0, 0), w=1, h=1, defend=False,
                 range_speed=np.array([0, 0, 0, 0]), range_w=np.array([10, 40]), range_h=np.array([10, 40]),
                 range_pos=[0, 0, 0, 0]):
        RectangleObject.__init__(self, w=w, h=h, pos=pos, color=color, range_speed=range_speed, range_w=range_w,
                                 range_h=range_h, range_pos=range_pos)
        Agent.__init__(self, 0, pos=pos)

        self.colliding_target = False
        self._target = target
        self.range_scalar_speed = range_scalar_speed
        self.defend = defend
        high = np.array([np.inf] * 14)
        low = -high
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        # Observation are pos, speed, w, h, target_pos, target_w, target_h,
        # dist to target, width, height, timestep remaining

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, mark):
        self._target = mark

    def _get_target(self, other_objects):
        target_object = None
        for a in other_objects:
            if self.target == a.marker:
                target_object = a
                break

        if target_object is None:
            raise ValueError("target_object and timestep should not be none")

        return target_object

    def reset(self, width, height, other_objects):
        self._reset()
        target_agent = self._get_target(other_objects)

        RectangleObject.reset(self, width, height)

        self.colliding_target = False

        self.scalar_speed = self.np_random.uniform(low=self.range_scalar_speed[0],
                                                   high=self.range_scalar_speed[1])

        dist_target = np.sqrt(np.sum((self.pos - target_agent.pos) ** 2))

        obs = np.concatenate([self.pos, self.speed, np.array([self.w, self.h]), target_agent.pos,
                              np.array([target_agent.w, target_agent.h, dist_target])])

        return obs

    def get_reward(self, other_agents, tau=0.2):
        target_agent = self._get_target(other_agents)

        self.colliding_target = target_agent.physics_proxy.get_physics_id() in self.physics_proxy.get_colliding_ids()
        done = self.colliding_target

        dist_target = np.sqrt(np.sum((self.pos - target_agent.pos) ** 2))
        last_dist_target = np.sqrt(np.sum((self.last_pos - target_agent.last_pos) ** 2))
        dist_done = np.sqrt(np.sum((self.pos - self.last_pos) ** 2))

        # print("agent %s - dist_target %f - last_dist_target %f - dist_done %f" % (self.marker, dist_target, last_dist_target, dist_done))

        coef = -1 if self.defend else 1
        move_reward = 2 if dist_done > 1 else -2

        toward_target = 0
        if dist_target > last_dist_target + self.scalar_speed * 0.8 * tau:
            toward_target = -5
        elif dist_target < last_dist_target - self.scalar_speed * 0.8 * tau:
            toward_target = 5

        running_reward = coef * toward_target
        if running_reward < 0:
            running_reward *= 1.5

        dense_reward = running_reward

        obs = np.concatenate([self.pos, self.speed, np.array([self.w, self.h]), target_agent.pos,
                              np.array([target_agent.w, target_agent.h, dist_target])])

        return dense_reward, done, obs


class SphereRunner(SphereAgent):

    def __init__(self, range_scalar_speed, target=None, pos=np.array([0, 0]), color=(0, 0, 0), radius=1, defend=False,
                 range_speed=np.array([0, 0, 0, 0]), range_radius=np.array([10, 40]), range_pos=[0, 0, 0, 0]):
        SphereObject.__init__(self, radius, pos=pos, color=color, range_speed=range_speed, range_radius=range_radius,
                              range_pos=range_pos)
        Agent.__init__(self, 0, pos=pos)

        self.colliding_target = False
        self._target = target
        self.range_scalar_speed = range_scalar_speed
        self.defend = defend
        high = np.array([np.inf] * 12)
        low = -high
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        # Observation are pos, speed, radius, target_pos, target_radius,
        # dist to target, width, height, timestep remaining

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, mark):
        self._target = mark

    def colliding_with(self, other_agent):

        if isinstance(other_agent, SphereObject):
            dist = np.sqrt(np.sum(np.square(self.pos - other_agent.pos)))
            if dist < self.radius + other_agent.radius:
                if other_agent.marker == self.target:
                    self.colliding_target = True
                    other_agent.colliding_target = True
                return True
            return False
        else:
            raise NotImplementedError

    def _get_target(self, other_objects):
        target_object = None
        for a in other_objects:
            if self.target == a.marker:
                target_object = a
                break

        if target_object is None:
            raise ValueError("target_object and timestep should not be none")

        return target_object

    def reset(self, width, height, other_objects):

        target_agent = self._get_target(other_objects)

        SphereObject.reset(self, width, height)

        self.colliding_target = False
        self._reset()

        self.scalar_speed = self.np_random.uniform(low=self.range_scalar_speed[0],
                                                   high=self.range_scalar_speed[1])

        dist_target = np.sqrt(np.sum((self.pos - target_agent.pos) ** 2))

        obs = np.concatenate([self.pos, self.speed, np.array([self.radius]), target_agent.pos,
                              np.array([target_agent.radius, dist_target])])

        return obs

    def get_reward(self, other_agents, tau=0.2):
        done = self.colliding_target

        target_agent = self._get_target(other_agents)

        dist_target = np.sqrt(np.sum((self.pos - target_agent.pos) ** 2))
        last_dist_target = np.sqrt(np.sum((self.last_pos - target_agent.last_pos) ** 2))
        dist_done = np.sqrt(np.sum((self.pos - self.last_pos) ** 2))

        # print("agent %s - dist_target %f - last_dist_target %f - dist_done %f" % (self.marker, dist_target, last_dist_target, dist_done))

        coef = -1 if self.defend else 1
        move_reward = 2 if dist_done > 1 else -2

        toward_target = 0
        if dist_target > last_dist_target + self.scalar_speed * 0.8 * tau:
            toward_target = -5
        elif dist_target < last_dist_target - self.scalar_speed * 0.8 * tau:
            toward_target = 5

        running_reward = coef * toward_target
        if running_reward < 0:
            running_reward *= 1.5

        dense_reward = running_reward

        obs = np.concatenate([self.pos, self.speed, np.array([self.radius]), target_agent.pos,
                              np.array([target_agent.radius, dist_target])])

        return dense_reward, done, obs


class GameGrid(object):

    def __init__(self, width, height, cell_size=10):
        self.width = width
        self.height = height
        self.cell_size = cell_size

        self.cells_occupied = np.zeros((width, height), np.uint8)

    def switch_usage(self, loc):
        loc = (int(loc[0]), int(loc[1]))
        val = self.cells_occupied[loc]

        if val > 0:
            self.cells_occupied[loc] = 0
        else:
            self.cells_occupied[loc] = 1

    def pathfinding_to(self, dest=None, start=None):
        if start is None or dest is None:
            raise ValueError("start point must not be None for pathfinding")

        return [dest]
