import numpy as np
import math

import pathfinding

try:
    import rendering

    g_rendering_enabled = True
except Exception as e:
    g_rendering_enabled = False
    print("Cannot import render")

from physics_engine.PyBody import PyBody


class PhysicObject(object):
    id_counter = 1

    def __init__(self, pos=np.array([0., 0.]), mass=1):
        self.grid = None
        self.last_pos = np.zeros_like(pos)
        self.pos = pos
        self.rotation = 0
        self.size = np.zeros((2,))
        self.colliding = False
        self.speed = np.zeros((2,))
        self.scale = np.array([1, 1])
        self.np_random = None
        self.trans = None
        self.is_spawn = False
        self.spawn_type: int = pathfinding.SPAWN_DEFENDER
        self._geom = None
        self._marker = None
        self.increment_id()
        self.id = self.get_id_counter()
        self.physics_proxy = PyBody(self.id, 1, 1, mass)
        self.physics_proxy.set_pos(*self.pos.tolist())
        self.physics_proxy_init = True

    @staticmethod
    def increment_id():
        PhysicObject.id_counter += 1

    @staticmethod
    def get_id_counter():
        return PhysicObject.id_counter

    def reset(self, width, height):
        raise NotImplementedError

    def colliding_with(self, other_agent):
        raise NotImplementedError

    def check_coord_bound(self, warp=False):
        pass
        # if self.pos[0] < 0:
        #     self.pos[0] = 0 if not warp else self.width
        # elif self.pos[0] > self.width:
        #     self.pos[0] = self.width if not warp else 0
        #
        # if self.pos[1] < 0:
        #     self.pos[1] = 0 if not warp else self.height
        # elif self.pos[1] > self.height:
        #     self.pos[1] = self.height if not warp else 0

    def step(self):
        # getting last objet data from physics proxy
        data = self.physics_proxy.get_data()
        self.pos = data[2:4]
        self.speed = data[:2]
        self.rotation = data[-1]

        # self.check_coord_bound(True)
        # self.physics_proxy.set_pos(*self.pos.tolist())

        if self.geom:
            self.geom.set_translation(self.pos)
            self.geom.set_scale(self.scale)
            self.geom.set_rotation(np.array([self.rotation]))

    def set_color(self, color):
        self.geom.set_color(np.array(color))

    @property
    def geom(self):
        raise NotImplementedError

    def bbox(self):
        return self.pos[0], self.pos[1], self.size[0], self.size[1]

    def last_bbox(self):
        return self.last_pos[0], self.last_pos[1], self.size[0], self.size[1]

    @property
    def marker(self):
        return self._marker

    @marker.setter
    def marker(self, mark):
        self._marker = mark


class RectangleObject(PhysicObject):

    def __init__(self, w=1, h=1, pos=np.array([0., 0.]), color=np.array([0, 0, 0, 1]),
                 range_speed=np.array([0, 0, 0, 0]), range_w=np.array([10, 40]), range_h=np.array([10, 40]),
                 range_pos=[0, 0, 0, 0], mass=1):
        PhysicObject.__init__(self, pos, mass)

        self.color = np.array(color)

        self.range_speed = range_speed
        self.range_w = range_w
        self.range_h = range_h
        self.range_pos = range_pos
        self.physics_proxy.set_width(w, h)
        self.physics_proxy.set_type("rectangle")
        self.w = w
        self.h = h
        self.mass = mass
        self.scale = np.array([w, h]) * 0.5

        self._geom_bbox = None

    def reset(self, width, height):
        # We return a dummy env for the first step
        center = np.array([width / 2,
                           height / 2])

        self.w = self.np_random.uniform(low=self.range_w[0], high=self.range_w[1])
        self.h = self.np_random.uniform(low=self.range_h[0], high=self.range_h[1])

        if self.geom:
            self.scale = np.array([self.w, self.h]) * 0.5
            self.geom.set_scale(self.scale)

        self.speed = self.np_random.uniform(low=self.range_speed[:2], high=self.range_speed[2:], size=2)
        self.pos = self.np_random.uniform(low=self.range_pos[:2], high=self.range_pos[2:], size=(2,)) + center
        self.size[:] = [self.w, self.h]
        # self.rotation = self.np_random.uniform(low=0, high=2 * math.pi)

        self.physics_proxy.reset()
        self.physics_proxy.set_width(self.w, self.h)
        self.physics_proxy.set_pos(*self.pos.tolist())
        self.physics_proxy.set_speed(*self.speed.tolist())

    @property
    def geom(self):
        if self._geom is None and g_rendering_enabled:
            self._geom = rendering.Geom("rectangle", color=self.color, translation=self.pos,
                                        scale=np.array(self.scale))

        return self._geom


class SphereObject(PhysicObject):

    def __init__(self, radius=1, pos=np.array([0., 0.]), color=(0, 0, 0, 1), range_speed=np.array([0, 0, 0, 0]),
                 range_radius=np.array([10, 40]), range_pos=[0, 0, 0, 0]):
        PhysicObject.__init__(self, pos)

        self.radius = radius
        self.radius_2 = self.radius ** 2
        self.color = np.array(color)

        self.range_speed = range_speed
        self.range_radius = range_radius
        self.range_pos = range_pos
        self._geom_bbox = None
        self.trans_bbox = None

    def colliding_with(self, other_object):
        if isinstance(other_object, SphereObject):

            dist_2 = np.sum((self.pos - other_object.pos) ** 2)
            radius_diff_2 = (self.radius + other_object.radius) ** 2
            if dist_2 < radius_diff_2:
                return True
            return False
        elif isinstance(other_object, TurretObject):
            return other_object.colliding_with(self)
        else:
            raise NotImplementedError

    def reset(self, width, height):
        # We return a dummy env for the first step
        center = np.array([width / 2,
                           height / 2])
        high_pos = np.array([width / 2,
                             height / 2])

        self.radius = self.np_random.uniform(low=self.range_radius[0], high=self.range_radius[1])
        self.radius_2 = self.radius ** 2
        if not self.geom is None:
            scale = self.radius
            self.scale = np.array([scale, scale])
            self.geom.set_scale(self.scale)

        self.speed = self.np_random.uniform(low=self.range_speed[:2], high=self.range_speed[2:], size=2)
        self.pos = self.np_random.uniform(low=self.range_pos[:2], high=self.range_pos[2:], size=(2,)) + center
        self.size[:] = [self.radius, self.radius]
        # self.pos = np.array([0, 0]) + center

    @property
    def geom(self):
        if self._geom is None and g_rendering_enabled:
            self._geom = rendering.Geom("circle", color=self.color,
                                        scale=np.array(self.scale))

        return self._geom


class TurretObject(PhysicObject):

    def __init__(self, height, width, radius=1, pos=np.array([0., 0.]), color=(0, 0, 0),
                 range_pos=np.array([0, 0, 0, 0]),
                 range_rot_speed=np.array([-10, 10]), range_radius_ext=np.array([100, 200]),
                 range_radius=np.array([10, 40]), range_angle_span=np.array([10, 35]),
                 range_fire_len=np.array([50, 75]),
                 range_nofire_len=np.array([50, 75])):
        PhysicObject.__init__(self, pos)

        self.radius = radius
        self.radius_2 = self.radius ** 2
        self.radius_ext = radius
        self.ext_angle_span = 10 * math.pi / 180
        self.rotation = 0
        self.last_rotation = 0
        self.color = color

        self.fire_prob = 0.2
        self.firing = False

        self.target = None

        self.range_rot_speed = range_rot_speed
        self.range_radius = range_radius
        self.range_radius_ext = range_radius_ext
        self.range_angle_span = range_angle_span
        self.range_pos = range_pos
        self.range_fire_len = range_fire_len
        self.range_nofire_len = range_nofire_len

        self.rng_parameters = {"rot_speed": self.range_rot_speed,
                               "radius": self.range_radius, "radius_ext": self.range_radius_ext,
                               "angle_span": self.range_angle_span, "pos": range_pos,
                               "fire_len": self.range_fire_len,
                               "no_fire_len": self.range_nofire_len}

    def colliding_with(self, other_object):
        if isinstance(other_object, SphereObject):
            dist_center = np.sqrt(np.sum((self.pos - other_object.pos) ** 2))
            # print(dist_center, self.radius, self.radius + self.radius_ext)
            if dist_center > self.radius + self.radius_ext + other_object.radius:
                return False
            elif dist_center <= self.radius + other_object.radius:
                return True
            elif self.firing:
                diff_x = other_object.pos[0] - self.pos[0]

                angle = math.acos(diff_x / dist_center)
                angle_span = abs(math.atan2(other_object.radius, dist_center))

                if self.pos[1] < other_object.pos[1]:
                    angle = 2 * math.pi - angle

                min_angle = (angle - angle_span) % (2 * math.pi)
                max_angle = (angle + angle_span) % (2 * math.pi)

                min_rot = self.rotation
                max_rot = (self.rotation + self.ext_angle_span) % (2 * math.pi)

                # print(angle * 180 / math.pi, angle_span * 180 / math.pi)
                # print(min_angle * 180 / math.pi, max_angle* 180 / math.pi)
                # print(min_rot* 180 / math.pi, max_rot* 180 / math.pi)

                if min_rot < max_rot:
                    if (min_angle > max_angle
                            and (max_angle >= min_rot or min_angle <= max_rot)):
                        return True
                    elif max_angle >= min_rot and min_angle <= max_rot:
                        return True
                    else:
                        return False

                elif min_rot > max_rot and min_angle > max_angle:
                    return True
                elif min_rot > max_rot and (max_angle >= min_rot or min_angle <= max_rot):
                    return True

                # orientation_min = np.array([math.cos(self.rotation), -math.sin(self.rotation)])
                # orientation_max = np.array([math.cos(self.rotation + self.ext_angle_span),
                #                        -math.sin(self.rotation + self.ext_angle_span)])

                # normal_orient_min = np.array([orientation_min[1], -orientation_min[0]])
                # normal_orient_max = np.array([-orientation_max[1], orientation_max[0]])

                # diff = other_object.pos - self.pos

                # intersect_orient_min = self.pos + orientation_min * np.dot(diff, orientation_min)
                # intersect_orient_max = self.pos + orientation_max * np.dot(diff, orientation_max)

                # dist_orient_min = np.dot(other_object.pos - intersect_orient_min, normal_orient_min)
                # dist_orient_max = np.dot(other_object.pos - intersect_orient_max, normal_orient_max)
                # if (dist_orient_min >= -other_object.radius
                #        and dist_orient_max >= -other_object.radius):
                #   return True

                return False
            else:
                return False
        else:
            raise NotImplementedError

    def set_target(self, target):
        self.target = target

    def reset(self, width, height):
        center = np.array([width / 2,
                           height / 2])

        self.radius = self.np_random.uniform(low=self.range_radius[0],
                                             high=self.range_radius[1])
        self.radius_ext = self.np_random.uniform(low=self.range_radius_ext[0],
                                                 high=self.range_radius_ext[1])

        self.pos = self.np_random.uniform(low=self.range_pos[:2],
                                          high=self.range_pos[2:], size=(2,)) + center
        self.last_pos[:] = self.pos
        self.size[:] = [self.radius + self.radius_ext, self.radius + self.radius_ext]
        self.rotation = self.np_random.uniform(low=0, high=2 * math.pi)
        self.last_rotation = self.rotation
        self.rot_speed = self.np_random.uniform(low=self.range_rot_speed[0],
                                                high=self.range_rot_speed[1]) * math.pi / 180

        self.ext_angle_span = self.np_random.uniform(low=self.range_angle_span[0],
                                                     high=self.range_angle_span[1]) * math.pi / 180

        self.len_not_firing = self.np_random.random_integers(low=self.range_nofire_len[0],
                                                             high=self.range_nofire_len[1])

        self.firing = False
        self.not_firing = 1

        self.orientation = np.array([math.cos(self.rotation), -math.sin(self.rotation)])
        self.last_orientation = self.orientation.copy()

        if not self._geom is None:
            scale = (self.radius + self.radius_ext) / self._geom_radius
            self.scale = np.array([scale, scale])

            outer_beam = rendering.make_circle(self.radius_ext + self.radius,
                                               span=(self.ext_angle_span), res=80)
            self._geom.gs[1].v = outer_beam.v
            self._geom.gs[1].set_color(*self.color[:3], 0)

    def step(self, tau):
        if self.target is None:
            delta = self.rot_speed * tau
        else:
            diff = self.target.pos - self.pos
            diff /= np.sqrt(np.sum((diff) ** 2))

            delta = 0
            if np.dot(diff, self.orientation) < 0.9995:
                normal = np.array([self.orientation[1], -self.orientation[0]])

                sign = np.dot(diff, normal)
                delta = abs(self.rot_speed * tau) if sign > 0 else -abs(self.rot_speed * tau)

        self.last_rotation = self.rotation
        self.rotation += delta

        self.rotation = self.rotation % (2 * math.pi)
        # print(self.rotation * 180 / math.pi)

        self.last_orientation[:] = self.orientation
        self.orientation[:] = [math.cos(self.rotation), -math.sin(self.rotation)]

        if not self.firing and isinstance(self.firing, bool):
            if self.not_firing >= self.len_not_firing:

                self.len_firing = self.np_random.random_integers(low=self.range_fire_len[0],
                                                                 high=self.range_fire_len[1])
                # print("firing for {} steps".format(self.len_firing))
                self.firing = 1
                if not self.trans is None:
                    self.geom.gs[1].set_color(*self.color[:3], 0.6)
            else:
                self.not_firing += 1
        else:
            self.firing += 1
            if self.firing >= self.len_firing:
                self.firing = False
                self.not_firing = 1
                self.len_not_firing = self.np_random.random_integers(low=self.range_nofire_len[0],
                                                                     high=self.range_nofire_len[1])
                if not self.trans is None:
                    self.geom.gs[1].set_color(*self.color[:3], 0)

        if not self.trans is None:
            self.trans.set_translation(self.pos[0], self.height - self.pos[1])
            self.trans.set_scale(self.scale[0], self.scale[1])
            self.trans.set_rotation(self.rotation)

    @property
    def geom(self):
        if self._geom is None:
            inner_circle = rendering.make_circle(self.radius)

            outer_beam = rendering.make_circle(self.radius_ext + self.radius,
                                               span=(self.ext_angle_span), res=80)

            beam = rendering.Compound([inner_circle, outer_beam])
            self._geom = beam
            self._geom_radius = self.radius + self.radius_ext
            self.trans = rendering.Transform()
            self.trans_ext = rendering.Transform()
            self._geom.set_color(*self.color)
            self._geom.add_attr(self.trans)
            self._geom.gs[1].add_attr(self.trans_ext)
            if self.firing:
                self._geom.gs[1].set_color(*self.color[:3], 0.6)
            else:
                self._geom.gs[1].set_color(*self.color[:3], 0)
            self._geom.gs[1].attrs.append(self._geom.gs[1]._color)

        return self._geom
