# distutils: language = c++

from libcpp.vector cimport vector
from libc.math cimport floor

import numpy as np
from gym.envs.classic_control import rendering

cdef struct Element:

    int id
    int next

cdef struct FreeElt:
    Element elt
    int next

cdef class FreeList:

    cdef int _free_head
    cdef vector[FreeElt] _data

    def __cinit__(self):
        self._data.reserve(256)
        self._free_head = -1

    cdef void erase(self, int n):
        #assert n >= 0 and n < len(self._data), "Error n out of range"

        self._data[n].next = self._free_head
        self._free_head = n

    cdef int insert(self, Element* elt):
        cdef int idx
        cdef FreeElt free_elt

        if (self._free_head != -1):

            idx = self._free_head
            self._free_head = self._data[self._free_head].next
            self._data[idx].elt = elt[0]
            return idx
        else:

            free_elt.elt = elt[0]
            free_elt.next = -1
            self._data.push_back(free_elt)
            return (<int>self._data.size() - 1)

    cdef int range(self):
        return (<int>self._data.size() - 1)

    cdef void clear(self):
        self._data.clear()
        self._free_head = -1

    cdef Element* get(self, int key):
        return &self._data[key].elt

    cdef void set(self, int key, Element* elt):
        self._data[key].elt = elt[0]


def clamp(value, min_val, max_val):

    return max(min(value, max_val), min_val)

import sys

cdef int min_int(int x, int y):
    cdef int CHAR_BIT = 8
    cdef int INT_BIT = 4
    cdef int diff = x - y

    return y + (diff & (diff >> (INT_BIT * CHAR_BIT - 1)))

cdef int max_int(int x, int y):
    cdef int CHAR_BIT = 8
    cdef int INT_BIT = 4
    cdef int diff = x - y

    return x - (diff & (diff >> (INT_BIT * CHAR_BIT - 1)))

cdef class Grid:

    cdef int width, height, num_rows, num_cols
    cdef float cell_size_w, cell_size_h, inv_cell_h, inv_cell_w
    cdef int[:, :] cells
    cdef object _geoms, cells_arr
    cdef FreeList objects



    def __cinit__(self, int width, int height, int num_rows=25, int num_cols=25):
        self.width = width
        self.height = height
        self.cell_size_w = width / num_cols
        self.cell_size_h = height / num_rows

        self._geoms = None

        self.num_rows = num_rows
        self.num_cols = num_cols

        self.inv_cell_w = 1 / self.cell_size_w
        self.inv_cell_h = 1 / self.cell_size_h

        self.cells_arr = -1 * np.ones((num_rows, num_cols), np.int32)
        self.cells = self.cells_arr
        self.objects = FreeList()

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def reset(self):
        self.cells_arr = -1 * np.ones((self.num_rows ,self.num_cols), np.int32)
        self.cells = self.cells_arr
        self.objects.clear()

    @property
    def geoms(self):
        if self._geoms is None:
            self._geoms = []
            l, r, t, b = 0, self.cell_size_w, 0, -self.cell_size_h
            points = [(l,t), (r, t), (r, b), (l, b)]
            for r in range(self.num_rows):
                for c in range(self.num_cols):
                    g = rendering.make_polygon(points, filled=False)
                    g.set_color(0, 0, 0, 1)
                    g.add_attr(rendering.Transform(translation=(c * self.cell_size_w, self.height - r * self.cell_size_h)))
                    self._geoms.append(g)

        return self._geoms

    def draw(self):

        cdef int r, c, idx
        for r in range(self.num_rows):
                for c in range(self.num_cols):
                    idx = r * self.num_rows + c

                    if self.cells[r, c] != -1:
                        self.geoms[idx].set_color(1, 0, 0, 1)
                    else:
                        self.geoms[idx].set_color(0, 0, 0, 1)

    cdef (int, int, int, int) _get_grid_cells(self, (float, float, float, float) rect):

        # we assume rect is left, top, right, bottom

        #rect[0] = clamp(rect[0], 0, self.width)
        #rect[2] = clamp(rect[2], 0, self.width)
        #rect[1] = clamp(rect[1], 0, self.height)
        #rect[3] = clamp(rect[3], 0, self.height)

        cdef int grid_x1, grid_x2, grid_y1, grid_y2

        grid_x1 = min_int(<int>floor(rect[0] * self.inv_cell_w), self.num_cols - 1)
        grid_x2 = min_int(<int>floor(rect[2] * self.inv_cell_w), self.num_cols - 1)

        grid_y1 = min_int(<int>floor(rect[1] * self.inv_cell_h), self.num_rows - 1)
        grid_y2 = min_int(<int>floor(rect[3] * self.inv_cell_h), self.num_rows - 1)

        return grid_x1, grid_x2, grid_y1, grid_y2

    cdef int _check_in_cell(self, int row, int col, int id):
        cdef int link
        link = self.cells[row, col]
        if link == -1:
            return 0

        while self.objects.get(link).id != id:
            link = self.objects.get(link).next
            if link == -1:
                return 0

        return 1


    def add_object(self, (float, float, float, float) rect, int id):

        cdef (int, int, int, int) cells
        cdef int idx_obj, row, col
        cdef Element elt

        cells = self._get_grid_cells(rect)
        for row in range(cells[2], cells[3] + 1):
            for col in range(cells[0], cells[1] + 1):
                #idx_cell = (row, col)#row * self.num_cols + col

                elt.id = id
                elt.next = -1
                idx_obj = self.objects.insert(&elt)

                self.objects.get(idx_obj).next = self.cells[row, col]
                self.cells[row, col] = idx_obj


    cdef void _remove_from_cell(self, int row, int col, int id):
        cdef int link

        link = self.cells[row, col]
        #if link == -1:
        #    raise ValueError("Error id {} is not present in cell {}".format(id, idx_cell))

        if self.objects.get(link).id == id:
            self.cells[row, col] = self.objects.get(link).next
        else:
            while self.objects.get(link).id != id:
                last_link = link
                link = self.objects.get(link).next
                #if link == -1:
                #    raise ValueError("Error id {} is not present in cell {}".format(id, idx_cell))

            self.objects.get(last_link).next = self.objects.get(link).next

        self.objects.erase(link)


    def remove(self, (float, float, float, float) rect, int id):

        cdef (int, int, int, int) cells
        cdef int row, col

        cells = self._get_grid_cells(rect)
        for row in range(cells[2], cells[3] + 1):
            for col in range(cells[0], cells[1] + 1):
                self._remove_from_cell(row, col, id)


    def move(self, (float, float, float, float) prev_rect,
                    (float, float, float, float) rect, int id):

        cdef (int, int, int, int) cells, prev_cells
        cdef int min_x, min_y, max_x, max_y, range_x, range_y

        prev_cells = self._get_grid_cells(prev_rect)
        cells = self._get_grid_cells(rect)

        min_x = min(prev_cells[0], cells[0])
        min_y = min(prev_cells[2], cells[2])


        max_x = max(prev_cells[1], cells[1])
        max_y = max(prev_cells[3], cells[3])

        range_x = max_x - min_x + 1
        range_y = max_y - min_y + 1

        #print(id)
        #print(prev_rect, rect)
        #print(prev_cells, cells)
        no_intersect = np.zeros((range_y, range_x), dtype=np.uint8)


        cdef int p_y0, p_y1, p_x0, p_x1
        p_y0 = prev_cells[2] - min_y
        p_y1 = prev_cells[3] - min_y + 1
        p_x0 = prev_cells[0] - min_x
        p_x1 = prev_cells[1] - min_x + 1
        no_intersect[p_y0:p_y1, p_x0:p_x1] += 1

        cdef int x0, x1, y0, y1
        y0 = cells[2] - min_y
        y1 = cells[3] - min_y + 1
        x0 = cells[0] - min_x
        x1 = cells[1] - min_x + 1
        no_intersect[y0:y1, x0:x1] += 2

        #print(no_intersect)

        cdef int row, col, idx_obj
        cdef Element elt

        for row in range(range_y):
            for col in range(range_x):
                #idx_cell = (row + min_y, col + min_x)#(row + min_y) * self.num_cols + col + min_x
                #Removing
                if no_intersect[row, col] == 1:
                    self._remove_from_cell(row + min_y, col + min_x, id)
                elif no_intersect[row, col] == 2:

                    elt.id = id
                    elt.next = self.cells[row + min_y, col + min_x]
                    idx_obj = self.objects.insert(&elt)
                    self.cells[row + min_y, col + min_x] = idx_obj


    def query_area(self, (float, float, float, float) rect, int omit_id):

        cdef vector[int] objs
        cdef (int, int, int, int) cells
        cdef int row, col, link

        cells = self._get_grid_cells(rect)
        for row in range(cells[2], cells[3] + 1):
            for col in range(cells[0], cells[1] + 1):

                link = self.cells[row, col]

                while link != -1:
                    objs.push_back(self.objects.get(link).id)
                    link = self.objects.get(link).next

        return objs
