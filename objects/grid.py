import numpy as np
import math
from collections import namedtuple
from gym.envs.classic_control import rendering

class FreeList(object):

    class FreeElt:
        def __init__(self, elt, next=-1):
            self.elt = elt
            self.next = next


    def __init__(self):

        self._free_head = -1
        self._data = []


    def erase(self, n):
        assert n >= 0 and n < len(self._data), "Error n out of range"

        self._data[n].next = self._free_head
        self._data[n].elt = None
        self._free_head = n

    def insert(self, elt):

        if (self._free_head != -1):
            idx = self._free_head
            self._free_head = self._data[self._free_head].next
            self._data[idx].elt = elt
            return idx
        else:
            free_elt = FreeList.FreeElt(elt, next=-1)
            self._data.append(free_elt)
            return len(self._data) - 1

    def clear(self):
        self._data.clear()
        self._free_head = -1

    def range(self):
        return len(self._data) - 1

    def __getitem__(self, key):
        return self._data[key].elt

    def __setitem__(self, key, elt):
        self._data[key].elt = elt


def clamp(value, min_val, max_val):

    return max(min(value, max_val), min_val)

class Grid(object):

    class Element:

        def __init__(self, id, next=-1):
            self.id = id
            self.next = next

    def __init__(self, width, height, num_rows=25, num_cols=25):
        self.width = width
        self.height = height
        self.cell_size_w = width / num_cols
        self.cell_size_h = height / num_rows

        self._geoms = None

        self.num_rows = num_rows
        self.num_cols = num_cols

        self.inv_cell_w = 1 / self.cell_size_w
        self.inv_cell_h = 1 / self.cell_size_h

        self.cells = -1 * np.ones((num_rows, num_cols), np.int32)
        self.objects = FreeList()

    def reset(self):
        self.cells = -1 * np.ones((self.num_rows ,self.num_cols), np.int32)
        self.objects.clear()

    @property
    def geoms(self):
        if self._geoms is None:
            self._geoms = []
            l, r, t, b = 0, self.cell_size_w, 0, -self.cell_size_h
            points = [(l,t), (r, t), (r, b), (l, b)]
            for r in range(self.num_rows):
                for c in range(self.num_cols):
                    g = rendering.make_polygon(points, filled=False)
                    g.set_color(0, 0, 0, 1)
                    g.add_attr(rendering.Transform(translation=(c * self.cell_size_w, self.height - r * self.cell_size_h)))
                    self._geoms.append(g)

        return self._geoms

    def draw(self):
        for r in range(self.num_rows):
                for c in range(self.num_cols):
                    idx = r * self.num_rows + c

                    if self.cells[r, c] != -1:
                        self.geoms[idx].set_color(1, 0, 0, 1)
                    else:
                        self.geoms[idx].set_color(0, 0, 0, 1)

    def _get_grid_cells(self, rect):

        # we assume rect is left, top, right, bottom

        #rect[0] = clamp(rect[0], 0, self.width)
        #rect[2] = clamp(rect[2], 0, self.width)
        #rect[1] = clamp(rect[1], 0, self.height)
        #rect[3] = clamp(rect[3], 0, self.height)

        grid_x1 = min(rect[0] * self.inv_cell_w, self.num_cols - 1)
        grid_x2 = min(rect[2] * self.inv_cell_w, self.num_cols - 1)

        grid_y1 = min(math.floor(rect[1] * self.inv_cell_h), self.num_rows - 1)
        grid_y2 = min(math.floor(rect[3] * self.inv_cell_h), self.num_rows - 1)

        return np.array([grid_x1, grid_x2, grid_y1, grid_y2], dtype=np.int32)

    def _check_in_cell(self, idx_cell, id):
        link = self.cells[idx_cell]
        if link == -1:
            return False

        while self.objects[link].id != id:
            link = self.objects[link].next
            if link == -1:
                return False

        return True


    def add_object(self, rect, id):

        cells = self._get_grid_cells(rect)
        for row in range(cells[2], cells[3] + 1):
            for col in range(cells[0], cells[1] + 1):
                idx_cell = (row, col)#row * self.num_cols + col
                idx_obj = self.objects.insert(Grid.Element(id, next=-1))

                self.objects[idx_obj].next = self.cells[idx_cell]
                self.cells[idx_cell] = idx_obj


    def _remove_from_cell(self, idx_cell, id):
        link = self.cells[idx_cell]
        if link == -1:
            raise ValueError("Error id {} is not present in cell {}".format(id, idx_cell))

        if self.objects[link].id == id:
            self.cells[idx_cell] = self.objects[link].next
        else:
            while self.objects[link].id != id:
                last_link = link
                link = self.objects[link].next
                if link == -1:
                    raise ValueError("Error id {} is not present in cell {}".format(id, idx_cell))

            self.objects[last_link].next = self.objects[link].next

        self.objects.erase(link)


    def remove(self, rect, id):

        cells = self._get_grid_cells(rect)
        for row in range(cells[2], cells[3] + 1):
            for col in range(cells[0], cells[1] + 1):
                idx_cell = (row, col)#row * self.num_cols + col

                self._remove_from_cell(idx_cell, id)


    def move(self, prev_rect, rect, id):

        prev_cells = self._get_grid_cells(prev_rect)
        cells = self._get_grid_cells(rect)

        min_x = min(prev_cells[0], cells[0])
        min_y = min(prev_cells[2], cells[2])


        max_x = max(prev_cells[1], cells[1])
        max_y = max(prev_cells[3], cells[3])

        range_x = max_x - min_x + 1
        range_y = max_y - min_y + 1

        #print(id)
        #print(prev_rect, rect)
        #print(prev_cells, cells)

        no_intersect = np.zeros((range_y, range_x), dtype=np.uint8)

        no_intersect[prev_cells[2] - min_y:prev_cells[3] - min_y + 1,
                        prev_cells[0] - min_x:prev_cells[1] - min_x + 1] += 1

        no_intersect[cells[2] - min_y:cells[3] - min_y + 1,
                        cells[0] - min_x:cells[1] - min_x + 1] += 2

        #print(no_intersect)

        for row in range(range_y):
            for col in range(range_x):
                idx_cell = (row + min_y, col + min_x)#(row + min_y) * self.num_cols + col + min_x
                #Removing
                if no_intersect[row, col] == 1:
                    self._remove_from_cell(idx_cell, id)
                elif no_intersect[row, col] == 2:
                    idx_obj = self.objects.insert(Grid.Element(id, next=-1))
                    self.objects[idx_obj].next = self.cells[idx_cell]
                    self.cells[idx_cell] = idx_obj






    def query_area(self, rect, omit_id):

        objs = []

        cells = self._get_grid_cells(rect)
        for row in range(cells[2], cells[3] + 1):
            for col in range(cells[0], cells[1] + 1):
                idx_cell = (row, col)#row * self.num_cols + col

                link = self.cells[idx_cell]

                while link != -1:
                    objs.append(self.objects[link].id)
                    link = self.objects[link].next

        return set(objs)
