from .objects import SphereObject, PhysicObject
import numpy as np
#from .loose_grid import Grid


class FlockUnit(SphereObject):

    def __init__(self, height, width, mass, color, range_speed=np.array([0, 0, 0, 0]),
                    range_radius = np.array([10, 40])):

        SphereObject.__init__(self, color=color, range_speed=range_speed, range_radius=range_radius,
                              range_pos=[-width / 2, -height / 2, width / 2, height / 2])

        self.mass = mass

class FlockObject(PhysicObject):


    def __init__(self, num_units, grid, marker, radius_detection, max_speed, max_force, mass, color=(0, 0 ,0),
                    range_speed=np.array([0, 0, 0, 0]), range_radius = np.array([10, 40])
                    , range_force_ensemble=np.array([0, 0, 0, 0])):

        if mass <= 0:
            raise ValueError("Error mass cannot be null or negative")


        self.height = grid.height
        self.width = grid.width
        self.units = [FlockUnit(self.height, self.width, mass, color, range_speed, range_radius) for _ in range(num_units)]
        self.marker = marker
        self.colliding = False
        self.range_force_ensemble = range_force_ensemble

        self.num_units = num_units
        self.mass = mass
        self.grid = Grid(grid.width, grid.height, num_rows=5, num_cols=5)
        self.marker = marker
        self.distances = np.zeros((num_units, num_units))
        self.forces = np.zeros((num_units,2))
        self.speed = np.zeros((num_units,2))
        self.acceleration = np.zeros((num_units,2))
        self._pos = np.zeros((num_units,2))
        self._last_pos = np.zeros((num_units,2))

        self.dummy_pos = np.zeros((2,))
        self.size = np.array([1, 1])
        self.max_speed = max_speed
        self.max_force = max_force
        self.radius_detection = radius_detection
        self.cur_step = 0

    @property
    def pos(self):
        return self.dummy_pos

    @property
    def last_pos(self):
        return self.dummy_pos

    @property
    def geom(self):
        return [unit.geom for unit in self.units]

    def reset(self, width, height):
        self.cur_step = 0

        self.grid.reset()
        for idx, unit in enumerate(self.units):
            unit.np_random = self.np_random
            unit.reset(width, height)
            self._pos[idx, :] = unit.pos
            self._last_pos[idx, :] = unit.last_pos
            self.speed[idx, :] = unit.speed

            self.grid.add_object(unit.pos[0], unit.pos[1], unit.size[0], unit.size[1], idx)

        self.forces[:, :] = 0
        self.acceleration[:, :] = 0

        self.force_ens = self.np_random.uniform(low=self.range_force_ensemble[:2],
                high=self.range_force_ensemble[2:], size=2)

    def _create_steering(self, avg, total, idx):
        if total <= 0:
            raise ValueError("Error total is to be > 0")

        avg /= total

        avg_norm = np.linalg.norm(avg)
        if avg_norm > 0:
            avg = avg / avg_norm * self.max_speed
        #avg[avg > self.max_speed] /= self.max_speed
        #avg[avg < -self.max_speed] /= self.max_speed

        steering = avg - self.speed[idx, :]

        st_norm = np.linalg.norm(steering)
        if st_norm > 0:
            steering = steering / st_norm * self.max_force

        #steering[steering > self.max_force] /= self.max_force
        #steering[steering < -self.max_force] /= self.max_force

        return steering

    def _update_forces(self, idx):

        ids = self.grid.query_area(self._pos[idx, 0], self._pos[idx, 1],
                    self.radius_detection, self.radius_detection, -1)

        average_dir = np.zeros((2,))
        average_pos = np.zeros((2,))
        average_sep = np.zeros((2,))
        total = 0

        for id in ids:

            if id == idx:
                dist = 0
            elif self.distances[idx, id] > 0:
                dist = self.distances[idx, id]
            else:
                dist = np.linalg.norm(self._pos[idx, :] - self._pos[id, :])
                self.distances[idx, id] = dist
                self.distances[id, idx] = dist

            if dist < self.radius_detection:
                total += 1
                average_pos += self._pos[id, :]
                average_dir += self.speed[id, :]
                if id != idx:
                    average_sep += (self._pos[idx, :] - self._pos[id, :]) / dist

        if total > 1:
            self.forces[idx, :] =  self._create_steering(average_pos, total, idx)
            + 0.7 * self._create_steering(average_dir, total, idx)
            +  2 * self._create_steering(average_sep, total - 1, idx)

    def check_coord_bound(self, warp=False):

        self._pos[self._pos[:, 0] < 0, 0] = 0 if not warp else self.width
        self._pos[self._pos[:, 0] > self.width, 0] = self.width if not warp else 0

        self._pos[self._pos[:, 1] < 0, 1] = 0 if not warp else self.height
        self._pos[self._pos[:, 1] > self.height, 1] = self.height if not warp else 0

    def _set_objective(self):
        center = np.array([self.width / 2,
                                self.height / 2])
        high_goal= np.array([self.width / 2,
                                self.height / 2])
        self.goal = self.np_random.uniform(low=-high_goal, high=high_goal, size=(2,)) + center

    def _get_goal_forces(self):

        dir = self.goal - self._pos
        norm_dir = np.linalg.norm(dir, axis=1).reshape(self.num_units, 1)
        cond = np.where(norm_dir > 0)[0]
        dir[cond, :] = dir[cond, :] / norm_dir[cond, :] * self.max_speed

        #print(dir)
        self.force_ens = dir - self.speed
        norm_force = np.linalg.norm(self.force_ens, axis=1).reshape(self.num_units, 1)
        cond = np.where(norm_force > 0)[0]
        self.force_ens[cond, :] = self.force_ens[cond, :] / norm_force[cond, :] * self.max_force

        return self.force_ens

    def step(self, tau):



        if self.cur_step % 1 == 0:
            self.distances[:, :] = 0
            self.forces[:, :] = 0
            for idx in range(self.num_units):
                self._update_forces(idx)

        if self.cur_step % 1500 == 0:
            self._set_objective()
            #print(self.goal)
            #self.force_ens = self.force_ens / np.linalg.norm(self.force_ens) * self.max_force

        self.forces[:, :] += 3.5 * self._get_goal_forces()
        self.speed += self.forces * tau / self.mass
        self._last_pos[:, :] = self._pos
        self._pos += self.speed * tau

        norm = np.linalg.norm(self.speed, axis=1).reshape((self.num_units, 1))
        self.speed = (self.speed / norm) * self.max_speed
        self.check_coord_bound(True)

        self.cur_step += 1
        for idx, unit in enumerate(self.units):
            self.grid.move(self._last_pos[idx, 0], self._last_pos[idx, 1], self._pos[idx, 0],
                            self._pos[idx, 1], idx)
            if not unit.trans is None:
                #print(self._pos[idx, :], self._last_pos[idx, :])
                unit.trans.set_translation(self._pos[idx, 0], self.height - self._pos[idx, 1])
                unit.trans.set_scale(unit.scale[0], unit.scale[1])
                unit.trans.set_rotation(unit.rotation)
