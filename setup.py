from distutils.core import setup
from Cython.Build import cythonize
import os

setup(ext_modules=cythonize(["objects/*.pyx", "physics_engine/*.pyx"],
    include_path=[os.path.join(os.path.dirname(os.path.abspath(__file__)), "objects"),
                  os.path.join(os.path.dirname(os.path.abspath(__file__)), "physics_engine")],
    language="c++",
    annotate=True,
    compiler_directives={'language_level': 3, 'boundscheck': False, 'cdivision': True, 'initializedcheck': False}), zip_safe=False)
