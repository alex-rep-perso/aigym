import os
import numpy as np
import keras
import tensorflow as tf
from keras import backend as K
from gym import spaces
from scipy.sparse import coo_matrix


class Policy(object):

    def __init__(self, model, save_path=".", name="policy"):
        self.model = model
        self.save_path = save_path
        self.name = name
        self.cur_step = 1
        self.dummy = None
        self.dummy_action = None
        self.dummy_rec = None
        self.dummy_action_rec = None
        self.train_fun = None

    def save_iter(self, num_iter):
        raise NotImplementedError

    def load_iter(self, num_iter):
        raise NotImplementedError

    def load_most_recent(self):
        raise NotImplementedError

    @tf.function
    def do_step(self, obs_inp, mask_inp, dummy_action, states_inp, states_val_inp):
        return self.model(obs_inp, mask_inp, dummy_action, states_inp, states_val_inp)

    def step(self, obs, states=None, states_val=None, mask=None):
        if self.dummy_action is None:
            self.dummy_action = np.zeros((*obs.shape[:-1], self.model.action_dim))

        if states is None:
            states = tf.constant(0)
        if states_val is None:
            states_val = tf.constant(0)

        neglogp, _, actions, values, new_s, new_s_val = self.do_step(obs,
                                                                     mask,
                                                                     self.dummy_action,
                                                                     states,
                                                                     states_val)

        return actions.numpy(), values.numpy(), neglogp.numpy(), new_s.numpy(), new_s_val.numpy()

    def train_on(self, lr, clipfrac, obs, actions, adv, returns, old_vpred,
                 old_pred, num_epoch, batch_size, shuffle=True,
                 mask=None, states=None):

        if self.train_fun is None:
            self.train_fun = self.model.get_train_function()

        K.set_value(self.model.optimizer.learning_rate, lr)
        mblossvals = []
        stop_update = False
        max_kl_update = 0.08

        if not self.model.recurrent:

            # Index of each element of batch_size
            # Create the indices array

            len_epoch = obs.shape[0] * obs.shape[1]
            inds = np.arange(len_epoch)

            for e in range(num_epoch):
                # Randomize the indexes
                np.random.shuffle(inds)
                # 0 to batch_size with batch_train_size step
                for start in range(0, len_epoch, batch_size):
                    end = start + batch_size
                    mbinds = inds[start:end]

                    slices = []
                    for idx, arr in enumerate((obs, mask, actions, old_pred, old_vpred, adv)):
                        slices.append(arr.reshape((-1, arr.shape[-1]))[mbinds])
                    slice_returns = returns.reshape((-1, returns.shape[-1]))[mbinds]

                    loss_metrics = self.train_fun(*slices, slice_returns, [0], [0])
                    mblossvals.append([loss.numpy() for loss in loss_metrics])
                    approxkl = mblossvals[-1][2]
                    if approxkl > max_kl_update:
                        stop_update = True
                        break

                if stop_update:
                    break

        else:

            envinds = np.arange(obs.shape[0])
            nenvs = obs.shape[0]

            for e in range(num_epoch):
                np.random.shuffle(envinds)
                for start in range(0, nenvs, batch_size):
                    end = start + batch_size
                    mbenvinds = envinds[start:end]

                    slices_x = []
                    for idx, arr in enumerate((obs, mask, actions, old_pred, old_vpred, adv)):
                        slices_x.append(arr[mbenvinds])

                    slice_returns = returns[mbenvinds]

                    slice_states = states[0][mbenvinds]
                    slice_states_val = states[1][mbenvinds]

                    loss_metrics = self.train_fun(*slices_x,
                                                  slice_returns, slice_states, slice_states_val)
                    mblossvals.append([loss.numpy() for loss in loss_metrics])

                    approxkl = mblossvals[-1][2]
                    if approxkl > max_kl_update:
                        stop_update = True
                        break

                if stop_update:
                    break

        mblossvals_mean = np.mean(np.array(mblossvals), axis=0)
        mblossvals_mean[2] = mblossvals[-1][2]
        mblossvals_mean[3] = mblossvals[-1][3]
        mblossvals_mean[1] = mblossvals[-1][1]
        losses = [(loss, name) for loss, name in zip(mblossvals_mean, self.model.loss_names)]

        return losses


class SimplePolicy(Policy):

    def save_iter(self, num_iter):
        self.model.save_weights(os.path.join(self.save_path, self.name + "_%d.hdf5" % num_iter))

    def load_iter(self, num_iter):

        self.model.load_weights(os.path.join(self.save_path, self.name + "_%d.hdf5" % num_iter))

    def load_most_recent(self):
        candidates = []

        for entry in os.scandir(self.save_path):
            if entry.name.startswith(self.name) and entry.is_file() and entry.name.endswith(".hdf5"):
                candidates.append([entry.name, int(entry.name.split("_")[-1][:-5])])

        candidates.sort(key=lambda c: c[1], reverse=True)

        print("loading most recent policy : %s" % candidates[0][0])

        self.load_iter(candidates[0][1])


class HistoryPolicy(SimplePolicy):

    def __init__(self, model, save_path=".", name="policy", alpha=0.5):
        super(HistoryPolicy, self).__init__(model, save_path, name)

        assert 0 <= alpha <= 1, "Alpha coefficient must be between 0 and 1"
        self.alpha = alpha

    def load_iter(self, num_iter):
        low_iter = max(1, int(num_iter * self.alpha))

        actual_iter = np.random.randint(low_iter, num_iter + 1)
        print("history policy loaded iter {}".format(actual_iter))

        SimplePolicy.load_iter(self, actual_iter)
