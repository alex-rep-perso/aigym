from multiagent_env import MultiAgent2DEnv
from objects import *
from register_env import register


@register("GoalEnv")
class GoalEnv(MultiAgent2DEnv):

    @staticmethod
    def get_default_agents_objects(height, width):
        runner = RectangleGoal(100, color=(1, 0, 0, 1), range_w=[25, 30], range_h=[25, 30],
                               range_pos=[-width / 2, -height / 2, width / 2, height / 2])
        mark = RectangleObject(color=np.array((0, 0, 1, 1)), range_speed=[0, 0, 0, 0], range_w=[100, 100],
                               range_h=[100, 100], range_pos=[-width / 2, -height / 2, width / 2, height / 2], mass=10)

        agents_goal = [runner]
        objects_goal = [mark]

        return agents_goal, objects_goal

    def __init__(self, agents, width, height, objects, seed=None, timesteps=500,
                 start_decrease_alpha=0.1, len_decrease_alpha=0.1):

        super(GoalEnv, self).__init__(agents, width, height, seed=seed, timesteps=timesteps, objects=objects,
                                      start_decrease_alpha=start_decrease_alpha, len_decrease_alpha=len_decrease_alpha)

        assert len(agents) == 1, "Error env accept only one agent"
        assert len(objects) == 1, "Error env accept only one object"
        assert all(isinstance(agent, RectangleGoal) for agent in agents), "Agent must be of type SphereGoal"
        assert all(isinstance(obj, RectangleObject) for obj in objects), "Object must be of type SphereObject"

        self.agents[0].target = "goal"

        self.objects[0].marker = "goal"

        self.goal_reward = 1000

    def step(self, actions):
        # self._compute_tau()
        self.tau = 1 / 60
        self._step(actions)

        obs = []
        done = False
        rewards = []

        for agent in self.agents:
            rew_agent, done_agent, ob = agent.get_reward(self.objects, self.tau)
            obs.append(ob)
            rewards.append(self.alpha * rew_agent)
            done = done or done_agent
            if done:
                rewards[-1] += (1 - self.alpha) * (self.max_timesteps - self.cur_step)  # speed reward

        self.cur_step += 1

        if self.cur_step == self.max_timesteps and not done:
            done = True
            for i, a in enumerate(self.agents):
                rewards[i] -= self.goal_reward * (1 - self.alpha)
        elif done:
            for i, a in enumerate(self.agents):
                rewards[i] += self.goal_reward * (1 - self.alpha)

        return obs, rewards, done, {}


@register("TurretEnv")
class TurretEnv(MultiAgent2DEnv):

    @staticmethod
    def get_default_agents_objects(height, width):
        agents_turret = [SphereAvoid(12, 200, color=(0, 0, 1), range_radius=[10, 35],
                                     range_pos=np.array([-width / 2, -height / 2, width / 2, height / 2]))]
        objects_turret = [TurretObject(height, width, color=(0, 0, 1),
                                       range_pos=np.array([-width / 4, -height / 4, width / 4, height / 4]),
                                       range_rot_speed=np.array([-10, 10]), range_radius_ext=np.array([700, 701]),
                                       range_radius=np.array([10, 40]), range_angle_span=np.array([10, 35]),
                                       range_nofire_len=np.array([50, 150]), range_fire_len=np.array([100, 400]))]

        return agents_turret, objects_turret

    def __init__(self, agents, width, height, objects, seed=None, timesteps=500,
                 start_decrease_alpha=0, len_decrease_alpha=0.15):

        super(TurretEnv, self).__init__(agents, width, height, seed=seed, timesteps=timesteps, objects=objects)

        assert len(agents) == 1, "Error env accept only one agent"
        assert len(objects) == 1, "Error env accept only one object"
        assert all(isinstance(agent, SphereAvoid) for agent in agents), "Agent must be of type SphereAvoid"
        assert all(isinstance(obj, TurretObject) for obj in objects), "Object must be of type TurretObject"

        self.agents[0].target = "turret"

        self.objects[0].marker = "turret"

        # self.objects[0].set_target(self.agents[0])

        self.start_decrease_alpha = start_decrease_alpha
        self.len_decrease_alpha = len_decrease_alpha

    def _resolve_collisions(self, id_a, id_b):
        self.all_entities[id_a].colliding_with(self.all_entities[id_b])

    def step(self, actions):

        self._step(actions)
        self._check_collision()

        self.alpha = 1 - min(1, max(0, self.frac - self.start_decrease_alpha) / self.len_decrease_alpha)

        obs = []
        done = False
        rewards = []

        for agent in self.agents:
            rew_agent, done_agent, ob = agent.get_reward(self.objects, self.alpha, self.tau)
            obs.append(ob)
            rewards.append(rew_agent)
            done = done or done_agent
            # if done:
            #    rewards[-1] += (self.alpha) * (self.cur_step) #speed reward

        self.cur_step += 1

        if self.cur_step == self.max_timesteps and not done:
            done = True
            for i, a in enumerate(self.agents):
                rewards[i] += (1 - self.alpha) * (500 - 250 * (a.max_health - a.health) / a.max_health)

        return obs, rewards, done, {}
