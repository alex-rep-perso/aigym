import numpy as np

from multiagent_env import MultiAgent2DEnv
from objects import *
from register_env import register
from physics_engine.loose_grid import Grid
from world_generator import LinearNodeGenerator, RoomGenerator, RectRoom
from physics_engine.PyPathfinding import PyPathfinder


@register("ChasingEnv")
class ChasingEnv(MultiAgent2DEnv):

    @staticmethod
    def get_default_agents_objects(height, width):
        defender = PathRectangleRunner(50, 10, 10, target='attacker', defend=True, range_w=np.array([10, 15]),
                                       range_h=np.array([10, 15]), color=np.array([0, 0, 1, 1]),
                                       big_grid_size=21, small_grid_size=7, range_scalar_speed=np.array([80, 150]))
        attacker = PathRectangleRunner(50, 10, 10, target='defender', color=np.array([1, 0, 0, 1]),
                                       range_w=np.array([15, 25]),
                                       range_h=np.array([15, 25]),
                                       big_grid_size=21, small_grid_size=7, range_scalar_speed=np.array([50, 90]))

        agents_goal = [defender, attacker]
        objects_goal = None

        return agents_goal, objects_goal

    def __init__(self, agents, width, height, objects, seed=None, timesteps=500,
                 start_decrease_alpha=0.1, len_decrease_alpha=0.1, interval_angle=12,
                 radius_visibility=300, rendering=False):

        super().__init__(agents, width, height, seed=seed, timesteps=timesteps, objects=objects,
                         start_decrease_alpha=start_decrease_alpha, len_decrease_alpha=len_decrease_alpha,
                         rendering=rendering)

        assert len(agents) == 2, "Error env accept only one agent"
        assert all(
            isinstance(agent, PathRectangleAgent) for agent in agents), "Agent must be of type PathRectangleAgent"

        assert agents[0].defend and not agents[1].defend, "Two asymmetric defender role must be set"

        self.agents[0].marker = "defender"
        self.agents[0].target = "attacker"

        self.agents[1].marker = "attacker"
        self.agents[1].target = "defender"

        self.end_goal_reward = self.max_timesteps
        self.max_object_visible = 16

        self.nodeGenerator = None
        self.roomGenerator = None
        self.pathfinder = None
        self.interval_angle = interval_angle
        self.radius_visibility = radius_visibility

        self.init_world_generator()
        self.pathfinder = PyPathfinder(self.width, self.height, 7, 7, self.roomGenerator.grid, self.nodeGenerator)
        for a in self.agents:
            a.pathfinder = self.pathfinder

    def init_world_generator(self):

        grid = Grid(self.width, self.height, num_rows=5, num_cols=5)
        grid_rooms = Grid(self.width, self.height, num_rows=20, num_cols=20)

        p_gen = 0.9
        max_depth = 7
        max_retry = 5
        room_type_p = {RectRoom: 1}
        dim_range = {RectRoom: np.array([100, 200])}
        self.nodeGenerator = LinearNodeGenerator(p_gen, room_type_p, grid, np.random, max_depth=max_depth,
                                                 render=None, max_retry_gen_branch=max_retry, p_close_branch=0.35,
                                                 dim_range_room_type=dim_range, area_threshold=0.02)

        self.roomGenerator = RoomGenerator(self.nodeGenerator.root_node, grid=grid_rooms)

    def get_observation_agent(self, agent_idx: int):
        assert 0 <= agent_idx < len(self.agents), f"wrong agent_idx {agent_idx}"

        t_remain = np.array([self.max_timesteps - self.cur_step])
        agent = self.agents[agent_idx]
        can_output_action = np.array([agent.cur_step == 3], dtype=np.float32)
        # lidar = np.zeros((36,))
        lidar = np.array(self.physics_world.body_lidar(self.interval_angle, agent.id, False))
        observations = np.concatenate([agent.pos, agent.speed, agent.size, t_remain, can_output_action, lidar])

        closest_objects, distances = self.physics_world.query_object_radius(agent.id, self.radius_visibility)

        closest_objects = list(zip(closest_objects, distances))
        closest_objects.sort(key=lambda k: k[1])
        other_agent = None
        other_agent_visible = True
        other_objects = []
        for obj_id, dist in closest_objects:
            if obj_id in self.roomGenerator.all_objects:
                obj = self.roomGenerator.all_objects[obj_id]
                other_objects.append(np.concatenate([obj.pos, np.array([obj.w, obj.h])]))
            else:
                obj = [a for a in self.agents if a.id == obj_id][0]
                other_agent = np.concatenate([obj.pos, obj.speed, obj.size])

        other_objects = other_objects[:self.max_object_visible]
        while len(other_objects) < self.max_object_visible:
            other_objects.append(np.zeros((4,)))

        if other_agent is None:
            other_agent_visible = False
            other_agent = np.zeros((6,))

        observations = np.concatenate([observations, other_agent] + other_objects)

        return observations, other_agent_visible

    def get_reward_agent(self, agent_idx: int, ignore_dist: bool = False):
        assert 0 <= agent_idx < len(self.agents), f"wrong agent_idx {agent_idx}"

        agent = self.agents[agent_idx]
        target_agent = [a for a in self.agents if a.marker == agent.target][0]

        colliding_target = target_agent.physics_proxy.get_physics_id() in agent.physics_proxy.get_colliding_ids()

        dist_target = np.sqrt(np.sum((agent.pos - target_agent.pos) ** 2))
        last_dist_target = np.sqrt(np.sum((agent.last_pos - target_agent.last_pos) ** 2))
        # dist_done = np.sqrt(np.sum((agent.pos - agent.last_pos) ** 2))

        coef = -1 if agent.defend else 1
        if ignore_dist:
            coef = 0

        toward_target = 0
        if dist_target > last_dist_target + agent.scalar_speed * 0.75 * self.tau:
            toward_target = -1
        elif dist_target < last_dist_target - agent.scalar_speed * 0.75 * self.tau:
            toward_target = 1

        running_reward = coef * toward_target
        return running_reward, colliding_target

    def step(self, actions):
        # self._compute_tau()
        self.tau = 1 / 60
        self._step(actions)

        obs = []
        done = False
        rewards = []  # Defender, attacker

        t_remain = np.array([self.max_timesteps - self.cur_step])

        for idx, agent in enumerate(self.agents):
            obs_agent, other_visible = self.get_observation_agent(idx)
            obs.append(obs_agent)
            rew_agent, done_agent = self.get_reward_agent(idx, ignore_dist=not other_visible)
            r = self.alpha * rew_agent
            rewards.append(r)
            done = done or done_agent

            # if done_agent:
            #     coef = 1 if agent.marker == "attacker" else -1
            #     rewards[-1] += 0.5 * (self.max_timesteps - self.cur_step) * (1 - self.alpha) * coef

        self.cur_step += 1

        if self.cur_step == self.max_timesteps and not done:
            done = True
            for i, a in enumerate(self.agents):
                if a.defend:
                    rewards[i] += self.end_goal_reward * (1 - self.alpha)
                else:
                    rewards[i] -= self.end_goal_reward * (1 - self.alpha)
        elif done:
            for i, a in enumerate(self.agents):
                if a.defend:
                    rewards[i] -= self.end_goal_reward * (1 - self.alpha)
                else:
                    rewards[i] += self.end_goal_reward * (1 - self.alpha)

        return obs, rewards, done, {}

    def reset_node_generator(self):
        self.nodeGenerator.reset()
        while len(self.nodeGenerator.all_nodes) <= 1:  # We need at least two room
            self.nodeGenerator.reset()

    def reset(self):

        self.cur_step = 1

        self.seed_state = self.seed_state + 1 if isinstance(self.seed_state, int) else None
        self.seed(seed=self.seed_state)

        if self.rendering and self.roomGenerator and self.roomGenerator.init:
            for g in self.roomGenerator.root_node.room.get_all_objects(only_geom=True):
                if g.render_index == -1:
                    continue
                self.viewer.remove_geom(g)

        self.reset_node_generator()
        try:
            self.roomGenerator.reset(self.nodeGenerator.root_node)
        except KeyError:
            reset_done = False
            while not reset_done:
                try:
                    self.reset_node_generator()
                    self.roomGenerator.reset(self.nodeGenerator.root_node)
                    reset_done = True
                except KeyError:
                    pass

        self.pathfinder.checking_collision()

        if not self.agents is None:
            for agent in self.agents:
                agent.reset()

        if not self.objects is None:
            for o in self.objects:
                o.reset(self.width, self.height)

        self.physics_world.clear()
        for ent in self.all_entities:
            self.physics_world.add_proxy(ent.physics_proxy)

        for obj in self.roomGenerator.root_node.room.get_all_objects(only_geom=False):
            if obj.is_spawn:
                continue

            self.physics_world.add_proxy(obj.physics_proxy)
            if self.rendering:
                self.viewer.add_geom(obj.geom)

        obs = [self.get_observation_agent(idx)[0] for idx, _ in enumerate(self.agents)]

        self.tlast_frame = None

        return tuple(obs)
