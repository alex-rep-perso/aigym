"""
2D rendering framework
"""

import weakref
import moderngl
import moderngl_window
import moderngl_window as mglw
from moderngl_window.timers.clock import Timer
from moderngl_window.conf import settings
from moderngl_window.utils.module_loading import import_string

import math
import numpy as np

RAD2DEG = 57.29577951308232


class Geom(object):
    def __init__(self, primitive_type, color=np.array((0, 0, 0, 1)),
                 translation=np.array((0.0, 0.0)), rotation=np.array([0.0]), scale=np.array((1, 1))):
        assert primitive_type in ["circle", "rectangle", "polygon", "polyline"]

        self._color = None
        self._trans = None
        self._rot = None
        self._scale = None
        self.trans_changed = False
        self.color_changed = False
        self.rot_changed = False
        self.scale_changed = False
        self.changed = True

        self.primitive_type = primitive_type
        self._points = None
        self.points_changed = False

        # Render index to specify where it is stored into buffer array
        self.render_index = -1
        self.window_size = None

        self.set_color(color)
        self.set_translation(translation)
        self.set_rotation(rotation)
        self.set_scale(scale)

    def set_points(self, points):
        if not self.primitive_type in ["polygon", "polyline"]:
            raise ValueError("Only polygon or polyline geometry should have its own points data")
        assert isinstance(points, np.ndarray)

        self._points = points
        self.changed = True

    @property
    def color(self):
        return self._color

    @property
    def translation(self):
        return self._trans

    @property
    def rotation(self):
        return self._rot

    @property
    def scale(self):
        return self._scale

    @property
    def points(self):
        return self._points

    def set_color(self, c):
        assert isinstance(c, np.ndarray) and c.shape[0] == 4
        self._color = c
        self.changed = True

    def set_translation(self, t):
        assert isinstance(t, np.ndarray) and t.shape[0] == 2
        self._trans = t
        # self.scale_translation_to_window_coordinates()
        self.changed = True

    def set_rotation(self, r):
        assert isinstance(r, np.ndarray) and r.shape[0] == 1
        self._rot = r
        self.changed = True

    def set_scale(self, s):
        assert isinstance(s, np.ndarray) and s.shape[0] == 2
        self._scale = s
        self.changed = True

    def scale_translation_to_window_coordinates(self, prev_scale=None):
        if self.window_size:
            if prev_scale:
                self._trans = (self._trans + 1) * np.array(prev_scale) / 2
            self._trans = (2 * self._trans / np.array(self.window_size)) - 1

    def set_window_size(self, win_size):
        prev_scale = self.window_size
        self.window_size = win_size
        # self.scale_translation_to_window_coordinates(prev_scale)


class Viewer:

    def __init__(self, width, height):

        self.window_size = (width, height)
        print("initiating viewer")
        window_cls = moderngl_window.get_local_window_cls(None)

        # Calculate window size
        # size = int(size[0] * values.size_mult), int(size[1] * values.size_mult)

        # Resolve cursor
        show_cursor = True
        self.window = window_cls(
            title=ViewerConfig.title,
            size=self.window_size,
            fullscreen=False,
            resizable=False,  # ViewerConfig.resizable,
            gl_version=ViewerConfig.gl_version,
            aspect_ratio=ViewerConfig.aspect_ratio,
            vsync=ViewerConfig.vsync,
            samples=ViewerConfig.samples,
            cursor=show_cursor if show_cursor is not None else True,
        )
        self.window.print_context_info()
        moderngl_window.activate_context(window=self.window)
        self.timer = Timer()
        self.config: ViewerConfig = ViewerConfig(ctx=self.window.ctx, wnd=self.window, timer=self.timer)
        self.window.config = self.config

        self.timer_started = False

    def add_geom(self, geom: Geom):
        self.config.add_geom(geom)

    def remove_geom(self, geom: Geom):
        self.config.remove_geom(geom)

    def remove_all_geom(self):
        self.config.remove_all_geom()

    def render(self):
        if not self.timer_started:
            self.timer.start()
            self.timer_started = True

        current_time, delta = self.timer.next_frame()

        if self.config.clear_color is not None:
            self.window.clear(*self.config.clear_color)

        # Always bind the window framebuffer before calling render
        self.window.use()

        self.window.render(current_time, delta)
        if not self.window.is_closing:
            self.window.swap_buffers()

    def __del__(self):
        _, duration = self.timer.stop()
        self.window.destroy()
        if duration > 0:
            print(
                "Duration: {0:.2f}s @ {1:.2f} FPS".format(
                    duration, self.window.frames / duration
                )
            )


class ViewerConfig(mglw.WindowConfig):
    gl_version = (3, 3)
    resizable = True
    samples = 4

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.geoms = []
        self._click_listener = []
        self.buffers_circle = self.ctx.buffer(reserve=1000000, dynamic=True)
        self.buffers_rectangle = self.ctx.buffer(reserve=1000000, dynamic=True)
        self.buffers = self.ctx.buffer(reserve=1024, dynamic=True)

        self.px_to_scale_x = 2 / self.wnd.width
        self.px_to_scale_y = 2 / self.wnd.height
        vertices_rect = np.array([
            -0.5, -0.5,
            -0.5, 0.5,
            0.5, -0.5,
            0.5, 0.5,
        ]) * 2

        vertices_circle, circle_render_indices = self.make_circle_points()

        self.buffers_rectangle_points = self.ctx.buffer(vertices_rect.astype('f4'))
        self.buffers_circle_points = self.ctx.buffer(vertices_circle.astype('f4'))

        rectangle_render_indices = np.array([
            0, 1, 2,
            1, 2, 3
        ])
        self.rectangle_index_buffer = self.ctx.buffer(rectangle_render_indices.astype('i4'))
        self.circle_index_buffer = self.ctx.buffer(circle_render_indices.astype('i4'))

        self.simple_draw_prog = self.ctx.program(
            vertex_shader='''
                        #version 330

                        in vec2 in_vert;
                        in vec2 in_center;
                        in vec2 in_scale;
                        in float in_rot;
                        in vec4 in_color;
                        uniform mat4 mv_projection;
                        uniform mat4 zoom;

                        out vec4 v_color;

                        void main() {
                            
                            float cosf = cos(in_rot);
                            float sinf = sin(in_rot);

                            mat2 rot = mat2(cosf, sinf,
                                            -sinf, cosf);
                            vec2 vert = in_center + rot * (in_vert * in_scale);
                            gl_Position = zoom * mv_projection * vec4(vert, 0.0, 1.0);
                            v_color = in_color;
                        }
                    ''',
            fragment_shader='''
                        #version 330

                        in vec4 v_color;
                        out vec4 f_color;

                        void main() {
                            f_color = v_color;
                        }
                    ''',
        )

        self.mv_projection = self.simple_draw_prog["mv_projection"]
        self.zoom = self.simple_draw_prog["zoom"]
        mv_matrix = np.array([[2 / self.wnd.width, 0, 0, 0],
                              [0, 2 / self.wnd.height, 0, 0],
                              [0, 0, 1, 0],
                              [-1, -1, 0, 1]])
        zoom_ratio = 1
        zoom = np.array([[zoom_ratio, 0, 0, 0],
                         [0, zoom_ratio, 0, 0],
                         [0, 0, 1, 0],
                         [0, zoom_ratio - 1, 0, 1]])
        self.mv_projection.write(mv_matrix.astype("f4").tobytes())
        self.zoom.write(zoom.astype("f4").tobytes())

        vao_rectangle_content = [
            (self.buffers_rectangle_points, '2f', 'in_vert'),
            (self.buffers_rectangle, '4f 2f 2f 1f/i', 'in_color', 'in_center', 'in_scale', 'in_rot'),
        ]

        vao_circle_content = [
            (self.buffers_circle_points, '2f', 'in_vert'),
            (self.buffers_circle, '4f 2f 2f 1f/i', 'in_color', 'in_center', 'in_scale', 'in_rot'),
        ]

        self.vao_rectangle = self.ctx.vertex_array(self.simple_draw_prog, vao_rectangle_content,
                                                   self.rectangle_index_buffer)
        self.vao_circle = self.ctx.vertex_array(self.simple_draw_prog, vao_circle_content,
                                                self.circle_index_buffer)

        self.num_instances_circle = 0
        self.num_instances_rect = 0
        self.clear_buffer = False

        self.to_write_rectangle = np.zeros((1024 * 9,))
        self.to_write_circle = np.zeros((1024 * 9,))

        print("Config initiated")

    def update_buffer_all_geoms(self, geoms, buffer, to_write):

        len_geom = len(geoms) * 9
        rewrite_all = False
        while len_geom >= to_write.shape[0]:
            to_write = np.zeros((to_write.shape[0] * 2,))
            rewrite_all = True

        #to_update = [(idx, g) for idx, g in enumerate(geoms) if g.changed or rewrite_all]

        for idx, geom in enumerate(geoms):#to_update:
            to_write[idx * 9:(idx + 1) * 9] = np.concatenate([geom.color, geom.translation, geom.scale, geom.rotation])
            #geom.changed = False

        if to_write.shape[0] * 4 > buffer.size:
            buffer.orphan(to_write.shape[0] * 4)
        buffer.write(to_write[:len_geom].astype('f4'))

    def update_buffer_geom(self, idx, geom: Geom, buffer):

        to_write = np.concatenate([geom.color, geom.translation, geom.scale, geom.rotation])

        if len(to_write) != 9:
            print(idx)
            print(to_write)
            print(geom.color)
            print(geom.translation)
            print(geom.scale)
            print(geom.rotation)
        assert len(to_write) == 9
        # buffer entry in length
        idx_offset = idx * 9 * 4

        buffer.write(to_write.astype('f4'), offset=idx_offset)

    def resize(self, width: int, height: int):
        print("Window was resized. buffer size is {} x {} - {} x {}".format(width, height, self.wnd.width,
                                                                            self.wnd.height))
        mv_matrix = np.array([[2 / self.wnd.width, 0, 0, 0],
                              [0, 2 / self.wnd.height, 0, 0],
                              [0, 0, 1, 0],
                              [-1, -1, 0, 1]])

        self.mv_projection.write(mv_matrix.astype("f4").tobytes())

    def add_geom(self, geom: Geom):
        if geom.primitive_type == "circle":
            geom.render_index = self.num_instances_circle
            # self.update_buffer_geom(self.num_instances_circle, geom, self.buffers_circle)
            self.num_instances_circle += 1
        elif geom.primitive_type == "rectangle":
            geom.render_index = self.num_instances_rect
            # self.update_buffer_geom(self.num_instances_rect, geom, self.buffers_rectangle)
            self.num_instances_rect += 1
        if geom.window_size is None or (np.array(geom.window_size) - np.array(self.window_size) == 0).all():
            geom.set_window_size(self.window_size)

        self.geoms.append(geom)

    def remove_all_geom(self):
        self.geoms.clear()
        self.num_instances_circle = 0
        self.num_instances_rect = 0
        self.buffers_rectangle.clear()
        self.buffers_circle.clear()

    def remove_geom(self, geom: Geom):
        assert geom in self.geoms
        assert geom.render_index != -1

        self.geoms.remove(geom)
        if geom.primitive_type == "circle":
            geom.render_index = -1
            self.num_instances_circle -= 1

        elif geom.primitive_type == "rectangle":
            geom.render_index = -1
            self.num_instances_rect -= 1

    def make_circle_points(self, res=30, span=2 * math.pi):
        points = []
        index = int(round(res * span / (2 * math.pi)))
        if span < 2 * math.pi:
            index += 1

        points.append(0)
        points.append(0)
        for i in range(index):
            ang = 2 * math.pi * i / res
            points.append(math.cos(ang))
            points.append(math.sin(ang))

        render_indices = []
        num_vertices = len(points) // 2
        for idx in range(1, num_vertices):
            if idx == num_vertices - 1 and span < 2 * math.pi:
                break

            render_indices.append(0)
            render_indices.append(idx)
            if idx + 1 < num_vertices:
                render_indices.append(idx + 1)
            else:
                render_indices.append(1)

        return np.array(points), np.array(render_indices)

    def key_event(self, key, action, modifiers):
        print(key, action, modifiers)

    def add_click_listener(self, listener):
        self._click_listener.append(listener)

    def mouse_press_event(self, x: int, y: int, button: int):
        print(f"mouse click at {x} - {y} button : {button}")
        for listener in self._click_listener:
            listener(x / self.wnd.width, y / self.wnd.height, button)

    def render(self, time: float, frame_time: float):

        self.ctx.clear(1.0, 1.0, 1.0)

        geom_circle = [g for g in self.geoms if g.primitive_type == "circle"]
        geom_rectangle = [g for g in self.geoms if g.primitive_type == "rectangle"]

        self.update_buffer_all_geoms(geom_circle, self.buffers_circle, self.to_write_circle)
        self.update_buffer_all_geoms(geom_rectangle, self.buffers_rectangle, self.to_write_rectangle)

        if self.num_instances_rect > 0:
            self.vao_rectangle.render(instances=self.num_instances_rect)

        if self.num_instances_circle > 0:
            self.vao_circle.render(instances=self.num_instances_circle)

    def __del__(self):
        self.close()
