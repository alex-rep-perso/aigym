import numpy as np
import multiprocessing as mp
import threading
import traceback
import time
import sys
from enum import Enum
from copy import deepcopy

from gym import logger
from gym.vector.vector_env import VectorEnv
from gym.error import (AlreadyPendingCallError, NoAsyncCallError,
                       ClosedEnvironmentError)
from gym.vector.utils import (create_shared_memory, create_empty_array,
                              write_to_shared_memory, read_from_shared_memory,
                              concatenate, CloudpickleWrapper, clear_mpi_env_vars)

__all__ = ['AsyncVectorEnv']


class AsyncState(Enum):
    DEFAULT = 'default'
    WAITING_RESET = 'reset'
    WAITING_STEP = 'step'


def _worker(index, env_fn_wrappers, pipe, parent_pipe, error_queue):
    def step_env(env, action):
        ob, reward, done, info = env.step(action)
        if done:
            ob = env.reset()
        return ob, reward, done, info

    parent_pipe.close()
    envs = [env_fn_wrapper() for env_fn_wrapper in env_fn_wrappers.fn]
    try:
        while True:
            command, data = pipe.recv()
            if command == 'reset':
                observation = [env.reset() for env in envs]
                pipe.send((observation, True))
            elif command == 'step':
                pipe.send(([step_env(env, action) for env, action in zip(envs, data)], True))
            elif command == 'seed':
                for env, d in zip(envs, data):
                    env.seed(d)
                pipe.send((None, True))
            elif command == 'set_frac':
                for env, d in zip(envs, data):
                    env.set_frac(d)
                pipe.send((None, True))
            elif command == 'close':
                pipe.send((None, True))
                break
            elif command == '_check_observation_space':
                pipe.send((data == envs[0].observation_space, True))
            else:
                raise RuntimeError('Received unknown command `{0}`. Must '
                                   'be one of {`reset`, `step`, `seed`, `close`, '
                                   '`_check_observation_space`}.'.format(command))
    except (KeyboardInterrupt, Exception) as e:
        t, e, trace = sys.exc_info()
        var_trace = traceback.format_exc()
        print(t, e)
        print(var_trace)
        if hasattr(trace, "print_stack"):
            trace.print_stack()
        error_queue.put((index,) + sys.exc_info()[:2])
        pipe.send((None, False))
    finally:
        for env in envs:
            env.close()


class AsyncVectorEnv(VectorEnv):
    """Vectorized environment that runs multiple environments in parallel. It
    uses `multiprocessing` processes, and pipes for communication.

    Parameters
    ----------
    env_fns : iterable of callable
        Functions that create the environments.

    observation_space : `gym.spaces.Space` instance, optional
        Observation space of a single environment. If `None`, then the
        observation space of the first environment is taken.

    action_space : `gym.spaces.Space` instance, optional
        Action space of a single environment. If `None`, then the action space
        of the first environment is taken.

    shared_memory : bool (default: `True`)
        If `True`, then the observations from the worker processes are
        communicated back through shared variables. This can improve the
        efficiency if the observations are large (e.g. images).

    copy : bool (default: `True`)
        If `True`, then the `reset` and `step` methods return a copy of the
        observations.

    context : str, optional
        Context for multiprocessing. If `None`, then the default context is used.
        Only available in Python 3.
    """

    def __init__(self, env_fns, observation_space=None, action_space=None,
                 copy=True, context="spawn", in_series=4):
        try:
            ctx = mp.get_context(context)
        except AttributeError:
            logger.warn('Context switching for `multiprocessing` is not '
                        'available in Python 2. Using the default context.')
            ctx = mp
        self.nenvs = len(env_fns)
        self.copy = copy

        self.frac = 0
        self.max_timesteps = 0

        assert self.nenvs % in_series == 0, "Number of envs must be divisible by number of envs to run in series"

        if (observation_space is None) or (action_space is None) or self.max_timesteps == 0:
            dummy_env = env_fns[0]()
            self.max_timesteps = dummy_env.max_timesteps
            observation_space = observation_space or dummy_env.observation_space
            action_space = action_space or dummy_env.action_space
            dummy_env.close()
            del dummy_env
        super(AsyncVectorEnv, self).__init__(num_envs=self.nenvs,
                                             observation_space=observation_space, action_space=action_space)

        self.observations = create_empty_array(
            self.single_observation_space, n=self.num_envs, fn=np.zeros)

        self.nremotes = self.nenvs // in_series
        self.env_fns = np.array_split(env_fns, self.nremotes)

        self.parent_pipes, self.processes = [], []
        self.error_queue = ctx.Queue()
        target = _worker
        with clear_mpi_env_vars():
            for idx, env_fn in enumerate(self.env_fns):
                parent_pipe, child_pipe = ctx.Pipe()
                process = ctx.Process(target=target,
                                      name='Worker<{0}>-{1}'.format(type(self).__name__, idx),
                                      args=(idx, CloudpickleWrapper(env_fn), child_pipe,
                                            parent_pipe, self.error_queue))

                self.parent_pipes.append(parent_pipe)
                self.processes.append(process)

                process.daemon = True
                process.start()
                child_pipe.close()

        self._state = AsyncState.DEFAULT
        self._check_observation_spaces()

    def seed(self, seeds=None):
        """
        Parameters
        ----------
        seeds : list of int, or int, optional
            Random seed for each individual environment. If `seeds` is a list of
            length `num_envs`, then the items of the list are chosen as random
            seeds. If `seeds` is an int, then each environment uses the random
            seed `seeds + n`, where `n` is the index of the environment (between
            `0` and `num_envs - 1`).
        """
        self._assert_is_running()
        if seeds is None:
            seeds = [None for _ in range(self.num_envs)]
        if isinstance(seeds, int):
            seeds = [seeds + i for i in range(self.num_envs)]
        assert len(seeds) == self.num_envs

        if self._state != AsyncState.DEFAULT:
            raise AlreadyPendingCallError('Calling `seed` while waiting '
                                          'for a pending call to `{0}` to complete.'.format(
                self._state.value), self._state.value)

        seeds = np.array_split(seeds, self.nremotes)
        for pipe, seed in zip(self.parent_pipes, seeds):
            pipe.send(('seed', seed))
        _, successes = zip(*[pipe.recv() for pipe in self.parent_pipes])
        self._raise_if_errors(successes)

    def set_frac(self, frac):
        """
        Parameters
        ----------
        frac
        """
        self._assert_is_running()
        assert isinstance(frac, float) and frac >= 0 and frac <= 1

        frac = [frac for _ in range(self.num_envs)]
        frac = np.array_split(frac, self.nremotes)

        if self._state != AsyncState.DEFAULT:
            raise AlreadyPendingCallError('Calling `set_frac` while waiting '
                                          'for a pending call to `{0}` to complete.'.format(
                self._state.value), self._state.value)

        for pipe, f in zip(self.parent_pipes, frac):
            pipe.send(('set_frac', f))
        _, successes = zip(*[pipe.recv() for pipe in self.parent_pipes])
        self._raise_if_errors(successes)

    def reset_async(self):
        self._assert_is_running()
        if self._state != AsyncState.DEFAULT:
            raise AlreadyPendingCallError('Calling `reset_async` while waiting '
                                          'for a pending call to `{0}` to complete'.format(
                self._state.value), self._state.value)

        for pipe in self.parent_pipes:
            pipe.send(('reset', None))
        self._state = AsyncState.WAITING_RESET

    def reset_wait(self, timeout=None):
        """
        Parameters
        ----------
        timeout : int or float, optional
            Number of seconds before the call to `reset_wait` times out. If
            `None`, the call to `reset_wait` never times out.

        Returns
        -------
        observations : sample from `observation_space`
            A batch of observations from the vectorized environment.
        """
        self._assert_is_running()
        if self._state != AsyncState.WAITING_RESET:
            raise NoAsyncCallError('Calling `reset_wait` without any prior '
                                   'call to `reset_async`.', AsyncState.WAITING_RESET.value)

        if not self._poll(timeout):
            self._state = AsyncState.DEFAULT
            raise mp.TimeoutError('The call to `reset_wait` has timed out after '
                                  '{0} second{1}.'.format(timeout, 's' if timeout > 1 else ''))

        results, successes = zip(*[pipe.recv() for pipe in self.parent_pipes])
        self._raise_if_errors(successes)
        self._state = AsyncState.DEFAULT

        results = _flatten_list(results)
        concatenate(results, self.observations, self.single_observation_space)

        return deepcopy(self.observations) if self.copy else self.observations

    def step_async(self, actions):
        """
        Parameters
        ----------
        actions : iterable of samples from `action_space`
            List of actions.
        """
        self._assert_is_running()
        if self._state != AsyncState.DEFAULT:
            raise AlreadyPendingCallError('Calling `step_async` while waiting '
                                          'for a pending call to `{0}` to complete.'.format(
                self._state.value), self._state.value)

        actions = np.array_split(actions, self.nremotes)
        for pipe, action in zip(self.parent_pipes, actions):
            pipe.send(('step', action))
        self._state = AsyncState.WAITING_STEP

    def step_wait(self, timeout=None):
        """
        Parameters
        ----------
        timeout : int or float, optional
            Number of seconds before the call to `step_wait` times out. If
            `None`, the call to `step_wait` never times out.

        Returns
        -------
        observations : sample from `observation_space`
            A batch of observations from the vectorized environment.

        rewards : `np.ndarray` instance (dtype `np.float_`)
            A vector of rewards from the vectorized environment.

        dones : `np.ndarray` instance (dtype `np.bool_`)
            A vector whose entries indicate whether the episode has ended.

        infos : list of dict
            A list of auxiliary diagnostic informations.
        """
        self._assert_is_running()
        if self._state != AsyncState.WAITING_STEP:
            raise NoAsyncCallError('Calling `step_wait` without any prior call '
                                   'to `step_async`.', AsyncState.WAITING_STEP.value)

        if not self._poll(timeout):
            self._state = AsyncState.DEFAULT
            raise mp.TimeoutError('The call to `step_wait` has timed out after '
                                  '{0} second{1}.'.format(timeout, 's' if timeout > 1 else ''))

        results, successes = zip(*[pipe.recv() for pipe in self.parent_pipes])
        self._raise_if_errors(successes)
        self._state = AsyncState.DEFAULT

        results = _flatten_list(results)
        observations_list, rewards, dones, infos = zip(*results)

        concatenate(observations_list, self.observations,
                    self.single_observation_space)

        return (deepcopy(self.observations) if self.copy else self.observations,
                np.array(rewards), np.array(dones, dtype=np.bool), infos)

    def close(self, timeout=None, terminate=False):
        """
        Parameters
        ----------
        timeout : int or float, optional
            Number of seconds before the call to `close` times out. If `None`,
            the call to `close` never times out. If the call to `close` times
            out, then all processes are terminated.

        terminate : bool (default: `False`)
            If `True`, then the `close` operation is forced and all processes
            are terminated.
        """
        if self.closed:
            return

        if self.viewer is not None:
            self.viewer.close()

        timeout = 0 if terminate else timeout
        try:
            if self._state != AsyncState.DEFAULT:
                logger.warn('Calling `close` while waiting for a pending '
                            'call to `{0}` to complete.'.format(self._state.value))
                function = getattr(self, '{0}_wait'.format(self._state.value))
                function(timeout)
        except mp.TimeoutError:
            terminate = True

        if terminate:
            for process in self.processes:
                if process.is_alive():
                    process.terminate()
        else:
            for pipe in self.parent_pipes:
                if (pipe is not None) and (not pipe.closed):
                    pipe.send(('close', None))
            for pipe in self.parent_pipes:
                if (pipe is not None) and (not pipe.closed):
                    pipe.recv()

        for pipe in self.parent_pipes:
            if pipe is not None:
                pipe.close()
        for process in self.processes:
            process.join()

        self.closed = True

    def _poll(self, timeout=None):
        self._assert_is_running()
        if timeout is None:
            return True
        end_time = time.time() + timeout
        delta = None
        for pipe in self.parent_pipes:
            delta = max(end_time - time.time(), 0)
            if pipe is None:
                return False
            if pipe.closed or (not pipe.poll(delta)):
                return False
        return True

    def _check_observation_spaces(self):
        self._assert_is_running()
        for pipe in self.parent_pipes:
            pipe.send(('_check_observation_space', self.single_observation_space))
        same_spaces, successes = zip(*[pipe.recv() for pipe in self.parent_pipes])
        self._raise_if_errors(successes)
        if not all(same_spaces):
            raise RuntimeError('Some environments have an observation space '
                               'different from `{0}`. In order to batch observations, the '
                               'observation spaces from all environments must be '
                               'equal.'.format(self.single_observation_space))

    def _assert_is_running(self):
        if self.closed:
            raise ClosedEnvironmentError('Trying to operate on `{0}`, after a '
                                         'call to `close()`.'.format(type(self).__name__))

    def _raise_if_errors(self, successes):
        if all(successes):
            return

        num_errors = self.num_envs - sum(successes)
        assert num_errors > 0, "num errors is zeros"
        for _ in range(num_errors):
            index, exctype, value = self.error_queue.get()
            logger.error('Received the following error from Worker-{0}: '
                         '{1}: {2} {3}'.format(index, exctype.__name__, value, t))
            logger.error('Shutting down Worker-{0}.'.format(index))
            self.parent_pipes[index].close()
            self.parent_pipes[index] = None

        logger.error('Raising the last exception back to the main process.')
        raise exctype(value)

    def __del__(self):
        if hasattr(self, 'closed'):
            if not self.closed:
                self.close(terminate=True)


def _flatten_obs(obs):
    assert isinstance(obs, (list, tuple)), "obs has a wrong format"
    assert len(obs) > 0, "obs empty"

    if isinstance(obs[0], dict):
        keys = obs[0].keys()
        return {k: np.stack([o[k] for o in obs]) for k in keys}
    else:
        return np.stack(obs)


def _flatten_list(l):
    assert isinstance(l, (list, tuple))
    assert len(l) > 0
    assert all([len(l_) > 0 for l_ in l])

    return [l__ for l_ in l for l__ in l_]
