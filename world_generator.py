from abc import ABC
from typing import Mapping, Tuple, List, Union
from physics_engine.loose_grid import Grid

import pathfinding
from objects import RectangleObject, PhysicObject
from Polygon.Shapes import Rectangle
import rendering
import numpy as np


def wait_input():
    inp = input("Waiting for input...")


class Node:

    def __init__(self, width: float, height: float, room_type: type,
                 position: np.ndarray, id_node: int, depth: int = 1,
                 color: np.ndarray = np.array([0, 0, 0, 1]), is_spawn: bool = False, spawn_type: int = pathfinding.SPAWN_DEFENDER):
        self.id_node = id_node
        self.depth = depth
        self.width = width
        self.height = height
        self.color = color
        self.room_type = room_type
        self.position = position
        self.room = None
        self.is_spawn: bool = is_spawn
        self.spawn_type: int = spawn_type
        self.neighbours = []
        self._neighbours_connections_geom = []

        self._geom = None
        self.objects = []

    def get_all_geoms(self, id_visited):
        geoms = self.geom
        id_visited.append(self.id_node)
        for c in self.neighbours:
            node = c["node"]
            if node.id_node in id_visited:
                continue
            geoms += node.geom + node.get_all_geoms(id_visited)

        return geoms

    @property
    def size(self):
        return np.array([self.width, self.height])

    @property
    def geom(self):

        if self._geom is None:
            self._geom = rendering.Geom("rectangle", color=self.color,
                                        translation=self.position,
                                        scale=self.size * 0.5)

        res = [self._geom]

        if len(self.neighbours) > 0:
            len_neighbours_geoms = len(self._neighbours_connections_geom)

            for idx in range(len_neighbours_geoms, len(self.neighbours)):
                dir_con = self.neighbours[idx]["dir"] * np.array(
                    [self.width, self.height]) / 4
                position = self.position + dir_con
                dir_con = np.abs(dir_con)
                dir_con[dir_con == 0] = 3
                self._neighbours_connections_geom.append(rendering.Geom("rectangle", color=np.array([1, 1, 1, 1]),
                                                                        translation=position,
                                                                        scale=dir_con))

            res += self._neighbours_connections_geom
        return res


class NodeGenerator:
    max_id = 2 ** 20
    max_id_bin = 20

    def __init__(self, p_gen_room: float, p_gen_room_type: Mapping[type, float], grid: object, np_random: object,
                 dim_range_room_type: Mapping[type, np.ndarray] = None,
                 max_depth: int = 10, render: rendering.Viewer = None):

        assert 0 < p_gen_room <= 1, "Probability of room generation should be > 0 and <= 1"
        assert 0 < max_depth, "Max depth should be a positive int"

        self.render = render
        self.max_depth = max_depth
        self.grid = grid

        self.p_gen_room_type = p_gen_room_type
        self.p_gen_room = p_gen_room
        self.dim_range_room_type = dim_range_room_type
        self.np_random = np_random
        self.id_counter = 1
        self.root_node: Node = None
        self.all_nodes: Mapping[int, Node] = {}

        if not self.dim_range_room_type is None and set(self.p_gen_room_type.keys()) != set(
                self.dim_range_room_type.keys()):
            raise ValueError("p_gen_room_type and dim_range_room_type do not have the same keys")

    def generate_room_dims(self, room_type: type, children_con: np.ndarray):
        if self.dim_range_room_type is None or not room_type in self.dim_range_room_type:
            width, height = room_type.generate_room_dim(self.np_random, children_con)
        else:
            width, height = room_type.generate_room_dim(self.np_random, children_con,
                                                        self.dim_range_room_type[room_type])

        return width, height

    def init(self):
        room_type = self._generate_room_type()

        width, height = self.generate_room_dims(room_type, None)
        center = self.np_random.uniform(low=[width / 2, height / 2],
                                        high=[self.grid.get_width() - width / 2, self.grid.get_height() - height / 2])

        self.root_node = self._create_node(width, height, room_type, center, 1, color=np.array([0, 0, 0, 1]))

    def reset(self):
        raise NotImplementedError

    def _create_node(self, w: float, h: float, room_type: type, center: np.ndarray, depth: int,
                     color: np.ndarray = None, add_to_grid: bool = True, add_to_nodes: bool = True,
                     add_to_render: bool = True):
        if color is None:
            color = self.np_random.uniform(size=4)
            color[-1] = 0.3
        node = Node(w, h, room_type, center, self.id_counter, depth=depth,
                    color=color)
        self.id_counter += 1
        assert self.id_counter < self.max_id
        if add_to_nodes:
            self.all_nodes[node.id_node] = node

        if not self.render is None and add_to_render:
            node_geoms = node.geom
            for g in node_geoms:
                self.render.add_geom(g)

        if add_to_grid:
            self.grid.add_object(node.position[0], node.position[1],
                                 node.width / 2, node.height / 2, node.id_node)

        return node

    def _generate_room_type(self):
        return self.np_random.choice(list(self.p_gen_room_type.keys()), p=list(self.p_gen_room_type.values()))

    def fill_render(self, render: rendering.Viewer = None, reset: bool = True):
        r = None
        if render is None and self.render is None:
            raise ValueError("no render available")

        elif render is None:
            r = self.render
        else:
            r = render

        if reset:
            r.remove_all_geom()
        id_visited = []
        for g in self.root_node.get_all_geoms(id_visited):
            r.add_geom(g)

    def get_node_by_id(self, node_id: int) -> Node:
        if node_id <= 0 or not node_id in self.all_nodes:
            return False

        return self.all_nodes[node_id]

    def check_node_collision(self, width: float, height: float, center: np.ndarray) -> Tuple[bool, List[int], bool]:
        ids = self.grid.query_area(center[0], center[1], width / 2, height / 2, -1)

        inside_grid = self.grid.inside_grid_bounds(center[0], center[1], width / 2, height / 2)

        return inside_grid and len(ids) == 0, ids, inside_grid

    def try_potential_node(self, starting_node: Node, potential_dirs: List[Tuple[np.ndarray, int]] = None,
                           max_try: int = 1) -> Tuple[Union[bool, Node], np.ndarray, int]:
        assert max_try > 0, "Max try should be a strictly positive int"

        dir_con = None
        idx_con = None
        if potential_dirs and len(potential_dirs) == 0:
            return False, dir_con, idx_con

        for _ in range(0, max_try):
            room_type = self._generate_room_type()

            if potential_dirs is None:
                potential_dirs = starting_node.room_type.get_potential_connections()

            idx = self.np_random.choice(range(len(potential_dirs)))
            dir_con, idx_con = potential_dirs[idx]

            width, height = self.generate_room_dims(room_type, -dir_con)
            center = starting_node.position + dir_con * np.array([starting_node.width + width,
                                                                  starting_node.height + height]) / 2 * 1.001

            node_possible, _, _ = self.check_node_collision(width, height, center)

            if node_possible:
                return self._create_node(width, height, room_type, center, starting_node.depth + 1,
                                         add_to_render=False), dir_con, idx_con

        return False, dir_con, idx_con


class BasicNodeGenerator(NodeGenerator):

    def __init__(self, p_gen_room: float, p_gen_room_type: Mapping[type, float], grid: object, np_random: object,
                 dim_range_room_type: Mapping[type, np.ndarray] = None, max_retry_gen_room: int = 10,
                 max_depth: int = 10, render: rendering.Viewer = None):

        super().__init__(p_gen_room, p_gen_room_type, grid, np_random, dim_range_room_type=dim_range_room_type,
                         render=render, max_depth=max_depth)

        assert 0 < max_retry_gen_room, "Max retry for room generation should be a positive int"

        self.max_retry_gen_room = max_retry_gen_room

        self.reset()

    def reset(self):
        if not self.render is None:
            self.render.remove_all_geom()

        del self.root_node
        self.grid.reset()
        self.init()
        self.generate_children(self.root_node)

    def generate_children(self, node: Node):
        if node.depth >= self.max_depth:
            return

        for potential_conn, idx_con in node.room_type.get_potential_connections():
            skip_con = False
            for neighbour in node.neighbours:
                if all(potential_conn == neighbour["dir"]):
                    skip_con = True
                    break

            if skip_con:
                continue

            if self.p_gen_room >= self.np_random.uniform():
                child_node, _, _ = self.try_potential_node(node, potential_dirs=[(potential_conn, idx_con)],
                                                           max_try=self.max_retry_gen_room)
                if child_node:
                    node.neighbours.append({"node": child_node, "idx": idx_con, "dir": potential_conn})
                    child_node.neighbours.append({"node": node, "dir": -potential_conn})
                    self.generate_children(child_node)


def distance_to_branch(node: Node, branch: List[Node]):
    min_dist = None
    min_node = None

    for n in branch:

        dist = np.sqrt(np.sum((n.position - node.position) ** 2))
        if min_dist is None or dist < min_dist:
            min_dist = dist
            min_node = n

    return min_dist, min_node


class LinearNodeGenerator(NodeGenerator):

    def __init__(self, p_gen_room: float, p_gen_room_type: Mapping[type, float], grid: object, np_random: object,
                 dim_range_room_type: Mapping[type, np.ndarray] = None, max_retry_gen_branch: int = 30,
                 max_retry_gen_room: int = 10, min_len: int = 3, max_depth: int = 10, render: rendering.Viewer = None,
                 p_gen_branch: float = 0.6, p_close_branch: float = 0.2, area_threshold: float = 0.05):
        super().__init__(p_gen_room, p_gen_room_type, grid, np_random, dim_range_room_type=dim_range_room_type,
                         render=render, max_depth=max_depth)

        assert 0 < max_retry_gen_branch, "Max retry for branch generation should be a positive int"
        assert min_len < max_depth - 2, "Minimum length for branch is too high compared to max depth"
        assert 0 < p_gen_branch < 1
        assert 0 < p_close_branch < 1
        assert 0 < area_threshold < 1

        self.max_retry_gen_room = max_retry_gen_room
        self.max_retry_gen_branch = max_retry_gen_branch
        self.p_gen_branch = p_gen_branch
        self.p_close_branch = p_close_branch
        self.area_threshold = area_threshold
        self.min_len = min_len
        self.main_branch = None
        self.aux_branches = []

        #self.reset()

    def reset(self, max_num_branches: int = 5):
        if not self.render is None:
            self.render.remove_all_geom()

        del self.root_node
        self.all_nodes = {}
        self.grid.reset()
        self.init()
        self.aux_branches = []
        self.generate_main_branch(self.root_node)

        for idx in range(max_num_branches):
            branch_list = [self.main_branch] + self.aux_branches
            start_branch_idx = self.np_random.choice(list(range(len(branch_list))))

            start_branch = branch_list[start_branch_idx]
            branch = self.generate_branch(start_branch)
            try_idx = 1

            while not branch and try_idx <= self.max_retry_gen_branch:
                branch = self.generate_branch(start_branch)
                try_idx += 1

            if branch:
                self.aux_branches.append(branch)

        self.root_node.is_spawn = True
        if len(self.aux_branches) > 0:
            branch_idx = self.np_random.choice(list(range(len(self.aux_branches))))

            attacker_spawn_node = self.aux_branches[branch_idx][-1]
        else:
            attacker_spawn_node = self.main_branch[-1]

        attacker_spawn_node.is_spawn = True
        attacker_spawn_node.spawn_type = pathfinding.SPAWN_ATTACKER

    def clean_failed_branch(self, branch: List[Node]):
        for node in reversed(branch):
            node.neighbours = None
            del self.all_nodes[node.id_node]
            self.grid.remove(node.position[0], node.position[1], node.id_node)

    def check_area_with_nodes(self, size: np.ndarray, center: np.ndarray,
                              ids_nodes: List[int], area_threshold: float) -> bool:

        r1 = Rectangle(size[0], size[1])
        r1.shift(center[0], center[1])

        inter_good = len(ids_nodes) > 0
        for i in ids_nodes:
            n = self.all_nodes[i]

            r2 = Rectangle(n.width, n.height)
            r2.shift(n.position[0], n.position[1])

            IoU = (r1 & r2).area() / (r1 | r2).area()
            inter_good = inter_good and IoU < area_threshold

        return inter_good

    def try_close_branch(self, cur_node: Node, closest_node: Node, potential_dirs: List[Tuple[np.ndarray, int]],
                         width_range: np.ndarray, height_range: np.ndarray, cur_branch: List[Node]) -> bool:

        dir_to_closest_node = closest_node.position - cur_node.position
        for dir_con, idx_con in potential_dirs:
            for dir_con_closest, idx_con_closest in closest_node.room_type.get_potential_connections():

                if dir_con_closest.dot(dir_to_closest_node) > 0:
                    continue

                room_type_child = self._generate_room_type()

                potential_dirs_child = room_type_child.get_potential_connections()
                potential_dirs_child = list(filter(lambda d: dir_to_closest_node.dot(d[0]) > 0, potential_dirs_child))

                H = dir_con * cur_node.size / 2 - dir_con_closest * closest_node.size / 2 - dir_to_closest_node

                # we cannot find a room with a compatible connection to closest node
                if len(list(filter(lambda d: all(-dir_con_closest == d[0]),
                                   room_type_child.get_potential_connections()))) == 0:
                    continue

                # We try a direction for child and see if we can satisfy constraint
                for dir_con_child, idx_con_child in potential_dirs_child:
                    diff_dir = dir_con_closest - dir_con_child
                    if any((dir_con + dir_con_child) == 0) or any(diff_dir == 0):
                        continue

                    lim_factor = diff_dir / (dir_con + dir_con_child)
                    lim_add = -H / (dir_con + dir_con_child)

                    lim_width = width_range / 2 * lim_factor[0] + lim_add[0]
                    min_lim_width = max(width_range[0] / 2, lim_width[0] if lim_factor[0] > 0 else lim_width[1])
                    max_lim_width = min(width_range[1] / 2, lim_width[1] if lim_factor[0] > 0 else lim_width[0])

                    lim_height = height_range / 2 * lim_factor[1] + lim_add[1]
                    min_lim_height = max(height_range[0] / 2, lim_height[0] if lim_factor[1] > 0 else lim_height[1])
                    max_lim_height = min(height_range[1] / 2, lim_height[1] if lim_factor[1] > 0 else lim_height[0])

                    if min_lim_width > max_lim_width or min_lim_height > max_lim_height:
                        continue

                    width_child = self.np_random.uniform(low=min_lim_width, high=max_lim_width)
                    height_child = self.np_random.uniform(low=min_lim_height, high=max_lim_height)
                    size_child = np.array([width_child, height_child])
                    center_child = cur_node.position + dir_con * (cur_node.size / 2 + size_child)

                    size_child_connect = (H + size_child * (dir_con + dir_con_child)) / diff_dir
                    center_child_connect = center_child + dir_con_child * (size_child + size_child_connect)

                    is_child_possible, ids_child, child_in_grid = self.check_node_collision(width_child * 2,
                                                                                            height_child * 2,
                                                                                            center_child)
                    is_child_connect_possible, ids_child_connect, child_con_in_grid = self.check_node_collision(
                        size_child_connect[0] * 2, size_child_connect[1] * 2, center_child_connect)

                    if child_in_grid and len(ids_child) > 0:
                        is_child_possible = self.check_area_with_nodes(size_child * 2, center_child,
                                                                       ids_child, self.area_threshold)
                    if child_con_in_grid and len(ids_child_connect) > 0:
                        is_child_connect_possible = self.check_area_with_nodes(size_child_connect * 2,
                                                                               center_child_connect,
                                                                               ids_child_connect, self.area_threshold)

                    if not is_child_possible or not is_child_connect_possible:
                        continue

                    child_node = self._create_node(size_child[0] * 2, size_child[1] * 2, room_type_child, center_child,
                                                   cur_node.depth + 1, add_to_render=False)
                    child_connect_node = self._create_node(size_child_connect[0] * 2, size_child_connect[1] * 2,
                                                           room_type_child,
                                                           center_child_connect, cur_node.depth + 2,
                                                           add_to_render=False)

                    # Doing connections
                    cur_node.neighbours.append({"node": child_node, "idx": idx_con, "dir": dir_con})
                    child_node.neighbours.append({"node": cur_node, "dir": -dir_con})

                    child_node.neighbours.append(
                        {"node": child_connect_node, "idx": idx_con_child, "dir": dir_con_child})
                    child_connect_node.neighbours.append({"node": child_node, "dir": -dir_con_child})

                    idx_con_child_connect = \
                        [idx for d, idx in room_type_child.get_potential_connections() if all(d == -dir_con_closest)][0]
                    child_connect_node.neighbours.append(
                        {"node": closest_node, "idx": idx_con_child_connect, "dir": -dir_con_closest})
                    closest_node.neighbours.append({"node": child_connect_node, "dir": dir_con_closest})

                    cur_branch.append(child_node)
                    cur_branch.append(child_connect_node)

                    return True

        return False

    def generate_branch(self, starting_branch: List[Node]) -> Union[bool, List[Node]]:
        cur_len = 1
        starting_node: Node = None
        cur_branch: List[Node] = []

        branch_failed = False

        for idx_branch, node in enumerate(starting_branch):
            if idx_branch == 0:
                continue

            if self.p_gen_branch >= self.np_random.uniform():
                dir_used = [n["dir"] for n in node.neighbours]

                # Removing already used direction for node
                potential_dirs = node.room_type.get_potential_connections()
                potential_dirs = list(filter(lambda dir: all([any(d != dir[0]) for d in dir_used]), potential_dirs))

                if len(potential_dirs) == 0:
                    continue

                cur_node, dir_con, idx_con = self.try_potential_node(node, potential_dirs=potential_dirs,
                                                                     max_try=self.max_retry_gen_room)
                if cur_node:
                    cur_branch.append(cur_node)
                    starting_node = (node, dir_con, idx_con)
                    while cur_len < self.min_len or cur_len <= self.max_depth:

                        dist, closest_node = distance_to_branch(cur_node, starting_branch)

                        width_range, height_range = closest_node.room_type.get_default_range_dim()
                        if self.dim_range_room_type:
                            width_range = self.dim_range_room_type[closest_node.room_type]
                            height_range = closest_node.room_type.get_second_dim_range(width_range)

                        min_size = np.array([width_range[0], height_range[0]])
                        dir_to_closest_node = closest_node.position - cur_node.position
                        dir_to_closest_node_abs = np.abs(dir_to_closest_node)
                        max_stride_to_connect = dir_to_closest_node_abs.dot(
                            dir_to_closest_node_abs) / dir_to_closest_node_abs.dot(min_size / 2) - 2

                        should_get_closer = cur_len >= self.min_len and (
                                self.max_depth - cur_len - 2 < max_stride_to_connect)
                        should_close_branch = cur_len >= self.min_len and (
                                self.max_depth - 2 <= cur_len or self.p_close_branch > self.np_random.uniform())

                        potential_dirs_child = cur_node.room_type.get_potential_connections()

                        def filter_dirs(dir):
                            dir_closer = dir[0].dot(dir_to_closest_node)
                            if should_get_closer:
                                return dir_closer > 0

                            return dir_closer <= 0

                        potential_dirs_child = list(filter(filter_dirs, potential_dirs_child))

                        # We try to close the branch
                        if should_close_branch and max_stride_to_connect <= 6:
                            node_to_test = [closest_node] + [c["node"] for c in closest_node.neighbours]

                            res = False
                            for n in node_to_test:
                                res = self.try_close_branch(cur_node, n, potential_dirs_child, width_range,
                                                            height_range, cur_branch)
                                if res:
                                    break

                            if self.max_depth - 2 <= cur_len or res:  # Must close branch here
                                branch_failed = not res
                                break

                        child_node, dir_con, idx_con = self.try_potential_node(cur_node,
                                                                               potential_dirs=potential_dirs_child,
                                                                               max_try=self.max_retry_gen_room)
                        if not child_node:
                            branch_failed = True
                            break

                        cur_node.neighbours.append({"node": child_node, "idx": idx_con, "dir": dir_con})
                        child_node.neighbours.append({"node": cur_node, "dir": -dir_con})
                        cur_branch.append(child_node)
                        cur_node = child_node

                        cur_len += 1

            if not branch_failed and len(cur_branch) > 0:
                node, dir_con, idx_con = starting_node
                node.neighbours.append({"node": cur_branch[0], "idx": idx_con, "dir": dir_con})
                cur_branch[0].neighbours.append({"node": node, "dir": -dir_con})
                return cur_branch

            # If branch failed we clean it
            self.clean_failed_branch(cur_branch)
            cur_branch = []
            cur_len = 1

        return False

    def generate_main_branch(self, start_node: Node):
        cur_len = 1
        cur_branch = [start_node]

        cur_node = start_node
        while cur_len < self.min_len or (self.p_gen_room >= self.np_random.uniform() and cur_len <= self.max_depth):
            child_node, dir_conn, idx_con = self.try_potential_node(cur_node, max_try=self.max_retry_gen_room)

            if child_node:
                cur_node.neighbours.append({"node": child_node, "idx": idx_con, "dir": dir_conn})
                child_node.neighbours.append({"node": cur_node, "dir": -dir_conn})
                cur_branch.append(child_node)
                cur_node = child_node

                cur_len += 1
            else:
                break

        self.main_branch = cur_branch


class RoomGenerator:

    def __init__(self, root_node: Node, grid: object = None):

        self.root_node = root_node
        self.grid = grid
        self.visited_node_id_generation = []
        self.all_objects: Mapping[int, PhysicObject] = {}
        self.init = False
        #self.reset(self.root_node)

    def erase_all_room(self):

        id_visited = []

        def erase_room(node: Node):
            if node is None:
                return

            node.room = None
            id_visited.append(node.id_node)
            for c in node.neighbours:
                if not c["node"].id_node in id_visited:
                    erase_room(c["node"])

        erase_room(self.root_node)

    def fill_render(self, render: rendering.Viewer, reset: bool = True):
        if render is None:
            raise ValueError("Renderer cannot be None")

        if reset:
            render.remove_all_geom()

        geoms = self.root_node.room.get_all_objects(only_geom=True)
        for g in geoms:
            render.add_geom(g)

    def reset(self, root_node: Node):
        self.erase_all_room()
        self.grid.reset()
        self.all_objects = {}
        self.root_node = root_node
        self.generate_room(root_node)
        objects = self.root_node.room.get_all_objects(only_geom=False)
        for obj in objects:
            self.all_objects[obj.id] = obj
        if not self.init:
            self.init = True

    def generate_room(self, node: Node):
        node.room = node.room_type(node.position, node, self.grid)
        self.visited_node_id_generation.append(node.id_node)

        for neighbour in node.neighbours:
            if "idx" not in neighbour:
                continue

            connection, is_parent_bigger = node.room.generate_connection(neighbour["idx"], neighbour["node"])
            neighbour["connection"] = (connection, is_parent_bigger)
            child_parent = [n for n in neighbour["node"].neighbours if all(n["dir"] == -neighbour["dir"])][0]
            child_parent["connection"] = (connection, is_parent_bigger)

            if neighbour["node"].id_node in self.visited_node_id_generation:
                continue

            self.generate_room(neighbour["node"])


class Room:

    def __init__(self, center: np.ndarray, node: Node, grid: object = None, is_spawn: bool = False,
                 spawn_type: int = pathfinding.SPAWN_DEFENDER):
        self.center = center
        self.node = node
        self.grid = grid
        self.is_spawn = is_spawn
        self.spawn_type = spawn_type
        self._objects = None
        self._geom = None

    def reset_objects(self):
        self._objects = []

    def _get_objects(self, id_visited, only_geom=False):
        res = []
        res += self.geom if only_geom else self.objects
        id_visited.append(self.node.id_node)

        for child in self.node.neighbours:
            if not child["node"].id_node in id_visited:
                res += child["node"].room._get_objects(id_visited, only_geom=only_geom)

        return res

    def get_all_objects(self, only_geom=False):
        id_visited = []

        res = self._get_objects(id_visited, only_geom=only_geom)

        return res

    @property
    def objects(self):
        raise NotImplementedError

    @property
    def geom(self):
        raise NotImplementedError

    def generate_connection(self, idx_connection: int, child_node: Node):
        raise NotImplementedError

    @staticmethod
    def get_default_range_dim():
        raise NotImplementedError

    @staticmethod
    def get_potential_connections():
        raise NotImplementedError

    @staticmethod
    def get_second_dim_range(dim_range: np.ndarray):
        raise NotImplementedError

    @staticmethod
    def generate_room_dim(np_random: object, connection: np.ndarray, dim_range: np.ndarray = None):
        raise NotImplementedError


class RectRoom(Room):
    connections_direction = [np.array([0, 1.]),
                             np.array([1., 0]),
                             np.array([0, -1.]),
                             np.array([-1., 0])]

    wall_width = 5

    def generate_connection(self, idx_connection: int, child_node: Node) -> Tuple[np.ndarray, bool]:
        assert 0 <= idx_connection < len(self.__class__.connections_direction)

        con_direction = self.__class__.connections_direction[idx_connection]

        dim_vec = np.array([self.node.width, self.node.height]) / 2
        middle_connection = self.center + con_direction * (dim_vec - self.__class__.wall_width)
        normal_direction = np.array([-con_direction[1], con_direction[0]])

        dim_vec_child = np.array([child_node.width, child_node.height]) / 2

        if normal_direction.dot(normal_direction * dim_vec) > normal_direction.dot(normal_direction * dim_vec_child):
            connection_points = np.concatenate(
                [middle_connection + normal_direction * (dim_vec_child - 2 * self.__class__.wall_width),
                 middle_connection - normal_direction * (dim_vec_child - 2 * self.__class__.wall_width)])
            parent_bigger = True
        else:
            connection_points = np.concatenate(
                [middle_connection + normal_direction * (dim_vec - 2 * self.__class__.wall_width),
                 middle_connection - normal_direction * (dim_vec - 2 * self.__class__.wall_width)])
            parent_bigger = False

        return connection_points, parent_bigger

    def _generate_object_connection(self, connection_points: np.ndarray, con_direction: np.ndarray,
                                    from_parent: bool = False):

        normal_direction = np.array([-con_direction[1], con_direction[0]])
        if from_parent:
            normal_direction = -normal_direction
            connection_points[:2] -= con_direction * self.__class__.wall_width
            connection_points[2:] -= con_direction * self.__class__.wall_width

        dim_vec = np.array([self.node.width, self.node.height]) / 2
        offset = self.__class__.wall_width
        if from_parent:
            offset *= 2
        middle_connection = self.center + con_direction * (dim_vec - offset)

        scale_first = (middle_connection + normal_direction * dim_vec - connection_points[:2]) / 2
        center_first = connection_points[:2] + scale_first
        scale_first[scale_first == 0] = self.__class__.wall_width
        scale_first = np.abs(scale_first)

        scale_second = (middle_connection - normal_direction * dim_vec - connection_points[2:]) / 2
        center_second = connection_points[2:] + scale_second
        scale_second[scale_second == 0] = self.__class__.wall_width
        scale_second = np.abs(scale_second)

        color = np.array([1, 0, 0, 1])

        object_first = RectangleObject(w=scale_first[0] * 2, h=scale_first[1] * 2, pos=center_first,
                                       color=color, mass=0)
        object_second = RectangleObject(w=scale_second[0] * 2, h=scale_second[1] * 2, pos=center_second,
                                       color=color, mass=0)

        if self.grid:
            self.grid.add_object(center_first[0], center_first[1], scale_first[0], scale_first[1],
                                                 self.node.id_node + NodeGenerator.max_id * (len(self._objects)))
            self.grid.add_object(center_second[0], center_second[1], scale_second[0], scale_second[1],
                             self.node.id_node + NodeGenerator.max_id * (len(self._objects) + 1))

        return [object_first, object_second]

    @property
    def objects(self):
        if self._objects is None:
            self._objects = []

            connection_done = []

            for neighbour in self.node.neighbours:
                connection_points, parent_is_bigger = neighbour["connection"]

                if "idx" not in neighbour:  # Connection to a parent
                    connection_idx = -1
                    for idx, conn in enumerate(self.__class__.connections_direction):
                        if all(neighbour["dir"] == conn):
                            connection_idx = idx
                            break
                    connection_done.append(connection_idx)
                    if not parent_is_bigger:
                        self._objects += self._generate_object_connection(connection_points,
                                                                       neighbour["dir"],
                                                                       from_parent=True)
                else:
                    connection_done.append(neighbour["idx"])
                    if parent_is_bigger:
                        self._objects += self._generate_object_connection(connection_points,
                                                                       neighbour["dir"])

            transversal_wall_done = False
            has_long_transversal_wall = False
            for con_idx, dir_con in enumerate(self.__class__.connections_direction):
                if con_idx in connection_done:
                    continue

                node_dim = self.node.size / 2
                center = self.center + dir_con * (node_dim - self.__class__.wall_width)

                normal_con = np.array([-dir_con[1], dir_con[0]])
                scale = normal_con * node_dim
                scale[scale == 0] = self.__class__.wall_width
                scale = np.abs(scale)

                if not transversal_wall_done and self.grid:
                    limit_scale = [1 / 4, 0.45]

                    if has_long_transversal_wall:
                        limit_scale = [1 / 4, 0.35]

                    num_wall = np.random.choice(list(range(1, 3)))

                    size_gap_per_wall = self.node.size / (num_wall + 1)

                    starting_point = self.center + (dir_con + normal_con) * node_dim
                    for i in range(num_wall):
                        scale_rng = np.random.uniform(*limit_scale)
                        if scale_rng >= 0.5:
                            has_long_transversal_wall = True

                        scale_wall = scale_rng * dir_con * self.node.size / 2
                        scale_wall[scale_wall == 0] = self.__class__.wall_width / 1.5

                        center_wall = starting_point - size_gap_per_wall * (i + 1) * normal_con - scale_wall
                        scale_wall = np.abs(scale_wall)

                        oversize_factor = 1
                        if self.grid:
                            ids = self.grid.query_area(center_wall[0], center_wall[1], scale_wall[0] * oversize_factor,
                                                       scale_wall[1] * oversize_factor, -1)
                            if len(ids) > 0:
                                continue

                        self._objects.append(RectangleObject(w=scale_wall[0] * 2, h=scale_wall[1] * 2, pos=center_wall, mass=0))

                        if self.grid:
                            self.grid.add_object(center_wall[0], center_wall[1], scale_wall[0] * oversize_factor,
                                                 scale_wall[1] * oversize_factor,
                                                 self.node.id_node + (NodeGenerator.max_id * (len(self._objects) - 1)))

                    transversal_wall_done = True

                self._objects.append(RectangleObject(w=scale[0] * 2, h=scale[1] * 2, pos=center, mass=0))

                if self.grid:
                    self.grid.add_object(center[0], center[1], scale[0], scale[1],
                                     self.node.id_node + (NodeGenerator.max_id * (len(self._objects) - 1)))

            if self.node.is_spawn and self.grid:
                self._objects.append(RectangleObject(w=10, h=10, pos=self.center, mass=0))
                self._objects[-1].is_spawn = True
                self._objects[-1].spawn_type = self.node.spawn_type
                self.grid.add_object(self.center[0], self.center[1], 10, 10,
                                     self.node.id_node + (NodeGenerator.max_id * (len(self._objects) - 1)))



        return self._objects

    @property
    def geom(self):
        if self._geom is None:
            self._geom = [obj.geom for obj in self.objects]

        return self._geom

    @staticmethod
    def get_potential_connections():
        return [(d, idx) for idx, d in enumerate(RectRoom.connections_direction)]

    @staticmethod
    def get_default_range_dim():
        first_dim_range = np.array([50, 150])
        second_dim_range = np.array([0.8 * 50, 1.2 * 150])
        return first_dim_range, second_dim_range

    @staticmethod
    def get_second_dim_range(dim_range: np.ndarray):
        return dim_range * np.array([0.8, 1.2])

    @staticmethod
    def generate_room_dim(np_random: object, connection: np.ndarray, dim_range: np.ndarray = None):
        if dim_range is None:
            dim_range = RectRoom.get_default_range_dim()[0]

        first_dim = np_random.uniform(low=dim_range[0], high=dim_range[1])
        second_dim = np_random.uniform(low=0.8 * first_dim, high=1.2 * first_dim)
        return np.array([first_dim, second_dim])


class HallwayRoom(RectRoom):
    connections_direction = [np.array([0, 1.]),
                             np.array([1., 0]),
                             np.array([0, -1.]),
                             np.array([-1., 0])]


if __name__ == "__main__":

    width, height = (1280, 720)
    grid = Grid(width, height, num_rows=5, num_cols=5)
    grid_rooms = Grid(width, height, num_rows=20, num_cols=20)

    p_gen = 0.9
    max_depth = 7
    max_retry = 5
    room_type_p = {RectRoom: 1}
    dim_range = {RectRoom: np.array([100, 200])}
    viewer = rendering.Viewer(width, height)
    nodeGenerator = LinearNodeGenerator(p_gen, room_type_p, grid, np.random, max_depth=max_depth,
                                        render=viewer, max_retry_gen_branch=max_retry, p_close_branch=0.35,
                                        dim_range_room_type=dim_range, area_threshold=0.02)

    print("Node generated")
    roomGenerator = RoomGenerator(nodeGenerator.root_node, grid=grid_rooms)

    for i in range(10):
        nodeGenerator.reset(max_num_branches=5)
        nodeGenerator.fill_render(viewer)
        viewer.render()
        wait_input()
        roomGenerator.reset(nodeGenerator.root_node)
        print("room generated")
        roomGenerator.fill_render(viewer)
        viewer.render()
        print(len(nodeGenerator.root_node.room.objects))
        wait_input()
