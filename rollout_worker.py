import tornado
import tornado.web
import tornado.ioloop
from tornado.httpserver import HTTPServer
from tornado.websocket import websocket_connect
from models import Model_PPO
import zlib
import base64
import json
import pickle
import argparse
import signal
from policies import SimplePolicy, HistoryPolicy
from runner import Runner
import logging

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    import tensorflow as tf

    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

logger.info("logger initialised")

DEFAULT_LISTEN_ADDRESS = '127.0.0.1'


class RollOutHandler(tornado.websocket.WebSocketHandler):
    """
    Handler that handles a websocket channel for launching rollout worker
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request.connection.stream.max_buffer_size = 512 * 1024 * 1024

    def open(self, *args, **kwargs):
        """
        Client opens a websocket
        """
        logger.info("Client opened a socket on rollout %r %r", args, kwargs)

    def init_runner(self, parameters, env_fn, models_args, models_kwargs, batch_sizes):

        for idx in range(len(models_args)):
            if models_args[idx][1][1]:  # Recurrent model
                models_args[idx] = (*models_args[idx][:-1], 1)  # Only one step

        self.application.env = env_fn()

        if self.application.runner:
            del self.application.runner

        self.application.runner = Runner(self.application.env, parameters["gamma"], parameters["lambda"],
                                         parameters["rollout_size"], len(models_args), parameters["timestep_size"])

        models = [Model_PPO(*args, **kwargs) for args, kwargs in zip(models_args, models_kwargs)]

        for idx, m in enumerate(models):
            m.warm_up(1)

        self.application.policies = [SimplePolicy(m, name="agent_{}".format(idx)) for idx, m in enumerate(models)]

        self.application.init = True

    def on_message(self, message):
        """
        Message received on channel
        """
        logger.info("got message ")
        parsed = json.loads(message)
        logger.info("message keys {}".format(parsed.keys()))

        try:
            if parsed["type"] == "initialization":
                if self.application.init:
                    raise ValueError("Rollout worker is already assigned to a PPO server")

                env_fn = pickle.loads(base64.b64decode(parsed["env_fn"].encode("ascii")))
                models_args = pickle.loads(base64.b64decode(parsed["models_args"].encode("ascii")))
                models_kwargs = pickle.loads(base64.b64decode(parsed["models_kwargs"].encode("ascii")))
                batch_sizes = pickle.loads(base64.b64decode(parsed["batch_sizes"].encode("ascii")))

                self.init_runner(parsed["parameters"], env_fn, models_args, models_kwargs, batch_sizes)

                message = {"type": "model_poll"}
                tornado.ioloop.IOLoop.current().add_callback(self.write_message, json.dumps(message))

            elif parsed["type"] == "model":
                if not self.application.init:
                    raise ValueError("Rollout worker is not initialised")

                for idx, raw_model in enumerate(parsed["models_train"]):
                    # model_train = extract_model_from_message(raw_model, self.application.policies[idx].model.loss_dict)
                    raw = base64.b64decode(raw_model.encode("ascii"))
                    with open("temp_remote.hdf5", "wb") as f:
                        f.write(raw)

                    self.application.policies[idx].model.load_weights("temp_remote.hdf5")

                frac = float(parsed["frac"])
                self.application.env.set_frac(frac)

                data_trajectory = self.application.runner.roll_out_all_agent(self.application.policies)

                binary_data = pickle.dumps(data_trajectory)
                compressed_data = zlib.compress(binary_data, 7)

                message = {"type": "trajectory_data", "data": base64.b64encode(compressed_data).decode("ascii")}
                msg = json.dumps(message)
                logger.info(len(msg))
                self.write_message(msg)

            elif parsed["type"] == "close":
                self.application.init = False
                self.close()
            elif parsed["type"] == "shutdown":
                self.application.init = False
                tornado.ioloop.IOLoop.current().add_callback(shutdown)
                self.close()
            else:
                logger.warning("Misformed message %r", parsed)
        except:
            logger.error("Error when processing message", exc_info=True)

    def on_close(self):
        """
        Channel is closed
        """
        self.application.init = False
        reason = "none"
        if not self.close_reason is None: reason = self.close_reason

        logger.info("Channel closed, reason : {}".format(reason))

    def get_compression_options(self):
        return {"compression_level": 6, "mem_level": 9}


class ApplicationRollOut(tornado.web.Application):

    def __init__(self, handlers, **kwargs):
        self.init = False
        self.policies = []
        self.runner = None
        self.env = None

        super(ApplicationRollOut, self).__init__(handlers, **kwargs)


MAX_WAIT_SECONDS_BEFORE_SHUTDOWN = 1


def sig_handler(sig, frame):
    logger.warning('Caught signal: %s', sig)
    tornado.ioloop.IOLoop.current().add_callback_from_signal(shutdown)


def shutdown():
    logger.info('Stopping http server')
    http_server.stop()

    if not app is None and not app.env is None:
        app.env.close()

    logger.info('Will shutdown in %s seconds ...', MAX_WAIT_SECONDS_BEFORE_SHUTDOWN)
    io_loop = tornado.ioloop.IOLoop.current()

    deadline = io_loop.time() + MAX_WAIT_SECONDS_BEFORE_SHUTDOWN

    def stop_loop():
        now = io_loop.time()
        if now < deadline:
            io_loop.add_timeout(now + 1, stop_loop)
        else:
            # app.threadPool.shutdown()
            io_loop.stop()
            logger.info('Shutdown')

    stop_loop()


def main(args):
    global http_server, app
    app = ApplicationRollOut(([
        (r'/rollout_worker', RollOutHandler),  # Route/Handler/kwargs
    ]), websocket_max_message_size=1024 * 1024 * 512,
        max_buffer_size=1024 * 1024 * 512) # 512 Mb

    # Setup HTTP Server
    logger.info("Starting server for rollout server on port : " + str(args.port))

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(args.port, args.address)

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    # Start IO/Event loop
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Find out the port for the application")
    parser.add_argument('--port', required=False, type=int, help="the port where the server will be bound",
                        default=8080)
    parser.add_argument('--conf', default="confServer.ini", type=str, help="Configuration file for the server")
    parser.add_argument('--address', default=DEFAULT_LISTEN_ADDRESS, type=str, help="Listening address for the server")
    args = parser.parse_args()

    main(args)
