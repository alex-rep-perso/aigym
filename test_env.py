import math
from typing import List
import gym
from gym import spaces, logger
from gym.utils import seeding
from objects import Agent, PhysicObject
import numpy as np
import time
from physics_engine.PyWorld import PyWorld
from physics_engine.loose_grid import Grid
from world_generator import LinearNodeGenerator, RoomGenerator, RectRoom
from physics_engine.PyPathfinding import PyPathfinder

try:
    import rendering

    g_rendering_enabled = True
except:
    g_rendering_enabled = False
    print("Cannot import render")


class TestEnv(gym.Env):

    def __init__(self, width, height, agents, objects=None, num_cols=5, num_rows=5, seed=None, timesteps=1000):

        assert all(isinstance(agent, Agent) for agent in agents), "Error agent argument must subclass Agent"
        self.agents = agents
        self.objects = objects
        self.num_agents = len(agents)

        self.all_entities = []
        if not self.agents is None:
            self.all_entities += self.agents
        if not self.objects is None:
            self.all_entities += self.objects

        self.num_entities = len(self.all_entities)
        self.max_timesteps = timesteps

        self.physics_world = PyWorld(0, -15, 100, num_cols, num_rows, width, height)
        self.tlast_frame = None

        self.cur_step = 0

        self.seed_state = seed
        self.seed(seed=seed)

        self.observation_space = None
        self.action_space = None

        self.width = width
        self.height = height

        self._viewer = None
        self.init_viewer()

    def init_viewer(self):
        a = self.viewer

    @property
    def viewer(self):
        if self._viewer is None:

            self._viewer = rendering.Viewer(self.width, self.height)

            for i in range(self.num_agents):
                if isinstance(self.agents[i].geom, list):
                    for g in self.agents[i].geom:
                        self.viewer.add_geom(g)
                else:
                    self.viewer.add_geom(self.agents[i].geom)

            if self.objects:
                for i in range(len(self.objects)):
                    if isinstance(self.objects[i].geom, list):
                        for g in self.objects[i].geom:
                            self.viewer.add_geom(g)
                    else:
                        self.viewer.add_geom(self.objects[i].geom)

        return self._viewer

    def _step(self, actions):

        if self.tlast_frame is None:
            tau = 1 / 60
            self.tlast_frame = time.perf_counter()
        else:
            tcur_frame = time.perf_counter()
            tau = tcur_frame - self.tlast_frame
            self.tlast_frame = tcur_frame
        self.physics_world.step(tau)

        for i, agent in enumerate(self.agents):
            agent.step(actions)

        if not self.objects is None:
            for o in self.objects:
                o.step()

    def step(self, actions):

        self.cur_step += 1
        done = self.cur_step == self.max_timesteps

        if not self.viewer is None:
            for ent in self.all_entities:
                if ent.colliding:
                    ent.colliding = False
                    ent.set_color((0, 0, 1, 1))

        self._step(actions)

        return [], 0, done, {}

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        for a in self.agents:
            a.np_random = self.np_random
        if not self.objects is None:
            for o in self.objects:
                o.np_random = self.np_random
        return [seed]

    def reset(self):

        self.cur_step = 0

        self.check_coll = np.zeros((len(self.all_entities), len(self.all_entities)), dtype=np.bool)

        self.seed_state = self.seed_state + 1 if isinstance(self.seed_state, int) else None
        self.seed(seed=self.seed_state)

        obs = []

        if not self.agents is None:
            for agent in self.agents:
                obs.append(agent.reset(self.width, self.height, self.all_entities))

        if not self.objects is None:
            for o in self.objects:
                o.reset(self.width, self.height)

        self.physics_world.clear()
        for ent in self.all_entities:
            self.physics_world.add_proxy(ent.physics_proxy)

        self.tlast_frame = None

        return tuple(obs)

    def set_frac(self, frac):
        self.frac = frac

    def render(self, mode='human'):
        if not g_rendering_enabled:
            return

        geom_contact = []
        for contact in self.physics_world.get_all_contact_points():
            g = rendering.Geom("circle", color=np.array((1, 0, 0, 1)), translation=contact, scale=np.array([3, 3]))
            geom_contact.append(g)
            self.viewer.add_geom(g)

        self.viewer.render()

        if not geom_contact is None:
            # print("len contacts ", len(geom_contact))
            for idx, g in enumerate(geom_contact):
                # print(self.objects[idx].physics_proxy.get_colliding_ids())
                self.viewer.remove_geom(g)

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None


class TestGeneratedWorldEnv(gym.Env):

    def __init__(self, width, height, agents, objects=None, num_cols=5, num_rows=5,
                 seed=None, timesteps=1000, interval_angle=90):

        assert all(isinstance(agent, Agent) for agent in agents), "Error agent argument must subclass Agent"
        self.agents: List[Agent] = agents
        self.objects: List[PhysicObject] = objects
        self.num_agents = len(agents)

        self.all_entities = []
        if self.agents:
            self.all_entities += self.agents
        if self.objects:
            self.all_entities += self.objects

        self.num_entities = len(self.all_entities)
        print(f"num entities {self.num_entities}")
        self.max_timesteps = timesteps

        self.physics_world = PyWorld(0, 0, 100, num_cols, num_rows, width, height)
        self.tlast_frame = None

        self.cur_step = 0

        self.seed_state = seed
        self.seed(seed=seed)

        self._viewer = None

        self.observation_space = None
        self.action_space = None

        self.width = width
        self.height = height

        self.nodeGenerator = None
        self.roomGenerator = None
        self.pathfinder = None
        self.selected_cell = None
        self.prev_color = None
        self.selected_path = None
        self.cur_action = [None] * len(self.agents)
        self.interval_angle = interval_angle
        self._debug_obj = []

        self.init_world_generator()
        self.pathfinder = PyPathfinder(width, height, 5, 5, self.roomGenerator.grid, self.nodeGenerator)
        for a in self.agents:
            a.pathfinder = self.pathfinder
        self.init_viewer()

    def init_viewer(self):
        a = self.viewer
        self.viewer.config.add_click_listener(self.select_room_object_click)

    def clear_path(self):
        if self.selected_path:
            for cItem in self.selected_path:
                cell = self.pathfinder.cells_row[cItem.y][cItem.x]
                cell.set_color(np.array([0, 1, 0, 1]))
            self.selected_path = None

    def select_room_object_click(self, x: int, y: int, button: int):

        print(f"room selection {x} - {y}")
        x_real = x * self.width
        y_real = self.height - y * self.height
        print(f"room selection {x_real} - {y_real}")
        ids = self.roomGenerator.grid.query_area(x_real, y_real, 5, 5, -1)
        print(f"ids : {ids}")
        for id in ids:
            obj_id = id >> LinearNodeGenerator.max_id_bin
            node_id = id - (obj_id << LinearNodeGenerator.max_id_bin)

            print(f"obj_id : {obj_id}, node_id : {node_id}")
            room = self.nodeGenerator.all_nodes[node_id].room
            print(room)
            print(room.node.position)
            if 0 <= obj_id < len(room.objects):
                obj = room.objects[obj_id]

                print(obj.pos, obj.id)
                c = self.nodeGenerator.np_random.uniform(size=4)
                c[-1] = 1
                obj.geom.set_color(c)

        col, row = self.pathfinder.to_cell_coord(x_real, y_real)
        print(f"row : {row} - col : {col}")

        agent = self.agents[0] if button == 2 else self.agents[1]

        col_agent, row_agent = self.pathfinder.to_cell_coord(*agent.pos.tolist())
        big_grid_action = np.array([(col - col_agent) // agent.small_grid_size,
                                        (row - row_agent) // agent.small_grid_size])
        small_grid_action = np.array([col - big_grid_action[0] * agent.small_grid_size - col_agent,
                                      row - big_grid_action[1] * agent.small_grid_size - row_agent])

        big_size = (agent.big_grid_size - 1) // 2
        small_size = (agent.small_grid_size - 1) // 2

        if all(np.abs(big_grid_action) <= big_size):
            big_grid_action += big_size
            small_grid_action += small_size

            idx_action = 0 if button == 2 else 1
            self.cur_action[idx_action] = np.concatenate([big_grid_action, small_grid_action])
        else:
            print("click outside of agent range")

    def init_world_generator(self):

        grid = Grid(self.width, self.height, num_rows=5, num_cols=5)
        grid_rooms = Grid(self.width, self.height, num_rows=20, num_cols=20)

        p_gen = 0.9
        max_depth = 10
        max_retry = 5
        room_type_p = {RectRoom: 1}
        dim_range = {RectRoom: np.array([100, 200])}
        self.nodeGenerator = LinearNodeGenerator(p_gen, room_type_p, grid, np.random, max_depth=max_depth,
                                                 render=None, max_retry_gen_branch=max_retry, p_close_branch=0.35,
                                                 dim_range_room_type=dim_range, area_threshold=0.02, min_len=7)

        self.roomGenerator = RoomGenerator(self.nodeGenerator.root_node, grid=grid_rooms)

    def _step(self, actions):

        if self.tlast_frame is None:
            tau = 1 / 60
            self.tlast_frame = time.perf_counter()
        else:
            tcur_frame = time.perf_counter()
            tau = min(tcur_frame - self.tlast_frame, 1 / 30)
            self.tlast_frame = tcur_frame
        self.physics_world.step(tau)

        for i, agent in enumerate(self.agents):
            agent.step(actions[i])

        if not self.objects is None:
            for o in self.objects:
                o.step()

    def _draw_lidar(self, debug_obj_empty, rad_angle, agent, dir, idx, distances, idx_agent):
        pos = agent.pos + dir * distances[idx] / 2
        scale = np.abs(np.array([distances[idx] / 2, 2]))
        rot = np.array([rad_angle])
        if distances[idx] < 0:
            pos = agent.pos
            rot = np.zeros((1,))
            scale = np.array([1, 1])
        if debug_obj_empty:
            self._debug_obj.append(rendering.Geom("rectangle", translation=pos,
                                                  rotation=rot, scale=scale, color=agent.color))
        else:
            self._debug_obj[2 * idx + idx_agent].set_translation(pos)
            self._debug_obj[2 * idx + idx_agent].set_scale(scale)
            self._debug_obj[2 * idx + idx_agent].set_rotation(rot)

    def step(self, actions):

        self.cur_step += 1
        done = self.cur_step == self.max_timesteps

        self._step(self.cur_action)
        self.cur_action = np.ones((4,))
        self.cur_action[:2] *= (self.agents[0].big_grid_size - 1) // 2
        self.cur_action[2:] *= (self.agents[0].small_grid_size - 1) // 2
        self.cur_action = [self.cur_action for _ in self.agents]


        debug_log = False#any(a.last_pos != a.pos)
        distances = [self.physics_world.body_lidar(self.interval_angle, a.id, debug_log) for a in self.agents]

        debug_obj_empty = len(self._debug_obj) == 0
        if debug_log:
            print(distances, end='\n')
        for idx, angle in enumerate(range(0, 360, self.interval_angle)):
            rad_angle = math.pi / 180 * angle
            dir = np.array([math.cos(rad_angle), math.sin(rad_angle)])

            self._draw_lidar(debug_obj_empty, rad_angle, self.agents[0], dir, idx, distances[0], 0)
            self._draw_lidar(debug_obj_empty, rad_angle, self.agents[1], dir, idx, distances[1], 1)


        return [], 0, done, {}

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        for a in self.agents:
            a.np_random = self.np_random
        if not self.objects is None:
            for o in self.objects:
                o.np_random = self.np_random
        return [seed]

    def reset(self):

        self.cur_step = 0
        self.cur_action = np.ones((4,))
        self.cur_action[:2] *= (self.agents[0].big_grid_size - 1) // 2
        self.cur_action[2:] *= (self.agents[0].small_grid_size - 1) // 2
        self.cur_action = [self.cur_action for _ in self.agents]

        self.seed_state = self.seed_state + 1 if isinstance(self.seed_state, int) else None
        self.seed(seed=self.seed_state)

        if self.roomGenerator and self.roomGenerator.init:
            for g in self.roomGenerator.root_node.room.get_all_objects(only_geom=True):
                if g.render_index == -1:
                    continue
                self.viewer.remove_geom(g)

        for g in self._debug_obj:
            if g.render_index == -1:
                continue
            self.viewer.remove_geom(g)

        self._debug_obj = []

        self.nodeGenerator.reset()
        self.roomGenerator.reset(self.nodeGenerator.root_node)
        self.pathfinder.checking_collision()

        obs = []

        if not self.agents is None:
            for agent in self.agents:
                obs.append(agent.reset())

        if not self.objects is None:
            for o in self.objects:
                o.reset(self.width, self.height)

        self.physics_world.clear()
        for ent in self.all_entities:
            self.physics_world.add_proxy(ent.physics_proxy)

        for obj in self.roomGenerator.root_node.room.get_all_objects(only_geom=False):
            if obj.is_spawn:
                continue
            self.physics_world.add_proxy(obj.physics_proxy)
            self.viewer.add_geom(obj.geom)

        self.tlast_frame = None

        return tuple(obs)

    def set_frac(self, frac):
        self.frac = frac

    @property
    def viewer(self):
        if self._viewer is None:

            self._viewer = rendering.Viewer(self.width, self.height)

            for i in range(self.num_agents):
                if isinstance(self.agents[i].geom, list):
                    for g in self.agents[i].geom:
                        self.viewer.add_geom(g)
                else:
                    self.viewer.add_geom(self.agents[i].geom)

            if self.objects:
                for i in range(len(self.objects)):
                    if isinstance(self.objects[i].geom, list):
                        for g in self.objects[i].geom:
                            self.viewer.add_geom(g)
                    else:
                        self.viewer.add_geom(self.objects[i].geom)

            # for cell in self.pathfinder.cells:
            #     self.viewer.add_geom(cell)

        return self._viewer

    def render(self, mode='human'):
        if not g_rendering_enabled:
            return

        geom_contact = []
        for contact in self.physics_world.get_all_contact_points():
            g = rendering.Geom("circle", color=np.array((1, 0, 0, 1)), translation=contact, scale=np.array([3, 3]))
            geom_contact.append(g)
            self.viewer.add_geom(g)

        for g in self._debug_obj:
            if g.render_index == -1:
                self.viewer.add_geom(g)

        self.viewer.render()

        if geom_contact:
            # print("len contacts ", len(geom_contact))
            for idx, g in enumerate(geom_contact):
                # print(self.objects[idx].physics_proxy.get_colliding_ids())
                self.viewer.remove_geom(g)

    def close(self):
        if self.viewer:
            self.viewer.close()
