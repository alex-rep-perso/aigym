import sys

import cProfile

from chasing_env import ChasingEnv
from objects import *
from physics_engine.loose_grid import Grid
from test_env import TestEnv, TestGeneratedWorldEnv
from rendering import ViewerConfig, Viewer
import numpy as np
import time


def main():
    #grid = Grid(640, 480, num_rows=5, num_cols=7)

    height = 720
    width = 1280

    # objs = [FlockObject(100, grid, "flock", 5, 15, 5, 0.5, range_speed=np.array([-1, -1, 1, 1]) * 2,
    #                               range_radius=np.array([2, 4]), color=(0, 0, 1),
    #                                range_force_ensemble=np.array([-5, -5, 5, 5]))]
    # for i in range(np.random.randint(30, 31)):
    #   objs.append(FlockObject(25, grid, "flock", 80, 3, 0.4, 1, range_speed=np.array([-1, -1, 1, 1]),
    #                               range_radius=np.array([4, 4]), color=(0, 0, 1)))

    # objs = [TurretObject(color=(0, 0 ,1),
    #                 range_pos=np.array([-grid.width/4, -grid.height/4, grid.width/4, grid.height/4]),
    #                 range_rot_speed=np.array([0, 0.2]), range_radius_ext = np.array([100, 300]),
    #                 range_radius = np.array([10, 40]), range_angle_span=np.array([10, 35]),
    #                 range_nofire_len=np.array([100, 235]), range_fire_len=np.array([400, 505]))]

    # objs = [SphereObject(color=(0, 0, 1, 1),
    #                      range_speed=np.array([-1.5, -1.5, 1.5, 1.5]) * 3,
    #                      range_pos=np.array([-grid.width / 2, -grid.height / 2, grid.width / 2, grid.height / 2])
    #                      , range_radius=np.array([5, 35])) for i in range(200)]

    objs = [RectangleObject(color=(0, 0, 1, 1), range_speed=np.array([-1, -1, 1, 1]) * 1.5, range_w=np.array([30, 30]),
                            range_h=np.array([30, 30]), range_pos=np.array([-width / 2, 0, width / 2, height / 2]),
                            mass=1000) for i in range(200)]

    objs += [
        RectangleObject(color=(0, 0, 0, 1), range_speed=np.array([0, 0, 0, 0]), range_w=np.array([width, width + 1]),
                        range_h=np.array([100, 100]), range_pos=np.array([0, -height / 2, 0, -height / 2]), mass=0)
        ]
    color = np.array([0, 0, 1, 1])

    # objs = [SphereObject(grid.height, grid.width, color=color,
    #                      range_speed=np.array([0, 0, 0, 0]),
    #                      range_pos=(np.array([0, 0, 0, 0]) + np.array([(50 * i) + 10 if i > 0 else 0 , 0, 50 * i + 10 if i > 0 else 0, 0])),
    #                      range_radius=np.array([50, 50])) for i in range(2)]
    #env = TestEnv(width, height, [], num_rows=5, num_cols=7, objects=objs, timesteps=1050)
    agents = [PathRectangleRunner(50, 10, 10, target='attacker', defend=True, range_w=np.array([10, 15]), range_h=np.array([10, 15]),
                                big_grid_size=11, small_grid_size=7, range_scalar_speed=np.array([50, 150])),
              PathRectangleRunner(50, 10, 10, target='defender', color=np.array([0, 0, 1, 1]), range_w=np.array([10, 15]), range_h=np.array([10, 15]),
                                  big_grid_size=11, small_grid_size=7, range_scalar_speed=np.array([50, 150]))
              ]
    agents[0].marker = "defender"
    agents[1].marker = "attacker"
    env = ChasingEnv(agents, width, height, objects=None, timesteps=10, interval_angle=10,
                     rendering=False)
    # env = TestGeneratedWorldEnv(width, height, agents, num_rows=5, num_cols=7, objects=None, timesteps=50000,
    #                       interval_angle=12)
    # pr = cProfile.Profile()
    # pr.enable()
    num_episode = 2000
    idx_ep = 1
    actions = [ np.array([3, 7, 2, 6], dtype=np.int32), np.array([4, 2, 2, 1], dtype=np.int32)]
    env.reset()
    tstart = time.perf_counter()
    fps_counter = []
    wrong_spawn = 0
    while idx_ep <= num_episode:

        obs, _, done, _ = env.step(actions)
        env.render()
        if 0 < wrong_spawn <= 4:
            input()
            wrong_spawn += 1

        if done:
            tnow = time.perf_counter()
            # Calculate the fps (frame per second)
            fps = int(env.max_timesteps / (tnow - tstart))
            print("fps : {}".format(fps))
            fps_counter.append(fps)
            env.reset()
            if (all(agents[0].pos == 0)):
                print("defender")
                wrong_spawn = 1
            if (all(agents[1].pos == 0)):
                print("attacker")
                wrong_spawn = 1
            idx_ep += 1
            tstart = time.perf_counter()

    # pr.disable()
    fps_counter = np.array(fps_counter)
    print("fps average {} - std {}".format(np.mean(fps_counter), np.std(fps_counter)))
    # pr.print_stats(sort="tottime")


if __name__ == '__main__':
    main()
