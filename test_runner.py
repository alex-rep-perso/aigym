import unittest
from runner import Runner
import random
import numpy as np
import copy

class DummyEnv:

    def __init__(self, nenv, nsteps):
        self.num_envs = nenv
        self.max_timesteps = nsteps

    def reset(self):

        return [np.array([0, 1])]

class TestRunner(unittest.TestCase):

    def setUp(self):
        self.nsteps = 500
        self.env = DummyEnv(32, self.nsteps)
        self.reset_runner()
        self.buffer = None

    def reset_runner(self):
        self.runner = Runner(self.env, 0.99, 0.95, self.nsteps)

    def sanitize_minibatches(self, num_agents, use_buffer=False):

        obs = [np.arange(self.runner.nsteps * self.runner.nenv * 4).reshape((self.runner.nsteps, self.runner.nenv, 4))] * num_agents
        rewards = [np.arange(self.runner.nsteps * self.runner.nenv * 1).reshape((self.runner.nsteps, self.runner.nenv, 1))] * num_agents
        actions = [np.arange(self.runner.nsteps * self.runner.nenv * 2).reshape((self.runner.nsteps, self.runner.nenv, 2))] * num_agents
        values = [np.arange(self.runner.nsteps * self.runner.nenv * 1).reshape((self.runner.nsteps, self.runner.nenv, 1))] * num_agents
        neglogp = [np.arange(self.runner.nsteps * self.runner.nenv * 1).reshape((self.runner.nsteps, self.runner.nenv, 1))] * num_agents

        dones = np.zeros((self.runner.nsteps, self.runner.nenv), dtype=np.bool)
        self.runner.dones = np.zeros((self.runner.nenv,), dtype=np.bool)

        for idx_env in range(self.runner.nenv):

            min_num_done = 0
            if not self.buffer is None and use_buffer:
                if len(self.buffer[0][idx_env][0]) >= 1:
                    min_num_done = 1

            num_done = np.random.randint(min_num_done, 25)

            if num_done == 0:
                if self.runner.dones[idx_env]:
                    dones[0, idx_env] = True
                self.runner.dones[idx_env] = True

            idx_step = 0


            for _ in range(num_done):
                if idx_step >= self.runner.nsteps:
                    break

                if idx_step == 0 and not self.buffer is None and use_buffer:
                    max_step = self.runner.nsteps + 1 - len(self.buffer[0][idx_env][0])
                    if len(self.buffer[0][idx_env][0]) > 0:
                        idx_step = 1
                else:
                    max_step = self.runner.nsteps + 1
                idx_done = np.random.randint(idx_step, max_step)

                if idx_done == 0 or self.runner.dones[idx_env]: # True at step 0 does not count as it was taken into account in previous batch
                    dones[0, idx_env] = True
                    idx_step = 1
                    idx_done = np.random.randint(idx_step, max_step)

                if idx_done == self.runner.nsteps:
                    self.runner.dones[idx_env] = True
                else:
                    dones[idx_done, idx_env] = True
                    self.runner.dones[idx_env] = False

                idx_step = idx_done + 2

        num_dones = np.sum(dones) + np.sum(self.runner.dones) - np.sum(dones[0, :])

        #print(num_dones)
        if not use_buffer:
            self.runner.reset_env_data_buffer()

        self.runner.init_env_data_buffer(num_agents)
        res_obs, res_rewards, res_actions, res_values, res_neglogp, res_mask = self.runner.sanitize_minibatches(obs,
                                                                        rewards, actions, values, neglogp, dones, num_agents)

        for out in [res_obs, res_rewards, res_actions, res_values, res_neglogp]:
            self.assertIsInstance(out, list)
            for idx_a in range(num_agents):
                self.assertEqual(num_dones, out[idx_a].shape[1])


        self.assertEqual(res_obs[0].shape, (self.runner.nsteps, num_dones, 4))

        idx_res_env = 0
        #print(num_dones, res_mask.shape[1])
        cond_equal = num_dones == res_mask.shape[1]
        if not cond_equal:
            for i in range(res_mask.shape[1]):
                print(res_mask[:, i])
                print(res_rewards[0][:, i, 0])
            for i in range(dones.shape[1]):
                print(dones[:, i])
                print(self.runner.dones[i])
        for idx_env in range(self.runner.nenv):
            idx_dones_env = np.where(dones[:, idx_env] == True)[0]
            if self.runner.dones[idx_env]:
                idx_dones_env = np.concatenate([idx_dones_env, np.array([self.runner.nsteps])])


            if (len(idx_dones_env) == 1 or (dones[0, idx_env] and len(idx_dones_env) == 2)) and self.runner.dones[idx_env]:
                for idx_a in range(num_agents):
                    cond = np.array_equal(rewards[idx_a][:, idx_env, :], res_rewards[idx_a][:, idx_res_env, :])
                    if not cond:
                        print(rewards[idx_a][:, idx_env, :])
                        print(res_rewards[idx_a][:, idx_res_env, :])
                        print(idx_res_env, idx_env)
                        print(dones[:, idx_env])
                        if idx_env < self.runner.nenv - 1:
                            print(dones[:, idx_env + 1])

                    self.assertTrue(cond)

                idx_res_env += 1
                continue

            start_idx = 0
            len_buffer = 0
            if use_buffer and not self.buffer is None:
                len_buffer = len(self.buffer[1][idx_env][0])

            for idx_done in idx_dones_env:
                if idx_done == 0:
                    continue
                if start_idx > 0:
                    len_buffer = 0

                len_traj = idx_done - start_idx

                for idx_a in range(num_agents):
                    cond = np.array_equal(rewards[idx_a][start_idx:start_idx + len_traj, idx_env, :],
                                            res_rewards[idx_a][len_buffer:len_traj + len_buffer, idx_res_env, :])

                    self.assertTrue(np.all(res_rewards[idx_a][len_traj + len_buffer:, idx_res_env] == 0))
                    mask = np.ones((self.nsteps,))
                    mask[len_traj + len_buffer:] = 0

                    cond2 = np.array_equal(mask, res_mask[:, idx_res_env])
                    cond3 = True
                    if start_idx == 0 and use_buffer and not self.buffer is None and len_buffer > 0:
                        cond3 = np.array_equal(self.buffer[1][idx_env][idx_a], res_rewards[idx_a][:len_buffer, idx_res_env, :])
                    if not cond or not cond2 or not cond3:
                        print(idx_dones_env)
                        if use_buffer and not self.buffer is None:
                            print(cond3)
                            print("len buffer : ", len_buffer)
                            print("buffer : ", self.buffer[1][idx_env][idx_a])
                            print("buffer res : ", res_rewards[idx_a][:len_buffer, idx_res_env, :])
                        print(mask)
                        print(res_mask[:, idx_res_env])
                        print(rewards[idx_a][start_idx:start_idx + len_traj, idx_env, :])
                        print(res_rewards[idx_a][:len_traj, idx_res_env, :])
                        print(idx_env, idx_res_env)
                        print(idx_done, len_traj)
                    self.assertTrue(cond and cond2 and cond3)


                start_idx = idx_done
                idx_res_env += 1

        self.assertTrue(cond_equal)

        self.buffer = copy.deepcopy(self.runner.env_data_buffer)
        for i in range(len(self.buffer)):
            for i2 in range(len(self.buffer[i])):
                for idx_a in range(num_agents):
                    self.buffer[i][i2][idx_a] = self.runner.env_data_buffer[i][i2][idx_a].copy()
        #print(self.buffer[1][0][0])

    def sanitize_minibatches_reverse(self, num_agents, use_buffer=False):

        obs = [[]] * num_agents
        rewards = [[]] * num_agents
        actions = [[]] * num_agents
        values = [[]] * num_agents
        neglogp = [[]] * num_agents

        dones = np.zeros((self.runner.nsteps, self.runner.nenv), dtype=np.bool)
        self.runner.dones = np.zeros((self.runner.nenv,), dtype=np.bool)

        for idx_env in range(self.runner.nenv):

            min_num_done = 1
            

            num_done = np.random.randint(min_num_done, 25)

            if num_done == 0:
                if self.runner.dones[idx_env]:
                    dones[0, idx_env] = True
                self.runner.dones[idx_env] = True

            idx_step = 0


            for _ in range(num_done):
                if idx_step >= self.runner.nsteps:
                    break

                if idx_step == 0 and not self.buffer is None and use_buffer:
                    max_step = self.runner.nsteps + 1 - len(self.buffer[0][idx_env][0])
                    if len(self.buffer[0][idx_env][0]) > 0:
                        idx_step = 1
                else:
                    max_step = self.runner.nsteps + 1
                idx_done = np.random.randint(idx_step, max_step)

                if idx_done == 0 or self.runner.dones[idx_env]: # True at step 0 does not count as it was taken into account in previous batch
                    dones[0, idx_env] = True
                    idx_step = 1
                    idx_done = np.random.randint(idx_step, max_step)

                if idx_done == self.runner.nsteps:
                    self.runner.dones[idx_env] = True
                else:
                    dones[idx_done, idx_env] = True
                    self.runner.dones[idx_env] = False

                idx_step = idx_done + 2

        num_dones = np.sum(dones) + np.sum(self.runner.dones) - np.sum(dones[0, :])

        #print(num_dones)
        if not use_buffer:
            self.runner.reset_env_data_buffer()

        self.runner.init_env_data_buffer(num_agents)
        res_obs, res_rewards, res_actions, res_values, res_neglogp, res_mask = self.runner.sanitize_minibatches(obs,
                                                                        rewards, actions, values, neglogp, dones, num_agents)

        for out in [res_obs, res_rewards, res_actions, res_values, res_neglogp]:
            self.assertIsInstance(out, list)
            for idx_a in range(num_agents):
                self.assertEqual(num_dones, out[idx_a].shape[1])


        self.assertEqual(res_obs[0].shape, (self.runner.nsteps, num_dones, 4))

        idx_res_env = 0
        #print(num_dones, res_mask.shape[1])
        cond_equal = num_dones == res_mask.shape[1]
        if not cond_equal:
            for i in range(res_mask.shape[1]):
                print(res_mask[:, i])
                print(res_rewards[0][:, i, 0])
            for i in range(dones.shape[1]):
                print(dones[:, i])
                print(self.runner.dones[i])
        for idx_env in range(self.runner.nenv):
            idx_dones_env = np.where(dones[:, idx_env] == True)[0]
            if self.runner.dones[idx_env]:
                idx_dones_env = np.concatenate([idx_dones_env, np.array([self.runner.nsteps])])


            if (len(idx_dones_env) == 1 or (dones[0, idx_env] and len(idx_dones_env) == 2)) and self.runner.dones[idx_env]:
                for idx_a in range(num_agents):
                    cond = np.array_equal(rewards[idx_a][:, idx_env, :], res_rewards[idx_a][:, idx_res_env, :])
                    if not cond:
                        print(rewards[idx_a][:, idx_env, :])
                        print(res_rewards[idx_a][:, idx_res_env, :])
                        print(idx_res_env, idx_env)
                        print(dones[:, idx_env])
                        if idx_env < self.runner.nenv - 1:
                            print(dones[:, idx_env + 1])

                    self.assertTrue(cond)

                idx_res_env += 1
                continue

            start_idx = 0
            len_buffer = 0
            if use_buffer and not self.buffer is None:
                len_buffer = len(self.buffer[1][idx_env][0])

            for idx_done in idx_dones_env:
                if idx_done == 0:
                    continue
                if start_idx > 0:
                    len_buffer = 0

                len_traj = idx_done - start_idx

                for idx_a in range(num_agents):
                    cond = np.array_equal(rewards[idx_a][start_idx:start_idx + len_traj, idx_env, :],
                                            res_rewards[idx_a][len_buffer:len_traj + len_buffer, idx_res_env, :])

                    self.assertTrue(np.all(res_rewards[idx_a][len_traj + len_buffer:, idx_res_env] == 0))
                    mask = np.ones((self.nsteps,))
                    mask[len_traj + len_buffer:] = 0

                    cond2 = np.array_equal(mask, res_mask[:, idx_res_env])
                    cond3 = True
                    if start_idx == 0 and use_buffer and not self.buffer is None and len_buffer > 0:
                        cond3 = np.array_equal(self.buffer[1][idx_env][idx_a], res_rewards[idx_a][:len_buffer, idx_res_env, :])
                    if not cond or not cond2 or not cond3:
                        print(idx_dones_env)
                        if use_buffer and not self.buffer is None:
                            print(cond3)
                            print("len buffer : ", len_buffer)
                            print("buffer : ", self.buffer[1][idx_env][idx_a])
                            print("buffer res : ", res_rewards[idx_a][:len_buffer, idx_res_env, :])
                        print(mask)
                        print(res_mask[:, idx_res_env])
                        print(rewards[idx_a][start_idx:start_idx + len_traj, idx_env, :])
                        print(res_rewards[idx_a][:len_traj, idx_res_env, :])
                        print(idx_env, idx_res_env)
                        print(idx_done, len_traj)
                    self.assertTrue(cond and cond2 and cond3)


                start_idx = idx_done
                idx_res_env += 1

        self.assertTrue(cond_equal)

        self.buffer = copy.deepcopy(self.runner.env_data_buffer)
        for i in range(len(self.buffer)):
            for i2 in range(len(self.buffer[i])):
                for idx_a in range(num_agents):
                    self.buffer[i][i2][idx_a] = self.runner.env_data_buffer[i][i2][idx_a].copy()
        #print(self.buffer[1][0][0])

    def test_sanitize_minibatches_one_agent_buffer(self):

        for i in range(1000):
            #print("test {}".format(i))
            self.sanitize_minibatches(1, use_buffer=True)

    def test_sanitize_minibatches_one_agent_no_buffer(self):

        for i in range(1000):
            #print("test {}".format(i))
            self.sanitize_minibatches(1, use_buffer=False)

    def test_sanitize_minibatches_two_agent_no_buffer(self):

        for i in range(1000):
           # print("test {}".format(i))
            self.sanitize_minibatches(2, use_buffer=False)

    def test_sanitize_minibatches_two_agent_buffer(self):

        for i in range(1000):
           # print("test {}".format(i))
            self.sanitize_minibatches(2, use_buffer=True)

if __name__ == "__main__":

    unittest.main()
