import math

import heapq
import time

import numpy as np
from rendering import Geom
from physics_engine.loose_grid import Grid
from typing import Any, List, Tuple, Union
from dataclasses import dataclass, field

SPAWN_DEFENDER = 4
SPAWN_ATTACKER = 6
PATH_FREE = 2
PATH_BLOCKED = 1
PATH_OUTSIDE = 0


@dataclass(order=True)
class CellItem:
    distance: float
    cost: float = field(compare=False)
    x: int = field(compare=False)
    y: int = field(compare=False)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


def check_array_in(array: np.ndarray, list_array: List[Any], return_idx=False):
    for idx, elt in enumerate(list_array):
        if isinstance(elt, list):
            elt = elt[0]
        if all(array == elt):
            if return_idx:
                return True, idx
            return True

    if return_idx:
        return False, -1

    return False


class PathfinderPython:
    potential_dirs = [(1, 0, 1), (0, 1, 1), (-1, 0, 1), (0, -1, 1), (1, 1, math.sqrt(2)), (1, -1, math.sqrt(2)),
                      (-1, 1, math.sqrt(2)), (-1, -1, math.sqrt(2))]

    def __init__(self, static_grid: Grid, nodeGenerator: object, cell_width: int = 5, cell_height: int = 5):

        self.static_grid = static_grid
        self.nodeGenerator = nodeGenerator
        self.cell_width = cell_width
        self.cell_height = cell_height
        self.width = self.static_grid.get_width()
        self.height = self.static_grid.get_height()

        self.num_rows = self.height // self.cell_height
        self.num_cols = self.width // self.cell_width
        # this grid store 1 for obstacle, 2 for interior
        self.grid_pathfinding = np.zeros((self.num_rows, self.num_cols), dtype=np.uint8)

        self.accepted_grid_val = [PATH_FREE, SPAWN_ATTACKER, SPAWN_DEFENDER]

        self.cells: List[Geom] = []
        self.cells_row: List[List[Geom]] = []
        self.init_cells()
        self.cell_cost = np.ones((self.num_rows, self.num_cols), dtype=np.float32) * -1
        self.cell_distance = np.ones((self.num_rows, self.num_cols), dtype=np.float32) * -1
        self.checking_collision()

    def init_cells(self):
        self.cells = []
        self.cells_row = []
        cell_color = np.array([0, 0, 1, 0.2])
        scale = np.array([self.cell_width, self.cell_height]) / 2

        for start_height in range(0, self.height, self.cell_height):
            row = []
            for start_width in range(0, self.width, self.cell_width):
                center = np.array([start_width + self.cell_width / 2, start_height + self.cell_height / 2])
                row.append(Geom("rectangle", color=cell_color, translation=center, scale=scale))
            self.cells_row.append(row)

        self.cells = [c for crow in self.cells_row for c in crow]

    def to_cell_coord(self, x: int, y: int) -> Tuple[int, int]:
        idx_row = int(math.floor(y / self.cell_height))
        idx_col = int(math.floor(x / self.cell_width))

        return idx_col, idx_row

    def to_world_coord(self, col: int, row: int) -> Tuple[float, float]:
        x = (col + 0.5) * self.cell_width
        y = (row + 0.5) * self.cell_height

        return x, y

    def checking_collision(self):

        for idx_row in range(self.num_rows):
            for idx_col in range(self.num_cols):

                cell = self.cells_row[idx_row][idx_col]
                ids = list(self.static_grid.query_area(cell.translation[0], cell.translation[1], cell.scale[0] * 2,
                                                       2 * cell.scale[1], -1))

                colliding_spawn = False
                if len(ids) > 0:
                    if len(ids) == 1:
                        obj_id = ids[0] >> self.nodeGenerator.max_id_bin
                        node_id = ids[0] - (obj_id << self.nodeGenerator.max_id_bin)
                        obj = self.nodeGenerator.all_nodes[node_id].room.objects[obj_id]

                        if obj.is_spawn:
                            cell.set_color(np.array([1, 0, 1, 0]))
                            self.grid_pathfinding[idx_row, idx_col] = obj.spawn_type
                            colliding_spawn = True

                    if not colliding_spawn:
                        cell.set_color(np.array([0, 0, 1, 0.2]))
                        self.grid_pathfinding[idx_row, idx_col] = PATH_BLOCKED
                else:
                    c = self.nodeGenerator.np_random.uniform(size=4)
                    c[:] = 1
                    cell.set_color(c)
                    ids_node = self.nodeGenerator.grid.query_area(cell.translation[0], cell.translation[1],
                                                                  cell.scale[0] * 2, 2 * cell.scale[1],
                                                                  -1)
                    if len(ids_node) > 0:
                        cell.set_color(np.array([0, 1, 0, 0.2]))
                        self.grid_pathfinding[idx_row, idx_col] = PATH_FREE
                    else:
                        cell.set_color(np.array([1, 1, 1, 0.2]))
                        self.grid_pathfinding[idx_row, idx_col] = PATH_OUTSIDE

    def build_path(self, start: np.ndarray, goal: np.ndarray) -> List[np.ndarray]:
        path = []

        cur_item = goal
        while cur_item[0] != start[0] or cur_item[1] != start[1]:
            min_cost = self.cell_cost[cur_item[1], cur_item[0]]
            min_item = cur_item

            for dir in self.potential_dirs:
                v_x = cur_item[0] + dir[0]
                v_y = cur_item[1] + dir[1]

                if v_x < 0 or v_x >= self.num_cols or v_y < 0 or v_y >= self.num_rows:
                    continue
                v = np.array([v_x, v_y])
                if min_cost > self.cell_cost[v_y, v_x] >= 0:
                    min_cost = self.cell_cost[v_y, v_x]
                    min_item = v

            path.append(cur_item)
            cur_item = min_item

        path.append(start)
        return path

    def get_path_to(self, cell_start_x: int, cell_start_y: int, cell_goal_x: int, cell_goal_y: int,
                    force_start: bool = False) -> Union[
        bool, List[np.ndarray]]:
        if not (0 < cell_start_x < self.num_cols and 0 < cell_start_y < self.num_rows):
            return False
        if not (0 < cell_goal_x < self.num_cols and 0 < cell_goal_y < self.num_rows):
            return False

        if ((not force_start and self.grid_pathfinding[cell_start_y, cell_start_x] not in self.accepted_grid_val) or
                self.grid_pathfinding[cell_goal_y, cell_goal_x] not in self.accepted_grid_val):
            return False

        self.cell_cost[:, :] = -1
        self.cell_distance[:, :] = -1

        closed_cell: List[np.ndarray] = []
        open_cell: List[List[np.ndarray, float]] = []
        cell_start = np.array([cell_start_x, cell_start_y])

        self.cell_cost[cell_start_y, cell_start_x] = 0
        open_cell.append([cell_start, -1])
        w = 4
        tstart = time.perf_counter()
        iter = 1
        while len(open_cell) > 0:
            if iter % 100 == 0:
                tend = time.perf_counter()
                if (tend - tstart) > 0.25:
                    # print("time out")
                    return False

            u, _ = open_cell.pop(0)
            if u[0] == cell_goal_x and u[1] == cell_goal_y:
                return self.build_path(cell_start, u)
            for dir in self.potential_dirs:
                v_x = u[0] + dir[0]
                v_y = u[1] + dir[1]

                if v_x < 0 or v_x >= self.num_cols or v_y < 0 or v_y >= self.num_rows:
                    continue
                if self.grid_pathfinding[v_y, v_x] not in self.accepted_grid_val:
                    continue

                v = np.array([v_x, v_y])
                v_in_open_cell, idx_v_open_cell = check_array_in(v, open_cell, return_idx=True)
                if not check_array_in(v, closed_cell) and (not v_in_open_cell
                                                           or self.cell_cost[v_y, v_x] > self.cell_cost[u[1], u[0]] +
                                                           dir[2]):

                    self.cell_cost[v_y, v_x] = self.cell_cost[u[1], u[0]] + dir[2]
                    self.cell_distance[v_y, v_x] = self.cell_cost[v_y, v_x] + \
                                                   w * np.linalg.norm(np.array([v_x - cell_goal_x, v_y - cell_goal_y]))

                    if v_in_open_cell:
                        open_cell[idx_v_open_cell][1] = self.cell_distance[v_y, v_x]
                    else:
                        open_cell.append([v, self.cell_distance[v_y, v_x]])

            closed_cell.append(u)
            iter += 1
            open_cell.sort(key=lambda k: k[1])

        return False
