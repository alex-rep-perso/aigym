from tornado.gen import WaitIterator
from tornado.concurrent import future_add_done_callback, Future
import typing

# Add the capacity to insert future into the iterator when it is not finished
class WaitIteratorModifiable(WaitIterator):


    def add_future(self, f: Future, idx: int):
        if self.done():
            raise Exception("wait iterator is already done !")
        if f in self._unfinished:
            raise Exception("Future is already in the wait iterator !")
            
        self._unfinished[f] = idx
        future_add_done_callback(f, self._done_callback)