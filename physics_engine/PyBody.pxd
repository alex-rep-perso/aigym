# distutils: language = c++

from PhysicsObject cimport Body

cdef class PyBody:

    cdef Body c_body
    cdef readonly float convert_factor # conversion from meter to cm
    cdef readonly float inv_convert_factor # conversion from meter to cm

