# distutils: language = c++

cimport LooseGrid

cdef class Grid:

    cdef LooseGrid.LGrid* grid
    cdef readonly int width
    cdef readonly int height
    cdef readonly float cell_size_w, cell_size_h

    def __cinit__(self, float width, float height, int num_rows=25, int num_cols=25):

        self.cell_size_w = width / <float>num_cols
        self.cell_size_h = height / <float>num_rows

        self.width = <int>width
        self.height = <int>height

        self.grid = LooseGrid.lgrid_create(self.cell_size_w, self.cell_size_h, 0, height, width, 0)

    def __dealloc__(self):
        LooseGrid.lgrid_destroy(self.grid)

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def reset(self):
        LooseGrid.lgrid_reset(self.grid)

    cdef (float, float, float, float) _convert_rect(self, (float, float, float, float) rect):

        cdef float mx, my, hx, hy
        mx = 0.5 * (rect[2] + rect[0])
        my = 0.5 * (rect[3] + rect[1])

        hx = rect[2] - mx
        hy = rect[3] - my

        return mx, my, hx, hy

    def add_object(self, float mx, float my, float hx, float hy, int id):

        LooseGrid.lgrid_insert(self.grid, id, mx, my, hx, hy)


    def remove(self, float mx, float my, int id):

        LooseGrid.lgrid_remove(self.grid, id, mx, my)

    def move(self, float prev_mx, float prev_my,
                    float mx, float my, int id):
        LooseGrid.lgrid_move(self.grid, id, prev_mx, prev_my, mx, my)


    def query_area(self, float mx, float my, float hx, float hy, int omit_id):

        return set(LooseGrid.lgrid_query(self.grid, mx, my, hx, hy, omit_id))


    def inside_grid_bounds(self,  float mx, float my, float hx, float hy):
        return LooseGrid.lgrid_in_bounds(self.grid, mx, my, hx, hy)

    def optimize(self):

        LooseGrid.lgrid_optimize(self.grid)

