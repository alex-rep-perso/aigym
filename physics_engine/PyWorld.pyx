# distutils: language = c++

from libcpp.vector cimport vector
from libcpp.map cimport map
from World cimport World, Body
from physics_engine.PyBody cimport PyBody
from libc.stdint cimport uintptr_t
import numpy as np
from cython.operator import dereference, postincrement

cdef class PyWorld:

    cdef World* c_world

    def __cinit__(self, float gravity_x, float gravity_y, int iterations, int num_cols, int num_rows, int width, int height):
        self.c_world = new World(gravity_x, gravity_y, iterations, num_cols, num_rows, width, height)

    def __dealloc__(self):
        del self.c_world

    def add_proxy(self, PyBody body):
        self.c_world.AddBody(&body.c_body)

    def clear(self):
        self.c_world.Clear()

    def get_all_contact_points(self):
        contact_points = []
        vec_contact_points = self.c_world.GetContactPoints()
        for v in vec_contact_points:
            contact_points.append(np.array([v.x, v.y]))

        return contact_points

    def body_lidar(self, angle_interval: float, body_id: int, log: bool = False):
        vec_distances = self.c_world.BodyLidar(angle_interval, body_id, log)

        return vec_distances

    def query_object_radius(self, body_id: int, radius: float):
        cdef vector[float] distances
        results = self.c_world.QueryObjectRadius(body_id, radius, distances)

        return results, distances

    def get_bodies(self):
        cdef vector[int] ids
        cdef map[int, Body*].iterator it = self.c_world.bodies.begin()

        while it != self.c_world.bodies.end():
            ids.push_back(dereference(it).first)
            postincrement(it)

        return ids

    def step(self, float dt):
        self.c_world.Step(dt)

    def set_gravity(self, float x, float y):
        self.c_world.gravity.x = x
        self.c_world.gravity.y = y

    def get_gravity(self) -> (float, float):
        return self.c_world.gravity.x, self.c_world.gravity.y