/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* Permission to use, copy, modify, distribute and sell this software
* and its documentation for any purpose is hereby granted without fee,
* provided that the above copyright notice appear in all copies.
* Erin Catto makes no representations about the suitability 
* of this software for any purpose.  
* It is provided "as is" without express or implied warranty.
*/

#include "Body.h"

Body::Body()
{
	Reset();
	friction = 0.3f;

	width.Set(1.0f, 1.0f);
	radius = 1.0f;
	bbox.Set(1.45f / 2.f, 1.45f / 2.f);
	mass = FLT_MAX;
	invMass = 0.0f;
	I = FLT_MAX;
	invI = 0.0f;
	type = BodyType::CIRCLE;
	id = -1;
}

Body::Body(int id_)
    : Body()
   {
       id = id_;
    }

void Body::Reset()
{
    position.Set(0.0f, 0.0f);
	rotation = 0.0f;
	velocity.Set(0.0f, 0.0f);
	angularVelocity = 0.0f;
	force.Set(0.0f, 0.0f);
	torque = 0.0f;
}

void Body::Set(const Vec2<float>& w, float m)
{
	Reset();
	friction = 0.3f;

	width = w;

	const float max_width = w.x > w.y ? w.x : w.y;
	bbox.Set(max_width * 1.45f / 2.f, max_width * 1.45f / 2.f);

	mass = m;

	if (mass < FLT_MAX && mass > 0)
	{
		invMass = 1.0f / mass;
		I = mass * (width.x * width.x + width.y * width.y) / 12.0f;
		invI = 1.0f / I;
	}
	else
	{
		invMass = 0.0f;
		I = FLT_MAX;
		invI = 0.0f;
	}
}

void Body::Set(float w, float h, float m)
{
    Vec2<float> v(w, h);
    Set(v, m);
}

void Body::SetWidth(float w, float h)
{
    width = Vec2<float>(w, h);

    const float max_width = width.x > width.y ? width.x : width.y;
	bbox.Set(max_width * 1.45f / 2.f, max_width * 1.45f / 2.f);
}

void Body::SetRadius(float r)
{
    radius = r;
    bbox.Set(r, r);
}

void Body::SetType(BodyType t)
{
    type = t;
}

void Body::SetPosition(float x, float y)
{
    position.Set(x, y);
}

void Body::SetVelocity(float x, float y)
{
    velocity.Set(x, y);
}

void Body::SetRotation(float r)
{
    rotation = r;
}