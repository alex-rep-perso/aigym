#pragma once

#include "MathUtils.h"
#include <vector>
#include <map>
#include <functional>

class Pathfinder
{
    public:

        Pathfinder(int cell_width, int cell_height, int width, int height);
        ~Pathfinder();

        std::vector<Vec2<int>> getPathTo(int cell_start_x, int cell_start_y, int cell_goal_x,
                                    int cell_goal_y, bool force_start = false);

        void setPathfindingGrid(std::vector<int> grid_pathfinding);
        enum CellType
        {
            PATH_OUTSIDE = 0,
            PATH_BLOCKED = 1,
            PATH_FREE = 2,
            SPAWN_DEFENDER = 4,
            SPAWN_ATTACKER = 6
        };

        using Cell = std::pair<int, float>;

    private:

        std::vector<Vec2<int>> buildPath(const Vec2<int>& start, const Vec2<int>& goal);
        bool checkCellIn(const std::vector<Cell>& heap, int key, int& idx);
        bool checkInAccepted(int cell_val);

        int width_;
        int height_;
        int cell_width_;
        int cell_height_;
        int num_rows_;
        int num_cols_;
        float* cell_cost_;
        float* cell_distance_;
        int* grid_pathfinding_;
        std::function<bool(const Cell&, const Cell&)> comparator_;
        std::vector<CellType> accepted_type_;
        std::vector<std::tuple<int, int, float>> potential_dirs_;


};