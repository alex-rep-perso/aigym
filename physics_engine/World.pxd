# distutils: language = c++

from libcpp.vector cimport vector
from libcpp.map cimport map

#sources
cdef extern from "Collide.cpp":
    pass

cdef extern from "Arbiter.cpp":
    pass

cdef extern from "LGrid.cpp":
    pass

cdef extern from "Joint.cpp":
    pass

cdef extern from "Body.cpp":
    pass

cdef extern from "World.cpp":
    pass

cdef extern from "Body.h":
    cdef cppclass Body:
        pass

cdef extern from "Joint.h":
    cdef cppclass Joint:
        pass

cdef extern from "MathUtils.h":
    cdef cppclass Vec2[T]:
        Vec2() except +
        Vec2(T x, T y) except +
        void Set(T x_, T y_)
        float Length()
        T x, y

cdef extern from "World.h":
    cdef cppclass World:
        World(float gravity_x, float gravity_y, int iterations, int num_cols, int num_rows, int width_, int height_) except +
        void AddBody(Body* b)
        void AddJoint(Joint* j)
        void Clear()
        void Step(float dt)
        vector[Vec2[float]] GetContactPoints()
        vector[float] BodyLidar(float angle_interval, int body_id, int log)
        vector[int] QueryObjectRadius(int body_id, float radius, vector[float]& distances)
        Vec2[float] gravity
        map[int, Body*] bodies