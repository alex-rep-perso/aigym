/*
* Copyright (c) 2006-2009 Erin Catto http://www.gphysics.com
*
* Permission to use, copy, modify, distribute and sell this software
* and its documentation for any purpose is hereby granted without fee,
* provided that the above copyright notice appear in all copies.
* Erin Catto makes no representations about the suitability 
* of this software for any purpose.  
* It is provided "as is" without express or implied warranty.
*/

#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <map>
#include "MathUtils.h"
#include "Arbiter.h"
#include "LGrid.hpp"
#include "Body.h"
#include "Joint.h"

struct World
{
	World(float gravity_x, float gravity_y, int iterations, int num_cols, int num_rows, int width_, int height_,
	      float ray_step = 25.f, float ray_bbox_x = 30.f, float ray_bbox_y = 30.f)
	    : iterations(iterations), width(width_), height(height_), ray_search_step(ray_step), max_ray_step(700)
	{
	    gravity = Vec2<float>(gravity_x, gravity_y);

	    const float cell_size_w = width / (float)num_cols;
        const float cell_size_h = height / (float)num_rows;

        grid = lgrid_create(cell_size_w, cell_size_h, 0, height, width, 0);
        ray_search_bbox = Vec2<float>(ray_bbox_x, ray_bbox_y);

	}

	~World()
	{
	    lgrid_destroy(grid);
	    grid = NULL;
	}

    // return T factor such that start + T * dir is the closest intersection with body_id. T is negative if no intersection is possible
    float RayInterBody(const Vec2<float>& start, const Vec2<float>& dir, const std::set<int>& bodies_id, bool log = false);
    // Return a positive distance to the closest body intersected with ray or negative value if none is intersecting
    float RayInter(const Vec2<float>& dir, Body* start_body, bool log = false);
	// Return distance using RayInter from each ray around body_id at angle_interval, angle_interval is in degrees not radian
	std::vector<float> BodyLidar(float angle_interval, int body_id, int log = 0);
	// Get all objects next to body_id Body within r radius. Distances to object are also returned using provided vector
	std::vector<int> QueryObjectRadius(int body_id, float radius, std::vector<float>& distances);

	void AddBody(Body* body);
	void AddJoint(Joint* joint);
	void Clear();

	void Step(float dt);

	void BroadPhase();

	std::vector<Vec2<float>> GetContactPoints();

	std::map<int, Body*> bodies;
	std::vector<Joint*> joints;
	std::map<ArbiterKey, Arbiter> arbiters;
	Vec2<float> gravity;
	int iterations;
	static bool accumulateImpulses;
	static bool warmStarting;
	static bool positionCorrection;
	LGrid* grid;

	float width;
	float height;

	float ray_search_step;
	unsigned int max_ray_step;
	Vec2<float> ray_search_bbox;
	std::map<float, std::vector<Vec2<float>>> ray_angle_cache;
};

#endif
