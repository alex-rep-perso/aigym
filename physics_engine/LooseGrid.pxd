# distutils: language = c++

from libcpp.vector cimport vector

cdef extern from "LGrid.cpp":
    pass

cdef extern from "LGrid.hpp":
    cdef cppclass LGrid:
        pass

    LGrid* lgrid_create(float cell_w, float cell_h, float l, float t, float r, float b)

    void lgrid_destroy(LGrid* grid)

    void lgrid_insert(LGrid* grid, int id, float mx, float my, float hx, float hy)

    void lgrid_remove(LGrid* grid, int id, float mx, float my)

    void lgrid_move(LGrid* grid, int id, float prev_mx, float prev_my, float mx, float my) except +

    vector[int] lgrid_query(const LGrid* grid, float mx, float my, float hx, float hy, int omit_id)

    int lgrid_in_bounds(const LGrid* grid, float mx, float my, float hx, float hy)

    void lgrid_optimize(LGrid* grid)

    void lgrid_reset(LGrid* grid)
