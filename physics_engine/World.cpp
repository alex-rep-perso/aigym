/*
* Copyright (c) 2006-2009 Erin Catto http://www.gphysics.com
*
* Permission to use, copy, modify, distribute and sell this software
* and its documentation for any purpose is hereby granted without fee,
* provided that the above copyright notice appear in all copies.
* Erin Catto makes no representations about the suitability 
* of this software for any purpose.  
* It is provided "as is" without express or implied warranty.
*/

#include "World.h"
#include <iostream>
#include <cstdint>
#include <algorithm>
#include <cmath>
#include <set>
#include <chrono>

using std::vector;
using std::map;
using std::pair;
using namespace std::chrono;

typedef map<ArbiterKey, Arbiter>::iterator ArbIter;
typedef pair<ArbiterKey, Arbiter> ArbPair;

bool World::accumulateImpulses = true;
bool World::warmStarting = true;
bool World::positionCorrection = true;
const double g_pi = std::acos(-1);

void World::AddBody(Body* body)
{
    lgrid_insert(grid, body->id, body->position.x, body->position.y, body->bbox.x, body->bbox.y);
    bodies[body->id] = body;
}

void World::AddJoint(Joint* joint)
{
	joints.push_back(joint);
}

void World::Clear()
{
    lgrid_reset(grid);
	bodies.clear();
	joints.clear();
	arbiters.clear();
}

float World::RayInterBody(const Vec2<float>& start, const Vec2<float>& dir, const std::set<int>& bodies_id, bool log)
{
    float t = -1;
    std::map<int, float> t_try_vec;
    for (int body_id : bodies_id)
    {
        Body* b = bodies[body_id];

        float t_try = RayIntersection(start, dir, b);
        t_try_vec.emplace(body_id, t_try);
        if (t_try > 0 && (t < 0 || t_try < t))
            t = t_try;
    }

    if (t > 0 && log)
    {
         for (int body_id : bodies_id)
        {
            Body* b = bodies[body_id];
            std::cout << " id " << b->id << " body " << b->position.x << ", " << b->position.y
                      << " body width : " << b->width.x << " , " << b->width.y << " body rotation : "
                      << (180 * b->rotation / g_pi)
                    << " t_try " << t_try_vec[body_id] << " t " << t << std::endl;
        }
    }

    return t;
}

float World::RayInter(const Vec2<float>& dir, Body* start_body, bool log)
{
    float t_search = 0;
    float t = -1;
    unsigned int check_steps_ahead = 0;

    for(unsigned int step = 1; step <= max_ray_step; ++step)
    {
        t_search = step * ray_search_step;

        const Vec2<float> cur_pos = start_body->position + t_search * dir;
        const std::vector<int> ids_colliding_bbox = lgrid_query(grid, cur_pos.x, cur_pos.y, ray_search_bbox.x,
                                                            ray_search_bbox.y, start_body->id);
        std::set<int> ids_colliding;
        for (int id : ids_colliding_bbox)
            ids_colliding.insert(id);

        if (ids_colliding.size() > 0)
        {
            float t_try = RayInterBody(start_body->position, dir, ids_colliding, log);
            if (t_try > 0 || t > 0)
            {
                if (t_try > 0 && (t < 0 || t_try < t))
                    t = t_try;

                if (log)
                {
                std::cout << "steps ahead " << check_steps_ahead << " step " << step << " t_search: " << t_search << " cur_pos " << cur_pos.x << ", " << cur_pos.y
                   << " ids: " << ids_colliding.size()  << " t found : " << t_try  << " t " << t << std::endl;
                 }
                if (check_steps_ahead > 0 && step == check_steps_ahead + 4)
                    return t;
                else if (check_steps_ahead == 0)
                    check_steps_ahead = step;
            }
        }
    }

    return -1;
}

vector<float>
World::BodyLidar(float angle_interval, int body_id, int log)
{
//    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    vector<float> result;
    if (angle_interval <= 0 || angle_interval >= 360)
        return result;

    Body* b = bodies[body_id];
    auto angle_found = ray_angle_cache.find(angle_interval);
    if (angle_found == ray_angle_cache.end())
    {
        std::vector<Vec2<float>> vector_angle = {};
        ray_angle_cache.emplace(angle_interval, vector_angle);
    }

    int idx = 0;
    for (float angle = 0; angle < 360; angle += angle_interval)
    {
        if (angle_found != ray_angle_cache.end())
        {
            result.push_back(this->RayInter(angle_found->second[idx], b, log));
        }
        else
        {
            float rad_angle = g_pi / 180 * angle;
            Vec2<float> dir(std::cos(rad_angle), std::sin(rad_angle));
            ray_angle_cache[angle_interval].push_back(dir);

            result.push_back(this->RayInter(dir, b, log));
        }
        idx += 1;
    }

//    high_resolution_clock::time_point t2 = high_resolution_clock::now();
//    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
//    std::cout << "took " << time_span.count() << " seconds" << std::endl;

    return result;
}

std::vector<int>
World::QueryObjectRadius(int body_id, float radius, std::vector<float>& distances)
{
    std::vector<int> result;
    auto find_body = bodies.find(body_id);
    if (find_body == bodies.end())
    {
        std::cerr << "body_id " << body_id << " is not in World" << std::endl;
        throw std::out_of_range("body_id not in World");
    }

    Body* b = bodies[body_id];


    std::vector<int> ids_colliding = lgrid_query(grid, b->position.x, b->position.y, radius, radius, b->id);
    for (auto id : ids_colliding)
    {
        Body* test = bodies[id];
        float dist = (b->position - test->position).Length();
        if (dist <= radius)
        {
            result.push_back(id);
            distances.push_back(dist);
        }
    }

    return result;
}

void World::BroadPhase()
{
	// O(n^2) broad-phase
	for (auto iter : bodies)
	{
		Body* bi = iter.second;

		std::vector<int> ids_colliding = lgrid_query(grid, bi->position.x, bi->position.y, bi->bbox.x, bi->bbox.y, bi->id);
		for (int id : ids_colliding)
		{
			Body* bj = bodies[id];

			if (bi->invMass == 0.0f && bj->invMass == 0.0f)
				continue;

			Arbiter newArb(bi, bj);
			ArbiterKey key(bi, bj);

			if (newArb.numContacts > 0)
			{
				ArbIter iter = arbiters.find(key);
				if (iter == arbiters.end())
				{
					arbiters.insert(ArbPair(key, newArb));
				}
				else
				{
					iter->second.Update(newArb.contacts, newArb.numContacts);
				}
			}
			else
			{
				arbiters.erase(key);
			}
		}
	}
}

std::vector<Vec2<float>> World::GetContactPoints()
{
    std::vector<Vec2<float>> contact_points;
    std::map<ArbiterKey, Arbiter>::const_iterator iter;
	for (iter = arbiters.begin(); iter != arbiters.end(); ++iter)
	{
		const Arbiter& arbiter = iter->second;
		for (int i = 0; i < arbiter.numContacts; ++i)
		{
				contact_points.push_back(arbiter.contacts[i].position);
		}
	}

    return contact_points;
}

void World::Step(float dt)
{
	float inv_dt = dt > 0.0f ? 1.0f / dt : 0.0f;

    for (auto i : bodies)
    {
        i.second->colliding_physics_ids.clear();
    }

	// Determine overlapping bodies and update contact points.
	BroadPhase();

	// Integrate forces.
	for (auto i : bodies)
	{
		Body* b = i.second;

		if (b->invMass == 0.0f)
			continue;

		b->velocity += dt * (gravity + b->invMass * b->force);
		b->angularVelocity += dt * b->invI * b->torque;
	}

	// Perform pre-steps.
	for (ArbIter arb = arbiters.begin(); arb != arbiters.end(); ++arb)
	{
		arb->second.PreStep(inv_dt);
	}

	for (int i = 0; i < (int)joints.size(); ++i)
	{
		joints[i]->PreStep(inv_dt);	
	}

	// Perform iterations
	for (int i = 0; i < iterations; ++i)
	{
		for (ArbIter arb = arbiters.begin(); arb != arbiters.end(); ++arb)
		{
			arb->second.ApplyImpulse();
		}

		for (int j = 0; j < (int)joints.size(); ++j)
		{
			joints[j]->ApplyImpulse();
		}
	}

	// Integrate Velocities
	for (auto i : bodies)
	{
		Body* b = i.second;

		if (b->invMass == 0)
		    continue;

        const Vec2<float> prev_pos = b->position;
		b->position += dt * b->velocity;
		b->rotation += dt * b->angularVelocity;

        //std::cout << "moving body " << b << " with data " << prev_pos.x << ", " << prev_pos.y
        //            << " and " << b->position.x << ", "  << b->position.y << std::endl;
        lgrid_move(grid, b->id, prev_pos.x, prev_pos.y, b->position.x, b->position.y);

		b->force.Set(0.0f, 0.0f);
		b->torque = 0.0f;
	}
}
