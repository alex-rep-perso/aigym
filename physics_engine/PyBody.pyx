# distutils: language = c++

from PhysicsObject cimport Body, Vec2, BodyType
from libc.stdint cimport uintptr_t
import numpy as np

cdef class PyBody:

    def __cinit__(self, int id, float w, float h, float m):
        self.convert_factor = 1
        self.inv_convert_factor = 1
        self.c_body = Body(id)
        self.c_body.Set(w * self.convert_factor, h * self.convert_factor, m)

    def set_width(self, float w, float h):
        self.c_body.SetWidth(w, h)

    def get_speed(self):
        return np.array([self.c_body.velocity.x, self.c_body.velocity.y])

    def get_pos(self):
        return np.array([self.c_body.position.x, self.c_body.position.y])

    def get_rotation(self) -> float :
        return self.c_body.rotation

    def get_data(self):
        return np.array([self.c_body.velocity.x, self.c_body.velocity.y, self.c_body.position.x, self.c_body.position.y, self.c_body.rotation])

    def get_mass(self) -> float :
        return self.c_body.mass

    def set_speed(self, float x, float y):
        self.c_body.SetVelocity(x, y)

    def set_radius(self, float r):
        self.c_body.SetRadius(r)

    def reset(self):
        self.c_body.Reset()

    def set_type(self, body_type: str):
        assert body_type in ["circle", "rectangle"]

        if body_type == "circle":
            self.c_body.SetType(BodyType.CIRCLE)
        else:
            self.c_body.SetType(BodyType.RECTANGLE)

    def add_speed(self, float x, float y):
        cdef Vec2[float] speed = Vec2[float](x, y)
        self.c_body.velocity += speed

    def set_pos(self, float x, float y):
        self.c_body.SetPosition(x, y)

    def set_rotation(self, float r):
        self.c_body.SetRotation(r)

    def get_colliding_ids(self):
        return self.c_body.colliding_physics_ids

    def get_physics_id(self):
        return self.c_body.id

    def print_body(self):
        print("Body values : ref {} - width {} - {} mass {}".format(<uintptr_t>(&self.c_body), self.c_body.width.x, self.c_body.width.y, self.c_body.mass))