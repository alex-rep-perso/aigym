/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* Permission to use, copy, modify, distribute and sell this software
* and its documentation for any purpose is hereby granted without fee,
* provided that the above copyright notice appear in all copies.
* Erin Catto makes no representations about the suitability 
* of this software for any purpose.  
* It is provided "as is" without express or implied warranty.
*/

#ifndef BODY_H
#define BODY_H

#include "MathUtils.h"
#include <vector>
#include <cstdint>


enum class BodyType
{
    RECTANGLE = 0,
    CIRCLE = 1
};


struct Body
{
	Body(int id_);
	Body();
	void Set(const Vec2<float>& w, float m);
	void Set(float w, float h, float m);
	void SetWidth(float w, float h);
	void SetRadius(float r);
	void SetType(BodyType t);
	void SetPosition(float x, float y);
	void SetVelocity(float x, float y);
	void SetRotation(float r);
	void Reset();

	void AddForce(const Vec2<float>& f)
	{
		force += f;
	}

	Vec2<float> position;
	float rotation;

	Vec2<float> velocity;
	float angularVelocity;

	Vec2<float> force;
	float torque;

	Vec2<float> width;
	Vec2<float> bbox;
	float radius;

	float friction;
	float mass, invMass;
	float I, invI;

	int id;

	std::vector<int> colliding_physics_ids;

	BodyType type;
};

#endif
