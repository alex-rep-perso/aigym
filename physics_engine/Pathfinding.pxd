# distutils: language = c++

from libcpp.vector cimport vector

#sources
cdef extern from "Pathfinding.cpp":
    pass

cdef extern from "MathUtils.h":
    cdef cppclass Vec2[T]:
        Vec2() except +
        Vec2(T x, T y) except +
        void Set(T x_, T y_)
        float Length()
        T x, y

cdef extern from "Pathfinding.h":
    cdef cppclass Pathfinder:
        Pathfinder(int cell_width, int cell_height, int width, int height) except +
        vector[Vec2[int]] getPathTo(int cell_start_x, int cell_start_y, int cell_goal_x, int cell_goal_y, int force_start)
        void setPathfindingGrid(vector[int] grid_pathfinding)