#include "Pathfinding.h"
#include "heap.hxx"
#include <cmath>
#include <vector>
#include <deque>
#include <iostream>

float lengthVec(int x_a, int y_a, int x_b, int y_b)
{
    float diff_x = static_cast<float>(x_a - x_b);
    float diff_y = static_cast<float>(y_a - y_b);
    return std::sqrt(diff_x * diff_x + diff_y * diff_y);
}

Pathfinder::Pathfinder(int cell_width, int cell_height, int width, int height)
    : width_(width), height_(height), cell_width_(cell_width), cell_height_(cell_height)
{
    num_cols_ = static_cast<int>(std::floor(width_ / cell_width_));
    num_rows_ = static_cast<int>(std::floor(height_ / cell_height_));

    cell_cost_ = new float[num_rows_ * num_cols_];
    cell_distance_ = new float[num_rows_ * num_cols_];
    grid_pathfinding_ = new int[num_rows_ * num_cols_];

    for (int row = 0; row < num_rows_; row++)
    {
        for (int col = 0; col < num_cols_; col++)
        {
            cell_cost_[num_cols_ * row + col] = -1;
            cell_distance_[num_cols_ * row + col] = -1;
            grid_pathfinding_[num_cols_ * row + col] = Pathfinder::PATH_OUTSIDE;
        }
    }

    comparator_ = [](const Cell& a, const Cell& b) -> bool
                    {
                        return a.second > b.second;
                    };

    accepted_type_ = {Pathfinder::PATH_FREE, Pathfinder::SPAWN_ATTACKER, Pathfinder::SPAWN_DEFENDER};
    potential_dirs_ = {std::make_tuple(1, 0, 1.f), std::make_tuple(0, 1, 1.f), std::make_tuple(-1, 0, 1.f),
                        std::make_tuple(0, -1, 1.f), std::make_tuple(1, 1, std::sqrt(2)), std::make_tuple(1, -1, std::sqrt(2)),
                      std::make_tuple(-1, 1, std::sqrt(2)), std::make_tuple(-1, -1, std::sqrt(2))};
}

Pathfinder::~Pathfinder()
{
    delete[] cell_cost_;
    delete[] cell_distance_;
    delete[] grid_pathfinding_;
}

bool Pathfinder::checkCellIn(const std::vector<Cell>& heap, int key, int& idx)
{
    for (std::size_t idx_heap = 0; idx_heap < heap.size(); ++idx_heap)
    {
        if (heap[idx_heap].first == key)
        {
            idx = idx_heap;
            return true;
        }
    }

    return false;
}

bool Pathfinder::checkInAccepted(int cell_val)
{
    return std::any_of(accepted_type_.begin(), accepted_type_.end(), [cell_val](const CellType& type)
                            { return cell_val == type;});
}

void Pathfinder::setPathfindingGrid(std::vector<int> grid_pathfinding)
{
    assert(grid_pathfinding.size() == num_rows_ * num_cols_);
    for (std::size_t idx = 0; idx < grid_pathfinding.size(); ++idx)
        grid_pathfinding_[idx] = grid_pathfinding[idx];
}

std::vector<Vec2<int>>
Pathfinder::buildPath(const Vec2<int>& start, const Vec2<int>& goal)
{
    std::vector<Vec2<int>> path;

    Vec2<int> cur_pos = goal;
    while (cur_pos.x != start.x || cur_pos.y != start.y)
    {
        float min_cost = cell_cost_[num_cols_ * cur_pos.y + cur_pos.x];
        Vec2<int> min_pos = cur_pos;

        for (const auto& dir : potential_dirs_)
        {
            int v_x = cur_pos.x + std::get<0>(dir);
            int v_y = cur_pos.y + std::get<1>(dir);

            if (v_x < 0 || v_x >= num_cols_ || v_y < 0 || v_y >= num_rows_)
                continue;

            int v_key = num_cols_ * v_y + v_x;
            if (min_cost > cell_cost_[v_key] && cell_cost_[v_key] >=  0)
            {
                min_cost = cell_cost_[v_key];
                min_pos = Vec2<int>(v_x, v_y);
            }
        }
        path.push_back(cur_pos);
        cur_pos = min_pos;
    }

    path.push_back(start);
    return path;
}

std::vector<Vec2<int>>
Pathfinder::getPathTo(int cell_start_x, int cell_start_y, int cell_goal_x,
                                    int cell_goal_y, bool force_start)
{
    if (cell_start_x < 0 || cell_start_x >= num_cols_ || cell_start_y < 0 || cell_start_y >= num_rows_
        || cell_goal_x < 0 || cell_goal_x >= num_cols_ || cell_goal_y < 0 || cell_goal_y >= num_rows_)
        return {};

    int start_key = num_cols_ * cell_start_y + cell_start_x;
    if ((!force_start && !checkInAccepted(grid_pathfinding_[start_key]))
        || !checkInAccepted(grid_pathfinding_[num_cols_ * cell_goal_y + cell_goal_x]))
        return {};

    for (int row = 0; row < num_rows_; row++)
    {
        for (int col = 0; col < num_cols_; col++)
        {
            cell_cost_[num_cols_ * row + col] = -1;
            cell_distance_[num_cols_ * row + col] = -1;
        }
    }

    std::deque<int> closed_cells;
    std::vector<Cell> open_cells;

    cell_cost_[start_key] = 0;
    Cell cell_start = std::make_pair(start_key, -1);
    heapq_insert(open_cells, cell_start, comparator_);
    float w = 4;
    while (open_cells.size() > 0)
    {
        Cell u = heapq_pop(open_cells, comparator_);
        int u_x = u.first % num_cols_;
        int u_y = static_cast<int>(std::floor(u.first / num_cols_));

        if (u_x == cell_goal_x && u_y == cell_goal_y)
        {

           Vec2<int> start(cell_start_x, cell_start_y);
           Vec2<int> goal(cell_goal_x, cell_goal_y);
           return buildPath(start, goal);
        }

        for (const auto& dir : potential_dirs_)
        {
            int v_x = u_x + std::get<0>(dir);
            int v_y = u_y + std::get<1>(dir);
            int v_key = num_cols_ * v_y + v_x;

            if (v_x < 0 || v_x >= num_cols_ || v_y < 0 || v_y >= num_rows_
                || !checkInAccepted(grid_pathfinding_[v_key]))
                continue;

            int v_in_open_cells_idx = -1;
            bool v_in_open_cells = checkCellIn(open_cells, v_key, v_in_open_cells_idx);
            bool v_in_closed_cells = std::any_of(closed_cells.begin(), closed_cells.end(), [&v_key](const int& key)
                                                    { return key == v_key;  });

            if (!v_in_closed_cells && (!v_in_open_cells || cell_cost_[v_key] > cell_cost_[u.first] + std::get<2>(dir)))
            {
                cell_cost_[v_key] = cell_cost_[u.first] + std::get<2>(dir);
                cell_distance_[v_key] = cell_cost_[v_key] + w * lengthVec(v_x, v_y, cell_goal_x, cell_goal_y);

                Cell v = std::make_pair(v_key, cell_distance_[v_key]);
                heapq_insert(open_cells, v, comparator_);
            }
        }

        closed_cells.push_front(u.first);
    }

    return {};
}