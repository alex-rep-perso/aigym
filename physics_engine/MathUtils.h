/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* Permission to use, copy, modify, distribute and sell this software
* and its documentation for any purpose is hereby granted without fee,
* provided that the above copyright notice appear in all copies.
* Erin Catto makes no representations about the suitability 
* of this software for any purpose.  
* It is provided "as is" without express or implied warranty.
*/

#ifndef MATHUTILS_H
#define MATHUTILS_H

#include <math.h>
#include <float.h>
#include <assert.h>
#include <stdlib.h>

const float k_pi = 3.14159265358979323846264f;

template <typename T>
struct Vec2
{
	Vec2() {}
	Vec2(T x, T y) : x(x), y(y) {}

	void Set(T x_, T y_) { x = x_; y = y_; }

	Vec2<T> operator -() { return Vec2<T>(-x, -y); }
	
	void operator += (const Vec2<T>& v)
	{
		x += v.x; y += v.y;
	}
	
	void operator -= (const Vec2<T>& v)
	{
		x -= v.x; y -= v.y;
	}

	void operator *= (float a)
	{
		x *= a; y *= a;
	}

	float Length() const
	{
		return sqrtf(static_cast<float>(x * x + y * y));
	}

	T x, y;
};

struct Mat22
{
	Mat22() {}
	Mat22(float angle)
	{
		float c = cosf(angle), s = sinf(angle);
		col1.x = c; col2.x = -s;
		col1.y = s; col2.y = c;
	}

	Mat22(const Vec2<float>& col1, const Vec2<float>& col2) : col1(col1), col2(col2) {}

	Mat22 Transpose() const
	{
		return Mat22(Vec2<float>(col1.x, col2.x), Vec2<float>(col1.y, col2.y));
	}

	Mat22 Invert() const
	{
		float a = col1.x, b = col2.x, c = col1.y, d = col2.y;
		Mat22 B;
		float det = a * d - b * c;
		assert(det != 0.0f);
		det = 1.0f / det;
		B.col1.x =  det * d;	B.col2.x = -det * b;
		B.col1.y = -det * c;	B.col2.y =  det * a;
		return B;
	}

	Vec2<float> col1, col2;
};

template <typename T>
inline T Dot(const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x * b.x + a.y * b.y;
}

template <typename T>
inline T Cross(const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x * b.y - a.y * b.x;
}

template <typename T>
inline Vec2<T> Cross(const Vec2<T>& a, T s)
{
	return Vec2<T>(s * a.y, -s * a.x);
}

template <typename T>
inline Vec2<T> Cross(T s, const Vec2<T>& a)
{
	return Vec2<T>(-s * a.y, s * a.x);
}


inline Vec2<float> operator * (const Mat22& A, const Vec2<float>& v)
{
	return Vec2<float>(A.col1.x * v.x + A.col2.x * v.y, A.col1.y * v.x + A.col2.y * v.y);
}

template <typename T>
inline Vec2<T> operator * (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x * b.x, a.y * b.y);
}

template <typename T>
inline Vec2<T> operator + (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x + b.x, a.y + b.y);
}

template <typename T>
inline Vec2<T> operator - (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x - b.x, a.y - b.y);
}

template <typename T>
inline bool operator == (const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x == b.x && a.y == b.y;
}

template <typename T>
inline bool operator != (const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x != b.x || a.y != b.y;
}

template <typename T>
inline Vec2<T> operator * (T s, const Vec2<T>& v)
{
	return Vec2<T>(s * v.x, s * v.y);
}

inline Mat22 operator + (const Mat22& A, const Mat22& B)
{
	return Mat22(A.col1 + B.col1, A.col2 + B.col2);
}

inline Mat22 operator * (const Mat22& A, const Mat22& B)
{
	return Mat22(A * B.col1, A * B.col2);
}

inline float Abs(float a)
{
	return a > 0.0f ? a : -a;
}

inline Vec2<float> Abs(const Vec2<float>& a)
{
	return Vec2<float>(fabsf(a.x), fabsf(a.y));
}

inline Mat22 Abs(const Mat22& A)
{
	return Mat22(Abs(A.col1), Abs(A.col2));
}

inline float Sign(float x)
{
	return x < 0.0f ? -1.0f : 1.0f;
}

inline float Min(float a, float b)
{
	return a < b ? a : b;
}

inline float Max(float a, float b)
{
	return a > b ? a : b;
}

inline float Clamp(float a, float low, float high)
{
	return Max(low, Min(a, high));
}

template<typename T> inline void Swap(T& a, T& b)
{
	T tmp = a;
	a = b;
	b = tmp;
}

// Random number in range [-1,1]
inline float Random()
{
	float r = (float)rand();
	r /= RAND_MAX;
	r = 2.0f * r - 1.0f;
	return r;
}

inline float Random(float lo, float hi)
{
	float r = (float)rand();
	r /= RAND_MAX;
	r = (hi - lo) * r + lo;
	return r;
}

#endif

