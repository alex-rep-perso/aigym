#pragma once
#include <algorithm>

template <typename T, class Compare>
void heapq_insert(std::vector<T>& heap, T elt, Compare compare = std::greater<T>{})
{
    heap.push_back(elt);
    std::push_heap(heap.begin(), heap.end(), compare);
}

template <typename T, class Compare>
T heapq_pop(std::vector<T>& heap, Compare compare = std::greater<T>{})
{
    std::pop_heap(heap.begin(), heap.end(), compare);
    T head = heap.back();
    heap.pop_back();

    return head;
}

template <typename T, class Compare>
void heapq_make(std::vector<T>& heap, Compare compare = std::greater<T>{})
{
    std::make_heap(heap.begin(), heap.end(), compare);
}