# distutils: language = c++

from Pathfinding cimport Pathfinder
import numpy as np
import math

SPAWN_DEFENDER = 4
SPAWN_ATTACKER = 6
PATH_FREE = 2
PATH_BLOCKED = 1
PATH_OUTSIDE = 0

cdef class PyPathfinder:

    cdef Pathfinder* c_pathfinder
    cdef readonly width, height
    cdef readonly cell_width, cell_height
    cdef readonly num_rows, num_cols
    cdef object static_grid
    cdef object nodeGenerator
    cdef object grid_pathfinding_list
    cdef readonly object grid_pathfinding

    def __dealloc__(self):
        del self.c_pathfinder

    def __cinit__(self, int width, int height, int cell_width, int cell_height, static_grid, nodeGenerator):
        self.width = width
        self.height = height
        self.cell_width = cell_width
        self.cell_height = cell_height
        self.num_rows = self.height // self.cell_height
        self.num_cols = self.width // self.cell_width

        self.c_pathfinder = new Pathfinder(cell_width, cell_height, width, height)

    def __init__(self, width, height, cell_width, cell_height, static_grid, nodeGenerator):

        self.static_grid = static_grid
        self.nodeGenerator = nodeGenerator

        # this grid store 1 for obstacle, 2 for interior
        self.grid_pathfinding_list = []
        self.grid_pathfinding = np.zeros((self.num_rows, self.num_cols), dtype=np.int)
        self.checking_collision()


    def to_cell_coord(self, int x, int y) -> Tuple[int, int]:
        idx_row = int(math.floor(y / self.cell_height))
        idx_col = int(math.floor(x / self.cell_width))

        return idx_col, idx_row

    def to_world_coord(self, int col, int row) -> Tuple[float, float]:
        x = (col + 0.5) * self.cell_width
        y = (row + 0.5) * self.cell_height

        return x, y

    def checking_collision(self):
        self.grid_pathfinding_list.clear()
        self.grid_pathfinding[:, :] = PATH_OUTSIDE
        for idx_row in range(self.num_rows):
            for idx_col in range(self.num_cols):
                center_x = (idx_col + 0.5) * self.cell_width
                center_y = (idx_row + 0.5) * self.cell_height
                ids = list(self.static_grid.query_area(center_x, center_y, self.cell_width,
                                                       self.cell_height, -1))

                colliding_spawn = False
                if len(ids) > 0:
                    for id in ids:
                        obj_id = ids[0] >> self.nodeGenerator.max_id_bin
                        node_id = ids[0] - (obj_id << self.nodeGenerator.max_id_bin)
                        obj = self.nodeGenerator.all_nodes[node_id].room.objects[obj_id]

                        if obj.is_spawn:
                            self.grid_pathfinding_list.append(obj.spawn_type)
                            self.grid_pathfinding[idx_row, idx_col] = obj.spawn_type
                            colliding_spawn = True
                            break

                    if not colliding_spawn:
                        self.grid_pathfinding_list.append(PATH_BLOCKED)
                        self.grid_pathfinding[idx_row, idx_col] = PATH_BLOCKED
                else:
                    ids_node = self.nodeGenerator.grid.query_area(center_x, center_y, self.cell_width,
                                                                self.cell_height, -1)
                    if len(ids_node) > 0:
                        self.grid_pathfinding_list.append(PATH_FREE)
                        self.grid_pathfinding[idx_row, idx_col] = PATH_FREE
                    else:
                        self.grid_pathfinding_list.append(PATH_OUTSIDE)
                        self.grid_pathfinding[idx_row, idx_col] = PATH_OUTSIDE

        self.c_pathfinder.setPathfindingGrid(self.grid_pathfinding_list)


    def get_path_to(self, cell_start_x: int, cell_start_y: int, cell_goal_x: int, cell_goal_y: int,
                    force_start: bool = False):

        path = self.c_pathfinder.getPathTo(cell_start_x, cell_start_y, cell_goal_x, cell_goal_y, force_start)

        result = [np.array([p.x, p.y]) for p in path]
        return result
