# distutils: language = c++

from libc.stdint cimport uintptr_t
from libcpp.vector cimport vector

cdef extern from "Body.cpp":
    pass

cdef extern from "MathUtils.h":
    cdef cppclass Vec2[T]:
        Vec2() except +
        Vec2(T x, T y) except +
        void Set(T x_, T y_)
        float Length()
        T x, y

cdef extern from "Body.h":
    cdef enum class BodyType:
        RECTANGLE = 0
        CIRCLE = 1

    cdef cppclass Body:
        Body() except +
        Body(int id_) except +
        void Set(float w, float h, float m)
        void SetWidth(float w, float h)
        void AddForce(const Vec2& f)
        void SetPosition(float x, float y)
        void SetVelocity(float x, float y)
        void SetRotation(float r)
        void SetRadius(float r)
        void SetType(BodyType t)
        void Reset()
        Vec2[float] position
        float rotation
        Vec2[float] velocity
        float angularVelocity
        Vec2[float] width
        float friction
        float mass, invMass
        float I, invI
        float radius
        int id
        vector[int] colliding_physics_ids
        BodyType type